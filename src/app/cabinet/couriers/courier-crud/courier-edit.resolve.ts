import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

import { CouriersService } from '../couriers.service';


@Injectable()

export class CourierEditResolve implements Resolve<any> {

  constructor (
      private couriersService: CouriersService,
      private router: Router,
  ) {}

  resolve (router: ActivatedRouteSnapshot) {
    return this.couriersService.editCourier(Number(router.paramMap.get('id'))).catch(err => {
      this.router.navigateByUrl('/cabinet/couriers');
      return Observable.of({error: err});
    });
  }

}
