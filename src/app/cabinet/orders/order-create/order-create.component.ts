import {Component, OnInit, ViewContainerRef, OnDestroy} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { HelpersService } from '@shared/services/helpers.service';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';

import { OrdersService } from '../orders.service';

import { FromValidation } from '@fromValidation';

import { OrdersGet } from '../orders';
import { maskPhone, maskTime } from '@shared/const/masks';
import { CustomerUserInfo } from '@shared/interface/user-info';
import {ValidateOnlyNumber} from '@shared/formValidators/validateOnlyNumber';
import {AutocompleteCity} from '@shared/interface/autocomplete-city';

@Component ({
    selector: 'app-order-create',
    templateUrl: '/order-create.component.html',
    styleUrls: ['./order-create.component.css']
})

export class OrderCreateComponent extends FromValidation implements OnInit, OnDestroy {

    public local_from = [];
    public local_to = [];
    public test1;

    public isWhoPay = false;
    public title = 'Добавить заказ';
    public form: FormGroup;
    public order: OrdersGet;

    public calcDiscount;
    public corectCalcDiscount;
    public dataDirectionTable;

    public maskPhone =  maskPhone;
    public maskTime = maskTime;


    public userInfo_from;

    public userInfo_to;

    public fromNames = {
      type: {
        name: 'transporter_type',
        value: '',
      },
      id: {
        name: 'transporter_id',
        value: '',
      },
      direction_id: {
        name: 'transporter_direction_id',
        value: '',
      },
      direction_name: {
        name: 'transporter_direction_name',
        value: '',
      },
      direction_phone: {
        name: 'transporter_direction_phone',
        value: '',
      },
      direction_email: {
        name: 'transporter_direction_email',
        value: '',
      },
      direction_resperson: {
        name: 'transporter_direction_resperson',
        value: '',
      },
    };

    private  destroy$: Subject<boolean> = new Subject<boolean>();

    public autocompleteCityFromFormComponent: AutocompleteCity;
    public autocompleteCityToFormComponent: AutocompleteCity;

    constructor (
        private fb: FormBuilder,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef,
        private activatedRoute: ActivatedRoute,
        public router: Router,
        public helpersService: HelpersService,
        public findToAutocompleteService: FindToAutocompleteService,
        public ordersService: OrdersService,
    ) {
      super (
            router,
            ordersService,
        );
       this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit () {
        this.order = this.activatedRoute.snapshot.data['order'];
        this.buildFrom();
        this.changeWhoPay();

        this.activatedRoute
            .queryParams
            .subscribe(params => {
              this.setCustomersUsersInfo(params);
        });
        this.setClacDiscount(this.order.data.clientDiscount);
        this.checkDiscount(this.form.get('type').value);
        this.chackTypeOrder();
        this.setDataDirectionTable(this.order.data.contractPrice);
        this.buildAutocompleteCityFromFormComponent();
        this.buildAutocompleteCityToFormComponent();
    }

    ngOnDestroy () {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

  /*========= LOCATION =========*/

  // public isLiction;
  // onGetLocation(city) {
  //   this.isLiction = this.findToAutocompleteService.findLocation(city)
  //     .subscribe(res => {
  //       this.local_from = res.data;
  //       this.local_to = res.data;
  //       this.isLiction.unsubscribe();
  //     });
  //
  // }
  //
  // onGetLocationFrom(event) {
  //   this.local_from = [];
  //   const test2 = event.target.value;
  //   this.onGetLocation(test2);
  // }
  //
  // onSelectLocationFrom(event) {
  //   if (!event) {
  //     return false;
  //   }
  //   this.form.get('country_from').setValue(event.countryName);
  //   this.form.get('zip_code_from').setValue(event.postal_code);
  //   this.form.get('region_from').setValue(event.region);
  // }
  //
  //
  // onGetLocationTo(event) {
  //   this.local_to = [];
  //   const test2 = event.target.value || this.test1;
  //   this.onGetLocation(test2);
  // }
  // onSelectLocationTo(event) {
  //   if (!event) {
  //     return false;
  //   }
  //   this.form.get('country_to').setValue(event.countryName);
  //   this.form.get('zip_code_to').setValue(event.postal_code);
  //   this.form.get('region_to').setValue(event.region);
  // }

  /*========= / LOCATION =========*/


  /*------FindUser----------*/

  onGetUserInfoFrom(event) {
    this.findToAutocompleteService.findUser(event.target.value)
      .subscribe(res => {
        this.userInfo_from = res['data'];
      });
  }

  onSelectUserInfoFrom(event) {
    if (!event) {
      return false;
    }
    this.form.get('name').patchValue(event.fio);
    this.form.get('company').patchValue(event.company);
    this.form.get('email').patchValue(event.email);
    this.form.get('phone').patchValue(event.phone);
    this.setClacDiscount(event.clientDiscount);
    this.setDataDirectionTable(event.contractPrice);
    this.checkDiscount(this.form.get('type').value);
  }

  onGetUserInfoTo(event) {
    this.findToAutocompleteService.findUser(event.target.value)
      .subscribe(res => {
        this.userInfo_to = res['data'];
      });
  }

  onSelectUserInfoTo(event) {
    if (!event) {
      return false;
    }
    this.form.get('name_receiver').setValue(event.fio);
    this.form.get('company_receiver').setValue(event.company);
    this.form.get('email_receiver').setValue(event.email);
    this.form.get('phone_receiver').setValue(event.phone);
  }
  /*------FindUser----------*/


  private buildFrom (): void {
        this.form = this.fb.group({
            code_dkl: this.order.data.code_dkl,
            code_transporter: this.order.data.code_transporter,
            name: this.order.data.name,
            courier_id: this.order.data.courier_id,
            company: this.order.data.company,
            email: this.order.data.email,
            phone: this.order.data.phone,
            // city_from: this.order.data.city_from,
            // country_from: this.order.data.country_from,
            // region_from: this.order.data.region_from,
            // zip_code_from: this.order.data.zip_code_from,
            name_receiver: this.order.data.name_receiver,
            company_receiver: this.order.data.company_receiver,
            email_receiver: this.order.data.email_receiver,
            phone_receiver: this.order.data.phone_receiver,
            // city_to: this.order.data.city_to,
            // country_to: this.order.data.country_to,
            // region_to: this.order.data.region_to,
            // zip_code_to: this.order.data.zip_code_to,
            type: this.order.data.type,
            // insurance: this.order.data.insurance,
            // insurance_currency: this.order.data.insurance_currency,
            // insurance_price: this.order.data.insurance_price,
            export_cause: this.order.data.export_cause,
            export_terms: this.order.data.export_terms,
            stock_pay_type: this.order.data.stock_pay_type,
            stock_pay: this.order.data.stock_pay,
            stock_price: this.order.data.stock_price,
            stock_price_currency: this.order.data.stock_price_currency,
            stock_comment: this.order.data.stock_comment,
            stock_delivery_fact_pay: this.order.data.stock_delivery_fact_pay,
            pickup_timefrom: this.order.data.pickup_timefrom,
            pickup_timeto: this.order.data.pickup_timeto,
            client_comment: this.order.data.client_comment,
            items_count: this.order.data.items_count,
            items_totalweight: this.order.data.items_totalweight,
            items_type: this.order.data.items_type,
            items_price: this.order.data.items_price,
            items_price_currency: this.order.data.items_price_currency,
            delivery_type: this.order.data.delivery_type,
            status: this.order.data.status,
            thirdside_name: this.order.data.thirdside_name,
            thirdside_company: this.order.data.thirdside_company,
            thirdside_email: this.order.data.thirdside_email,
            thirdside_phone: this.order.data.thirdside_phone,
            thirdside_city: this.order.data.thirdside_city,
            thirdside_country: this.order.data.thirdside_country,
            thirdside_region: this.order.data.thirdside_region,
            thirdside_zip_code: this.order.data.thirdside_zip_code,
            filesUpload: '',
            delivery_price: this.order.data.delivery_price,
            delivery_price_currency: this.order.data.delivery_price_currency,
            invoice_account: this.order.data.invoice_account,
            invoice_nds: this.order.data.invoice_nds,
            pay_to_agents: this.order.data.pay_to_agents,
            // itemsParams: this.fb.group({
            //   client_comment_1: '',
            //   items_type_1: '',
            //   delivery_type_1: '',
            //   items_count_1: ['', ValidateOnlyNumber],
            //   items_weight_1: ['', ValidateOnlyNumber],
            //   items_length_1: ['', ValidateOnlyNumber],
            //   items_width_1: ['', ValidateOnlyNumber],
            //   items_height_1: ['', ValidateOnlyNumber],
            //   items_price_1: ['', ValidateOnlyNumber],
            //   items_price_currency_1: '',
            //   client_comment_2: '',
            //   items_type_2: '',
            //   delivery_type_2: '',
            //   items_count_2: ['', ValidateOnlyNumber],
            //   items_weight_2: ['', ValidateOnlyNumber],
            //   items_length_2: ['', ValidateOnlyNumber],
            //   items_width_2: ['', ValidateOnlyNumber],
            //   items_height_2: ['', ValidateOnlyNumber],
            //   items_price_2: ['', ValidateOnlyNumber],
            //   items_price_currency_2: '',
            //   client_comment_3: '',
            //   items_type_3: '',
            //   delivery_type_3: '',
            //   items_count_3: ['', ValidateOnlyNumber],
            //   items_weight_3: ['', ValidateOnlyNumber],
            //   items_length_3: ['', ValidateOnlyNumber],
            //   items_width_3: ['', ValidateOnlyNumber],
            //   items_height_3: ['', ValidateOnlyNumber],
            //   items_price_3: ['', ValidateOnlyNumber],
            //   items_price_currency_3: '',
            //   client_comment_4: '',
            //   items_type_4: '',
            //   delivery_type_4: '',
            //   items_count_4: ['', ValidateOnlyNumber],
            //   items_weight_4: ['', ValidateOnlyNumber],
            //   items_length_4: ['', ValidateOnlyNumber],
            //   items_width_4: ['', ValidateOnlyNumber],
            //   items_height_4: ['', ValidateOnlyNumber],
            //   items_price_4: ['', ValidateOnlyNumber],
            //   items_price_currency_4: '',
            // }),
        });
    }

    public onSubmit (form): void {
      if (this.form.valid) {
        const dataWithNewFormatDate = this.helpersService.changeFormatDateInForm(this.form.value, ['pickup_date']);
        this.ordersService.createOrder(dataWithNewFormatDate).takeUntil(this.destroy$).subscribe( res => {
          if (res.success === 'ok') {
            this.toastr.success('Has been created successfully');
            setTimeout(() => {
              this.router.navigateByUrl('/cabinet/orders');
            }, 1000);
          }  else {
            this.toastr.error('Error');
            this.validationFormBackEnd(res.validation, this.form);
          }
        });
      } else {
        this.toastr.error('Error');
        this.validationFormsSubmit(this.form);
      }
    }

    public setCustomersUsersInfo (param) {
        if (Object.keys(param).length > 0) {
          this.form.get('name').setValue(param.fio);
          this.form.get('company').setValue(param.company);
          this.form.get('phone').setValue(param.phone);
          this.form.get('email').setValue(param.email);
        }
    }

    private changeWhoPay () {
      this.form.get('stock_pay').valueChanges.subscribe(type => {
          this.isWhoPay = this.helpersService.whoPay(type);
      });
    }

    private chackTypeOrder () {
      this.form.get('type').valueChanges.subscribe( (item) => {
        this.checkDiscount(item);
      });
    }

    private setClacDiscount (val) {
      this.calcDiscount = val;
      this.corectCalcDiscount = val;
    }

    private setDataDirectionTable (val) {
      this.dataDirectionTable = val;
    }

    private checkDiscount (val) {
      if (val != '1_export_courier_standard') {
          this.calcDiscount = 0;
      } else {
        this.calcDiscount = this.corectCalcDiscount;
      }
    }

  private buildAutocompleteCityToFormComponent () {
    this.autocompleteCityToFormComponent = {
      searchSelectAutocomplete: {
        formControlName: 'city_to',
        label: 'Город получателя',
        value: this.order.data.city_to,
      },
      searchInfoAutocomplete: {
        country: {
          formControlName: 'country_to',
          label: 'Страна получателя',
          value: this.order.data.country_to,
          searchEvent: 'countryName',
        },
        region: {
          formControlName: 'region_to',
          label: 'Район/Штат получателя',
          value: this.order.data.region_to,
          searchEvent: 'region',
        },
        zip_code: {
          formControlName: 'zip_code_to',
          label: 'Индекс получателя',
          value: this.order.data.zip_code_to,
          searchEvent: 'postal_code',
        }
      }
    }
  }

  private buildAutocompleteCityFromFormComponent () {
    this.autocompleteCityFromFormComponent = {
      searchSelectAutocomplete: {
        formControlName: 'city_from',
        label: 'Город отправителя',
        value: this.order.data.city_from,
      },
      searchInfoAutocomplete: {
        country: {
          formControlName: 'country_from',
          label: 'Страна отправителя',
          value: this.order.data.country_from,
          searchEvent: 'countryName',
        },
        region: {
          formControlName: 'region_from',
          label: 'Район/Штат отправителя',
          value: this.order.data.region_from,
          searchEvent: 'region',
        },
        zip_code: {
          formControlName: 'zip_code_from',
          label: 'Индекс отправителя',
          value: this.order.data.zip_code_from,
          searchEvent: 'postal_code',
        }
      }
    }
  }

}
