import { Component, ChangeDetectionStrategy, ViewChild, TemplateRef, OnInit, OnDestroy, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { Subject } from 'rxjs/Subject';
import { CalendarEvent, CalendarEventAction, CalendarDateFormatter, DAYS_OF_WEEK} from 'angular-calendar';
import 'rxjs/add/operator/takeUntil';

import { FromValidation } from '@fromValidation';

import { CustomDateFormatter } from './custom-date-formatter.provider';
import { TaskmanModalGet } from './taskman';
import 'rxjs/add/operator/filter';
import { TaskmanService } from './taskman.service';

import moment = require('moment');
import { ToastsManager } from 'ng2-toastr';


const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    }
};

@Component ({
    selector: 'app-taskman',
    templateUrl: 'taskman.component.html',
    styleUrls: ['taskman.component.css'],
    // changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: CalendarDateFormatter,
            useClass: CustomDateFormatter
        }
    ]
})

export class TaskmanComponent extends FromValidation implements OnInit, OnDestroy {

    @ViewChild('modalContent') modalContent: TemplateRef<any>;

    public title = 'Таск-Трекер';

    public view = 'month';
    public viewDate: Date = new Date();
    public weekStartsOn: number = DAYS_OF_WEEK.MONDAY;
    public activeDayIsOpen = false;
    public tasks;

    public formModal: FormGroup;
    public taskModal: TaskmanModalGet;

    public page = 1;
    public managerId = '';
    public taskF = 'all';
    public datePick;
    public calendarDays;
    public eventArray = [];
    public events;
    public disabledBtn  = {
      next: {
        disabledBtnState: false,
        disabledInDate: new Date(),
      },
      prev: {
        disabledBtnState: false,
        disabledInDate: new Date(),
      }
    }

    public refresh: Subject<any> = new Subject();

    private modalRef: NgbModalRef;
    private destroy$: Subject<boolean> = new Subject<boolean>();


/*    events: CalendarEvent[] = [
        {
            /!*start: subDays(startOfDay(new Date(2018, 7, 8)), 0),*!/
            start: new Date(2018, 6, 8),
            /!*end: addDays(4, 1),*!/
            title: 'A 3 day event',
            color: colors.red,
        },*/
/*        {
            start: addHours(startOfDay(new Date()), 2),
            end: new Date(),
            title: 'A draggable and resizable evsent',
            color: colors.yellow,
            // actions: this.actions,
            resizable: {
                beforeStart: true,
                afterEnd: true
            },
              {
          start: new Date(2018, 6 ,8),
          title: 'blabal',
          color: colors.yellow,
        },
        {
          start: new Date(2018, 6 ,7),
          title: 'blabal',
          color: colors.yellow,
        },
        {
          start: new Date(2018, 6 ,7),
          title: 'blabal',
          color: colors.yellow,
        }
            draggable: false
        }*/
   /* ];*/

    // modalData: {
    //     action: string;
    //     event: CalendarEvent;
    // };


/*    actions: CalendarEventAction[] = [
        {
            label: '<i class="fa fa-fw fa-pencil"></i>',
            onClick: ({ event }: { event: CalendarEvent }): void => {
                this.handleEvent('Edited', event);

            }
        },
        {
            label: '<i class="fa fa-fw fa-times"></i>',
            onClick: ({ event }: { event: CalendarEvent }): void => {
                this.events = this.events.filter(iEvent => iEvent !== event);
                this.handleEvent('Deleted', event);
            }
        }
    ];*/

    constructor (
        public taskmanService: TaskmanService,
        public router: Router,
        private modalService: NgbModal,
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder,
        private vcr: ViewContainerRef,
        private toastr: ToastsManager,
    ) {
      super(
        router,
        taskmanService,
      );
      this.toastr.setRootViewContainerRef(vcr);
    }

    whatADate() {
      if (this.disabledBtn.next.disabledInDate < this.viewDate) {
        this.disabledBtn.next.disabledBtnState = true;
      } else {
        this.disabledBtn.next.disabledBtnState = false;
      }

      if (this.disabledBtn.prev.disabledInDate > this.viewDate) {
        this.disabledBtn.prev.disabledBtnState = true;
      } else {
        this.disabledBtn.prev.disabledBtnState = false;
      }

      this.datePick = moment(this.viewDate).format('D.M.YYYY');
      this.taskmanService.getMonthOfDate(this.datePick)
        .subscribe(res => {
          this.eventArray = [];
          this.calendarDays = res['days'];
          this.calendarDays.forEach(item => {
            this.calendarDays = moment(item['start']).format('YYYY,M,D');
            this.events =
              {
                start: new Date(this.calendarDays),
                title: item['title'],
                color: item['color'],
              };
            this.eventArray.push(this.events);
          });
        });
    }

    ngOnInit () {
      this.tasks = this.activatedRoute.snapshot.data['tasks'];
      this.whatADate();
      this.setMaxDateToNextBtn(11);
      this.setMinDateToPrevBtn(11);
    }

    ngOnDestroy () {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

/*    getEvents() {
      this.taskmanService.getMonthCount()
        .subscribe(res => {
          this.calendarDays = res['days'];
          this.calendarDays.forEach(item => {
            this.calendarDays = moment(item['start']).format('YYYY,M,D');
            this.events =
              {
                start: new Date(this.calendarDays),
                title: item['title'],
                color: item['color'],
              };
            this.eventArray.push(this.events);
          });



        });
    }*/
    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
        this.viewDate = date;
        this.datePick = moment(this.viewDate).format('D.M.YYYY');
/*        this.taskmanService.getMonthOfDate(this.datePick)
          .subscribe(res => {
            this.eventArray = [];
            this.calendarDays = res['days'];
            this.calendarDays.forEach(item => {
              this.calendarDays = moment(item['start']).format('YYYY,M,D');
              this.events =
                {
                  start: new Date(this.calendarDays),
                  title: item['title'],
                  color: item['color'],
                };
              this.eventArray.push(this.events);
            });


          });*/



      this.taskmanService.getFilterOfDate(this.datePick)
        .subscribe(res => {
          this.tasks = res;
          this.calendarDays = res['listdata'];
        });

        if (isSameMonth(date, this.viewDate)) {
            if (
                (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
                events.length === 0
            ) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }

    }

    public statusChange (id, event) {
      this.taskmanService.editTasks({id: id, value: event.target.value})
        .takeUntil(this.destroy$).subscribe( res => {
            if (res.success = 'ok') {
              this.toastr.success('Has been created successfully');
            } else {
              this.toastr.error('Error');
            }
        });
    }
    public open (content) {
        this.taskmanService.creataTasks()
            .takeUntil(this.destroy$)
            .subscribe( res => {
              this.taskModal = res;
              this.modalRef = this.modalService.open(content, {size: 'lg'});
              this.buildForm();
            });
    }

    public managerFilter (event) {
      this.managerId = event.target.value;
      this.sendSort();
    }

    public tablePage (page: number): void {
      this.page = page;
      this.sendSort();
    }

    public tasksFilte (value) {
      this.taskF = value;
      this.sendSort();
    }

    private sendSort () {
        this.taskmanService.sort({page: this.page, userId: this.managerId, tasksfilter: this.taskF})
          .takeUntil(this.destroy$).subscribe( res => {
              this.tasks = res;
        });
    }

    public onSubmit (): void {
      // return false;
      if (this.formModal.valid) {
        this.taskmanService.updateTasks(this.formModal.value)
          .takeUntil(this.destroy$)
          .subscribe( res => {
            if (res.success === 'ok') {
                this.toastr.success('Has been created successfully');
                this.getTasks();
                this.modalRef.close();
            }  else {
              this.toastr.error('Error');
              this.validationFormBackEnd(res.validation, this.formModal);
            }
        });

      } else {
        this.validationFormsSubmit(this.formModal);
      }
    }


  private buildForm () {
      this.formModal = this.fb.group({
        title: ['', Validators.required],
        description: '',
        // end_date: ['', Validators.required],
        user_id: ['', Validators.required],
        priority: '',
      });
  }

  private getTasks () {
      this.taskmanService.getTasks().takeUntil(this.destroy$).subscribe( res => {
        this.tasks = res;
      });
  }

  private setMaxDateToNextBtn (month: number) {
    this.disabledBtn.next.disabledInDate.setMonth(this.disabledBtn.next.disabledInDate.getMonth() + month);
  }

  private setMinDateToPrevBtn (month: number) {
    this.disabledBtn.prev.disabledInDate.setMonth(this.disabledBtn.prev.disabledInDate.getMonth() - month);
  }
}
