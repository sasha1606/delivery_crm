import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { CustomersUsersService } from './customers-users.service';

@Injectable()

export class CustomerUserResolve implements Resolve<any> {
    constructor (
        private customersUsersService: CustomersUsersService
    ) {}

    resolve (router: ActivatedRouteSnapshot) {
        return this.customersUsersService.getUsersList();
    }
}
