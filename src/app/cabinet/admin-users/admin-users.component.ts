import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { AdminUsersService } from './admin-users.service';

import { AdminList } from './admin-user';

@Component ({
    selector: 'app-admin-user',
    templateUrl: './admin-users.component.html',
    styleUrls: ['./admin-users.component.css']
})

export class AdminUsersComponent implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    title = 'Административный персонал';
    usersList: AdminList[];

    sort = '';
    page = 1;
    search = '';

    constructor (
        private activatedRoute: ActivatedRoute,
        private adminUsersService: AdminUsersService,
        private router: Router,
    ) {}

    ngOnInit () {
        this.usersList = this.activatedRoute.snapshot.data['usersList'];
    }

    ngOnDestroy () {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

    public deleteUser (id: number): void {
        this.adminUsersService.deleteUser(id).takeUntil(this.destroy$).subscribe(res => {
            if (res.success === 'ok') {
                this.sendSort();
            }

        });
    }

    public tableSearch (word: string): void {
        this.search = word;
        this.sendSort();
    }

    public tableSort (param: string): void {
        if (this.sort.length > 0 && param === this.sort) {
            param = `-${param}`;
        }
        this.sort = param;
        this.sendSort();
    }

    public tablePage (page: number): void {
        this.page = page;
        this.sendSort();
    }

    public goToLink (link) {
      console.log(link);
      this.router.navigate([link]);
      return false;
    }

    private sendSort (): void {
        this.adminUsersService.search(this.sort, this.search, this.page).takeUntil(this.destroy$).subscribe( users => {
            this.usersList = users;
        });
    }

}
