import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { PaginationModule } from '@shared/pagination/pagination.module';
import { PipeModule } from '@shared/pipes/pipe.module';

import { TaskmanService } from './taskman.service';
import { TaskmanResolve } from './taskman.resolve';

import { CalendarModule } from 'angular-calendar';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import { TaskmanComponent } from './taskman.component';

import { TaskmanRoutingModule } from './taskman-routing.module';

import { DatepickerModule } from '@shared/datepicker/datepicker.module';

@NgModule ({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PipeModule,
        NgbModalModule.forRoot(),
        CalendarModule.forRoot(),
        TaskmanRoutingModule,
        PaginationModule,
        DatepickerModule,
    ],
    declarations: [
        TaskmanComponent,
    ],
    providers: [
        { provide: LOCALE_ID, useValue: 'ru' },
        TaskmanResolve,
        TaskmanService,
    ]
})

export class TaskmanModule { }
