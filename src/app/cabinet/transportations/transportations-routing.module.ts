import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransportationsComponent } from './transportations.component';
import { TransportationCreateComponent } from './transportation-create-edit/transportation-create.component';
import { TransportationShowComponent } from './transportation-show/transportation-show.component';
import { TransportationEditComponent } from './transportation-edit_delete/transportation-edit.component';

import { TransportationCreateResolve } from './transportation-create-edit/transportation-create.resolve';
import { TransportationsResolve } from './transportations.resolve';
import { TransportationGetResolve } from './transportation-get.resolve';

import { typePage } from './transportations';

const ROUTERS: Routes = [
    {
        path: '',
        component: TransportationsComponent,
        resolve: {
          transportations: TransportationsResolve,
        }
    },
    {
        path: 'create',
        component: TransportationCreateComponent,
        resolve: {
          transportation: TransportationCreateResolve,
        },
        data: {
          data: typePage.create,
        }
    },
    {
        path: 'show/:id',
        component: TransportationShowComponent,
        resolve: {
          transportation: TransportationGetResolve,
        }
    },
    {
        path: 'edit/:id',
        component: TransportationCreateComponent,
        resolve: {
          transportation: TransportationGetResolve,
        },
        data: {
          data: typePage.edit,
        }
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class TransportationsRoutingModule {}
