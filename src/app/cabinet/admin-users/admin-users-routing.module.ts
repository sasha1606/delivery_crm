import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminUsersComponent } from './admin-users.component';
import { AdminUserCreateComponent } from './admin-user-create/admin-user-create.component';
// import { AdminUserShowComponent } from './admin-user-show/admin-user-show.component';
import { AdminUserEditComponent } from './admin-user-edit/admin-user-edit.component';

import { AdminUsersResolve } from './admin-users.resolve';
import { AdminUsersEditResolve } from './admin-user-edit/admin-user-edit.resolve';
import { AdminUserCreateResolve } from './admin-user-create/admin-user-create.resolve';

const ROUTERS: Routes = [
    {
        path: '',
        component: AdminUsersComponent,
        resolve: {
            usersList: AdminUsersResolve,
        },
    },
    {
        path: 'create',
        component: AdminUserCreateComponent,
        resolve: {
            data: AdminUserCreateResolve,
        }
    },
    // {
    //     path: 'show/:id',
    //     component: AdminUserShowComponent,
    // },
    {
        path: 'edit/:id',
        component: AdminUserEditComponent,
        resolve: {
            user: AdminUsersEditResolve,
        },
    }
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class AdminUsersRoutingModule {}
