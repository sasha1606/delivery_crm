import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppRoutingModele } from './app-routing.module';
import { RegistrationModule } from './auth/registration.module';
import { CabinetModule } from './cabinet/cabinet.module';

import { AppComponent } from './app.component';

import { AuthGuard } from './auth.guard';
import { AuthInterceptor } from './auth.interceptor';

import { ApiService } from './api/api.service';
import { TokenStorageService } from './token-storage.service';
import { UserInfoService } from './cabinet/user.info.service';

import {ProfileSettingsService} from './cabinet/profile-settings/profile-settings.service';
import { ToastModule, ToastOptions } from 'ng2-toastr/ng2-toastr';
import {CustomOption} from './toastr.options';

import { OrdersCountService } from '@shared/services/orders-count.service';

registerLocaleData(localeRu);

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    NgbModule.forRoot(),
    AngularFontAwesomeModule,
    BrowserModule,
    RegistrationModule,
    BrowserAnimationsModule,
    CabinetModule,
    AppRoutingModele,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    ToastModule.forRoot(),

  ],
  providers: [
      OrdersCountService,
      AuthGuard,
      ApiService,
      TokenStorageService,
      UserInfoService,
      ProfileSettingsService,
      {
        provide: ToastOptions,
        useClass: CustomOption
      },
      {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true
      }
  ],
  bootstrap: [AppComponent],
})

export class AppModule { }
