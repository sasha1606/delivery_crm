import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';

import { TariffsDkl } from './tariffs-dkl';

@Component ({
    selector: 'app-tariffs-dkl',
    templateUrl: './tariffs-dkl.component.html',
    styleUrls: ['./tariffs-dkl.component.css']
})

export class TariffsDklComponent implements OnInit {
    title = 'Тарифы DKL';
    tariffs: TariffsDkl;

    constructor (
      private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit () {
      this.tariffs = this.activatedRoute.snapshot.data['tariffs'];
    }

}
