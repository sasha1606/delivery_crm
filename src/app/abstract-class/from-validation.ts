import 'rxjs/add/operator/takeUntil';

export abstract class FromValidation {

    constructor (
       public router,
       public sendService,
    ) {}

    protected validationFormBackEnd (errors, form): void {
        for (const error of Object.keys(errors)) {
            if (Array.isArray(errors[error])) {
              form.controls[error].setErrors({'incorrect': true});

              this.setErrors({form: form, field: error});
            } else {
              for (const innerError of Object.keys(errors[error])) {
                  form.controls[error].controls[innerError].setErrors({'incorrect': true});
              }
            }
        }
    }

    protected validationFormsSubmit (form): void {
        Object.keys(form.controls).forEach(field => {
          this.setErrors({form: form, field: field});
        });
    }

    public submitForm (sendMetod, form, userId, destroy) {
        if (form.valid) {
            this.sendService[sendMetod](form.value, userId).takeUntil(destroy).subscribe( res => {
                if (res.success === 'ok') {
                  setTimeout(() => {
                    this.router.navigateByUrl('/cabinet/admin-users');
                  }, 1000);
                }  else {
                    this.validationFormBackEnd(res.validation, form);
                }
            });

        } else {
            this.validationFormsSubmit(form);
        }
    }

    private setErrors (data) {
      const control = data['form'].get(data['field']);
      control.markAsTouched({ onlySelf: true });
      control.markAsDirty({onlySelf: true});
    }
}
