import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiService } from '@api/api.service';

import { ApiSrc } from '@api/api.src';

import { Courier, CourierList, CouriersGet} from './couriers';

@Injectable()

export class CouriersService {

  constructor (
      private apiService: ApiService,
  ) {}

  public getCouriersList (): Observable<CourierList> {
    return this.apiService.get(ApiSrc.couriers.list);
  }

  public search ({sort = '', key = '', page = 1}): Observable<CourierList> {
    // for IE
    const  keyword = encodeURIComponent(key);
    return this.apiService.get(`${ApiSrc.couriers.list}?sort=${sort}&keyword=${keyword}&page=${page}`);
  }

  public deleteCourier (id: number): Observable<CourierList> {
    return this.apiService.post(ApiSrc.couriers.deleteCourier, {id: id});
  }


  public getCourier (): Observable<CouriersGet> {
    return this.apiService.get(ApiSrc.couriers.createCourier);
  }

  public createCourier (courier: Courier): Observable<CouriersGet> {
    return this.apiService.post(ApiSrc.couriers.createCourier, courier);
  }

  public showCourier (id: number): Observable<CouriersGet> {
    return this.apiService.get(`${ApiSrc.couriers.showCourier}/${id}`);
  }

  public editCourier (id: number): Observable<CouriersGet> {
    return this.apiService.get(`${ApiSrc.couriers.editCourier}/${id}`);
  }

  public updateCourier ({ courier, id }): Observable<CouriersGet> {
    return this.apiService.post(`${ApiSrc.couriers.editCourier}/${id}`, courier);
  }
}
