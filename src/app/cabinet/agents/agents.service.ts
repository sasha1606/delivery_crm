import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiService } from '@api/api.service';

import { ApiSrc } from '@api/api.src';

import { AgentGet, Agent, AgentModalGet, AgentModal, AgentsListGet } from './agents';

@Injectable()

export class AgentsService {

  constructor (
    private apiService: ApiService
  ) {}

  public getAgentsList (): Observable<AgentsListGet> {
    return this.apiService.post(ApiSrc.agents.list, {});
  }

  public search ({page: page = 1, keyword: keyword = '', sort: sort = ''}) {
    // for IE
    const  key = encodeURIComponent(keyword);
    return this.apiService.get(`${ApiSrc.agents.list}?keyword=${key}&page=${page}&sort=${sort}`);
  }


  public getAgent (id: number): Observable<AgentGet> {
    return this.apiService.get(`${ApiSrc.agents.show}/${id}`);
  }

  public dataCreateAgent (): Observable<AgentGet> {
    return this.apiService.get(ApiSrc.agents.createAgent);
  }

  public createAgent (agent: Agent | object): Observable<AgentGet> {
    return this.apiService.post(`${ApiSrc.agents.createAgent}`, agent);
  }

  public editAgent ({agent, id}): Observable<AgentGet> {
    return this.apiService.post(`${ApiSrc.agents.edit}/${id}`, agent);
  }

  public dataAgentModal (): Observable<AgentModalGet> {
    return this.apiService.get(ApiSrc.agents.createAgentModal);
  }

  public createAgentModal (agent: AgentModal): Observable<AgentModalGet> {
    return this.apiService.post(ApiSrc.agents.createAgentModal, agent);
  }


  public getLengthObj (params) {
    if (params) {
      return Object.keys(params).length;
    }
    return 0;
  }
}

