import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component ({
    selector: 'app-select',
    templateUrl: './select.component.html',
    styleUrls: ['./select.component.css'],
})

export class SelectComponent implements OnInit {

    @Input() data;
    @Input() form;
    @Input() dataValue;
    @Input() dataName;

    constructor () {}

    ngOnInit () {
        this.form.addControl(this.dataName, new FormControl(this.dataValue, [Validators.required]));
    }
}
