import { Component, Input, OnInit } from '@angular/core';

@Component ({
    selector: 'app-dashboard-carriers',
    templateUrl: './dashboard-carriers.component.html',
    styleUrls: ['./dashboard-carriers.component.css'],
})

export class CarriersAgentsComponent implements OnInit {

    @Input() carriers;

    public carriersInfo;
    // data: any = [
    //     {
    //         name: 'Компания 1',
    //         count: 2
    //     },
    //     {
    //         name: 'Компания 2',
    //         count: 6
    //     },
    //     {
    //         name: 'Компания 3',
    //         count: 2
    //     },
    //     {
    //         name: 'Компания 4',
    //         count: 8
    //     },
    //     {
    //         name: 'Компания 5',
    //         count: 2
    //     }
    // ];

    constructor () {}

    ngOnInit () {
      this.carriersInfo = this.carriers;
    }
}
