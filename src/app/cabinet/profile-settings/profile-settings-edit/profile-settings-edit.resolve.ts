import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { ProfileSettingsService } from '../profile-settings.service';

@Injectable()

export class ProfileSettingsEditResolve implements Resolve<any> {

    constructor (
        private profileSettingsService: ProfileSettingsService
    ) {}

    resolve () {
        return this.profileSettingsService.getUser();
    }
}
