import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StorageDealRoutingModule } from './storage-deal-routing.module';

import { StorageDealComponent } from './storage-deal.component';

import { StorageDealService } from './storage-deal.service';
import { StorageDealResolve } from './storage-deal.resolve';

@NgModule({
  imports: [
    CommonModule,
    StorageDealRoutingModule,
  ],
  declarations: [
    StorageDealComponent
  ],
  providers: [
    StorageDealService,
    StorageDealResolve,

  ],
})
export class StorageDealModule { }
