import {Component, Input, OnInit, ViewChildren, OnChanges, SimpleChanges} from '@angular/core';

@Component ({
  selector: 'app-add-direction-table',
  templateUrl: 'add-direction-table.component.html',
  styleUrls: ['add-direction-table.component.css'],
})

export class AddDirectionTableComponent implements OnInit, OnChanges {

  @Input() directions?;
  @Input() showOnly = false;

  @ViewChildren('parentInput') parentInput;


  ngOnInit () {
    this.checkJson(this.directions);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.directions && changes.directions.currentValue) {
      this.checkJson(changes.directions.currentValue);
    }
  }

  public addDirection () {
    if (!this.directions) {
      this.directions = [];
    }
    this.directions.push(['', '', '']);
  }

  public removeDirection (index) {
    this.directions.splice(index, 1);
  }

  public getInputValue ( ) {
    const dataValue = [];
    this.parentInput['_results'].forEach( (item, index) => {
      dataValue[index] = [];
      item.nativeElement.querySelectorAll('input').forEach( (elem) => {
        dataValue[index].push(elem.value);
      });
    });
    return dataValue;
  }

  public checkJson (val) {
    if (typeof val === 'string') {
      this.directions = JSON.parse(val);
    }
  }

}
