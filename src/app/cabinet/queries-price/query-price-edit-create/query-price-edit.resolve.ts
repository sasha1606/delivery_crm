import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot,  Resolve } from '@angular/router';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';

import { QueriesPriceService } from '../queries-price.service';

@Injectable()

export class QueryPriceEditResolve implements Resolve<any> {

  constructor (
    private queriesPriceService: QueriesPriceService,
    private router: Router,
  ) {}

  resolve ( router: ActivatedRouteSnapshot ) {
    return this.queriesPriceService.getQueriesPrice(Number(router.paramMap.get('id'))).catch ( err => {
      this.router.navigateByUrl('/cabinet/queries-price');
      return Observable.of({error: err});
    });
  }

}
