import { Pagination } from '@shared/interface/pagination';

export interface QueriesGet {
    data: Queries;
    success: string;
    discount: number;
    fuel_markup: number;
}

export interface QueriesList {
    listData: ShortQueries[];
    success: string;
    listmanagers: object;
    pagination: Pagination;
}

export interface Queries {
    address_from: string;
    address_to: string;
    city_from: string;
    city_to: string;
    client_id: number;
    company: string;
    country_from: string;
    country_to: string;
    email: string;
    insurance: number;
    insurance_price: string;
    itemsParams: ItemsParams;
    items_count: number;
    items_totalweight: number;
    itemstypeNames: object;
    ordersinfoTable: object;
    items_price: number;
    name: string;
    phone: string;
    region_from: string;
    region_to: string;
    status: number;
    statusNames: object;
    type: string;
    typeNames: string;
    zip_code_from: string;
    zip_code_to: string;
    items_type: string;
    client_comment: string;
    delivery_price: string;
    rate: string;
    user_comment: string;
    insurance_currency: string;
    ItemscountFormated: string;
}

interface ShortQueries {
    countryCityFrom: string;
    countryCityTo: string;
    client_comment: string;
    created_at: number;
    delivery_price: string;
    insuranceName: string;
    items_count: string;
    items_totalweight: string;
    name: string;
    sizeFormated: string;
    statusName: string;
    typeName: string;
    managerName: string;
    id: number;
}

interface ItemsParams {
  client_comment_1: string;
  items_type_1: string;
  delivery_type_1: string;
  items_count_1: string;
  items_weight_1: string;
  items_length_1: string;
  items_width_1: string;
  items_height_1: string;
  items_price_1: string;
  items_price_currency_1: string;
  client_comment_2: string;
  items_type_2: string;
  delivery_type_2: string;
  items_count_2: string;
  items_weight_2: string;
  items_length_2: string;
  items_width_2: string;
  items_height_2: string;
  items_price_2: string;
  items_price_currency_2: string;
  client_comment_3: string;
  items_type_3: string;
  delivery_type_3: string;
  items_count_3: string;
  items_weight_3: string;
  items_length_3: string;
  items_width_3: string;
  items_height_3: string;
  items_price_3: string;
  items_price_currency_3: string;
  client_comment_4: string;
  items_type_4: string;
  delivery_type_4: string;
  items_count_4: string;
  items_weight_4: string;
  items_length_4: string;
  items_width_4: string;
  items_height_4: string;
  items_price_4: string;
  items_price_currency_4: string;
}
