import { Injectable } from '@angular/core';

@Injectable()

export class TokenStorageService {

    constructor () {}

    get token ()  {
        return localStorage.getItem('user-token');
    }

    set token (token: string) {
        localStorage.setItem('user-token', token);
    }

    deleteToken (): void {
        localStorage.removeItem('user-token');
    }
}
