import { Injectable } from '@angular/core';

@Injectable()

export class HelpersService {

  public changeFormatDateInForm (formVal, value) {
    const cloneObject = Object.assign({}, formVal);
    value.forEach((item) => {
      if (item && cloneObject[item]) {
        cloneObject[item] = +cloneObject[item] / 1000;
        // form.get(item).setValue(+form.get(item).value / 1000);
      }
    });
    return cloneObject;
  }

  public whoPay (type): boolean {
    if (type == '3_THIRDSIDE') {
      return true;
    }
    return false;
  }

  public getCheckboxIds (id: number, ids = []): number[] {
    const i = ids.indexOf(id);

    if (i === -1) {
      ids.push(id);
    } else  {
      ids.splice(i, 1);
    }

    return ids;
  }

}
