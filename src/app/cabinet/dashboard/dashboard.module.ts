import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';

import { DashboardComponent } from './dashboard.component';
import { DashboardAnalyticsComponent } from './dashboard-analytics/dashboard-analytics.component';
import { DashboardProfitComponent } from './dashboard-profit/dashboard-profit.component';
import { DashboardTaskComponent } from './dashboard-tasks/dashboard-task.component';
import { DashboardSalesAverageComponent } from './dashboard-sales-average/dashboard-sales-average.component';
import { DashboardAgentsComponent } from './dashboard-agents/dashboard-agents.component';
import { CarriersAgentsComponent } from './dashboard-carriers/dashboard-carriers.component';
import { DashboardManagersComponent } from './dashboard-managers/dashboard-managers.component';
import {DashboardOrdersComponent} from './dashboard-orders/dashboard-orders.component';

import { DashboardRoutingModule } from './dashboard-routing.module';

import { DashboardService } from './dashboard.service';
import { DashboardResolve } from './dashboard.resolve';
import { PipeModule } from '@shared/pipes/pipe.module';

@NgModule ({
    imports: [
        DashboardRoutingModule,
        CommonModule,
        ChartsModule,
        PipeModule,
    ],
    declarations: [
        DashboardComponent,
        DashboardAnalyticsComponent,
        DashboardProfitComponent,
        DashboardTaskComponent,
        DashboardSalesAverageComponent,
        DashboardAgentsComponent,
        CarriersAgentsComponent,
        DashboardManagersComponent,
        DashboardOrdersComponent
    ],
    providers: [
      DashboardService,
      DashboardResolve,
    ],
})

export class DashboardModule { }
