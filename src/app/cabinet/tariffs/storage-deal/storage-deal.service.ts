import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiSrc } from '@api/api.src';

import { ApiService } from '@api/api.service';

import { StorageDeal } from './storage-deal';

@Injectable()

export class StorageDealService {

  constructor (
    private apiService: ApiService,
  ) {}

  public getStorageDeal (): Observable<StorageDeal> {
    return this.apiService.get(ApiSrc.storageDeal.storage);
  }

}
