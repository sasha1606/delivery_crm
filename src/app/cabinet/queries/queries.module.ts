import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';

import { DatepickerModule } from '../shared-cabinet/datepicker/datepicker.module';
import { QueriesRoutingModule } from './queries-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';

import { QueriesComponent } from './queries.component';
import { QueryCreateComponent } from './query-create/query-create.component';
import { QueryShowComponent } from './query-show/query-show.component';
import { QueryEditComponent } from './query-edit/query-edit.component';

import { QueriesService } from './queries.service';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';

import { QueryShowResolve } from './query-show/query-show.resolve';
import { QueryCreateResolve } from './query-create/query-create.resolve';
import { QueriesResolve } from './queries.resolve';

import { PipeModule } from '@shared/pipes/pipe.module';
import { PaginationModule } from '@shared/pagination/pagination.module';
import { SearchTableModule } from '@shared/search-table/search-table.module';
import { CalculatorModule } from '@shared/calculator/calculator.module';
import { HistoryOrderTableModule } from '@shared/components/history-order-table/history-order-table.module';
import { AutocompleteCityModule } from '@shared/components/autocomplete-city/autocomplete-city.module';
import { ParcelFormParamsModule } from '@shared/components/parcel-form-params/parcel-form-params.module';
import { InsuranceFormElemModule } from '@shared/components/insurance-form-elem/insurance-form-elem.module';

@NgModule ({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        DatepickerModule,
        QueriesRoutingModule,
        PipeModule,
        PaginationModule,
        SearchTableModule,
        NgSelectModule,
        Ng2AutoCompleteModule,
        CalculatorModule,
        TextMaskModule,
        HistoryOrderTableModule,
        AutocompleteCityModule,
        ParcelFormParamsModule,
        InsuranceFormElemModule,
    ],
    declarations: [
        QueriesComponent,
        QueryShowComponent,
        QueryCreateComponent,
        QueryEditComponent,
    ],
    providers: [
        QueriesService,
        QueryShowResolve,
        QueryCreateResolve,
        QueriesResolve,
        FindToAutocompleteService,
    ]
})

export class QueriesModule { }
