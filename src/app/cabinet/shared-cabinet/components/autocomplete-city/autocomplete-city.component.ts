import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { debounceTime, switchMap, tap } from 'rxjs/operators';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';
import { AutocompleteCity } from '@shared/interface/autocomplete-city';

@Component({
  selector: 'app-autocomplete-city',
  templateUrl: './autocomplete-city.component.html',
  styleUrls: ['./autocomplete-city.component.css'],
})

export class AutocompleteCityComponent implements OnInit {

  @Input() form: FormGroup;
  @Input() fromData: AutocompleteCity;
  @Input() disableForm = false;
  @Input() controlLabelColClass = 'col-sm-4';
  @Input() formControlColClass = 'col-sm-8';

  private autocompleteSubject$ = new Subject();
  public autocompleteLoading = false;
  public local = [];

  constructor (
    private findToAutocompleteService: FindToAutocompleteService,
  ) {}

  ngOnInit(): void {
    this.autocompleteSubscribe();
    this.buildForm();

    if (this.disableForm) {
      this.form.disable();
    }
  }

  public onGetLocation(event): void {
    this.local = [];
    this.autocompleteSubject$.next(event.target.value);
  }

  public onSelectLocation(event): void | boolean {
    if (!event) {
      return false;
    }
    Object.keys(this.fromData.searchInfoAutocomplete).forEach( (item) => {
      if (this.fromData.searchInfoAutocomplete[item].searchEvent) {
        this.form.get(this.fromData.searchInfoAutocomplete[item].formControlName).setValue(event[this.fromData.searchInfoAutocomplete[item].searchEvent]);
      }
    });
    // if (this.fromData.searchInfoAutocomplete['country'].formControlName) {
    //   this.form.get(this.fromData.searchInfoAutocomplete['country'].formControlName).setValue(event.countryName);
    // }
    //
    // if (this.fromData.searchInfoAutocomplete['region'].formControlName) {
    //   this.form.get(this.fromData.searchInfoAutocomplete['region'].formControlName).setValue(event.region);
    // }
    //
    // if (this.fromData.searchInfoAutocomplete['zip_code'].formControlName) {
    //   this.form.get(this.fromData.searchInfoAutocomplete['zip_code'].formControlName).setValue(event.postal_code);
    // }
  }

  // https://github.com/ng-select/ng-select/blob/master/demo/app/examples/search.component.ts#L105
  private autocompleteSubscribe (): void {
    this.autocompleteSubject$.pipe(
      tap(() => this.autocompleteLoading = true),
      debounceTime(400),
      switchMap( (city) => {
        return this.findToAutocompleteService.findLocation(city);
      })
    ).subscribe(res => {
      this.local = res.data;
      this.autocompleteLoading = false;
    });
  }

  private buildForm () {
    this.form.addControl( this.fromData.searchSelectAutocomplete.formControlName , new FormControl( this.fromData.searchSelectAutocomplete.value));
    Object.keys(this.fromData.searchInfoAutocomplete).forEach( (item) => {
        this.form.addControl( this.fromData.searchInfoAutocomplete[item].formControlName , new FormControl( this.fromData.searchInfoAutocomplete[item].value));
    });
  }
}


