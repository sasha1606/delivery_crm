import { Component, OnInit, OnDestroy, Input, Output, EventEmitter} from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { CustomersUsersService } from '../../customers-users.service';

@Component ({
  selector: 'app-customer-user-orders-list',
  templateUrl: './customer-user-orders-list.component.html',
  styleUrls: ['./customer-user-orders-list.component.css']
})

export class CustomerUserOrdersListComponent implements OnInit, OnDestroy {

  @Input() ordersList;
  @Input() id;

  @Output() sendOrderIds: EventEmitter<any> = new EventEmitter;

  public title = 'История заказов:';
  public sort = '';
  public page = 1;
  public search = '';
  public date = '';
  public status = '';
  public invoiceStatus = '';

  public showTable = false;

  public isChecked = false;
  orderIds: number[] = [];

  private destroy$: Subject<boolean> = new Subject<boolean>();


  constructor (
    public customersUsersService: CustomersUsersService,
    public router: Router,
    private activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit () {
    // setInterval(() => {
    //   console.log(this.ordersList);
    // }, 5000);
  }

  ngOnDestroy () {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  public tablePage (page: number): void {
    this.page = page;
    this.sendSort();
  }

  public tableSort (param: string): void {
    if (this.sort.length > 0 && param === this.sort) {
      param = `-${param}`;
    }
    this.sort = param;
    this.sendSort();
  }

  public tableSearch (word: string): void {
    this.search = word;
    this.sendSort();
  }

  public getSearchData (data)  {
    this.date = data.data;
    this.search = data.word;
    this.status = data.anotherFilterSecond;
    this.invoiceStatus = data.anotherFilter;

    this.sendSort();
  }

  private sendSort (): void {
    this.customersUsersService.searchCustomersUserOrder({
      id: this.id,
      sort: this.sort,
      key: this.search,
      page: this.page,
      date: this.date,
      status: this.status,
      invoiceStatus:  this.invoiceStatus,
    }).takeUntil(this.destroy$).subscribe( orders => {
      this.ordersList = orders;
      this.showTable = true;
    });
  }

  public check (id: number): void {
    this.orderIds = this.customersUsersService.checkboxIds(id, this.orderIds);
    this.sendOrderIds.emit( this.orderIds );
  }
}
