import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnChanges,
  SimpleChanges,
  ViewChild,
  OnInit, ViewContainerRef
} from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CreateBillService } from '@shared/services/createBill.service';
import { ToastsManager } from 'ng2-toastr';

import { environment } from '../../../../../environments/environment';

@Component ({
  selector: 'app-create-bill-modal',
  templateUrl: './create-bill-modal.component.html',
  styleUrls: ['./create-bill-modal.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class CreateBillModalComponent implements OnChanges, OnInit {

  @Input() stateModalCreateBill: boolean;
  @Input() userId: number;
  @Input() email: string;
  @Output() emitCloseModalCreateBill: EventEmitter<boolean> = new EventEmitter();

  @ViewChild('modalView') modalView;

  public ordersList;
  public orderIds = [];
  public billLink: string;

  private modalRef: NgbModalRef;

  constructor (
    private currentModal: NgbModal,
    private createBillService: CreateBillService,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit () {
  }

  ngOnChanges (changes: SimpleChanges) {
    if (changes['stateModalCreateBill'].currentValue) {
      this.defaultData();
      this.openModalCreateBill();
    }
  }

  public openModalCreateBill (): void {
    setTimeout (() => {
      this.modalRef = this.currentModal.open(this.modalView, {size: 'lg'});
      this.modalRef.result.then(() => {
          this.sendModalCreateBill();
      }, () => {
          this.sendModalCreateBill();
      });
    });
  }

  public getDateToBill (date) {
    this.sendDate(date);
  }

  public openPreviewDocument ( ) {
    if (this.billLink) {
      this.openNewWindow(this.billLink);
    } else {
      this.createBillService.getLinkToDocument({orderIds: this.orderIds, userId: this.userId}).subscribe( response => {
        this.billLink = response.data.url;
        this.openNewWindow(response.data.url);
      });
    }
  }

  public triggerCheckboxId (event, orderId) {
    const index = this.orderIds.indexOf(orderId);
    this.billLink = '';
    if (event.target.checked) {
      this.orderIds.push(orderId);
    } else if (index >= 0)  {
      this.orderIds.splice(index, 1);
    }
  }

  public sendBill () {
    this.createBillService.sendBill({ordersIds: this.orderIds}).subscribe( response => {
      if (response.success === 'ok') {
        this.toastr.success('Successfully');
        setTimeout( () => {
          this.closeModal();
        }, 1000);
      }  else {
        this.toastr.error('Error');
      }
      // this.closeModal();
    });
  }

  public downloadDocument () {
    if (this.billLink) {
      this.createDownloadLink(this.billLink);
    } else {
      this.createBillService.getLinkToDocument({orderIds: this.orderIds, userId: this.userId}).subscribe( response => {
        this.billLink = response.data.url;
        this.createDownloadLink(response.data.url);
      });
    }
  }

  private sendModalCreateBill () {
    this.emitCloseModalCreateBill.emit(false);
  }

  private closeModal ( ) {
    this.modalRef.close();
  }

  private sendDate (date) {
    this.defaultData();
    this.createBillService.getListOrders({date, userId: this.userId }).subscribe( response => {
      this.ordersList = response.listdata;
    });
  }

  private defaultData () {
    this.ordersList = [];
    this.orderIds = [];
    this.billLink = '';
  }

  private openNewWindow (link) {
    console.log(`${environment.origin}${link}`);
    window.open(
      `${environment.origin}${link}`,
      'document',
      'location,width=490,height=368,top=0'
    );
  }

  private createDownloadLink (url) {
    const link = document.createElement('a');
    link.setAttribute('href',  `${environment.origin}${url}`);
    link.setAttribute('download', 'download');
    link.style.visibility = 'hidden';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

}
