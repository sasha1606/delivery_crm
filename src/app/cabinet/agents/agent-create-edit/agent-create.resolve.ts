import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { AgentsService } from '../agents.service';

@Injectable()

export class AgentCreateResolve implements Resolve<any> {

  constructor (
      private agentsService: AgentsService
  ) {}

  resolve () {
      return this.agentsService.dataCreateAgent();
  }
}
