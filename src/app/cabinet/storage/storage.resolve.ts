import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { StorageService } from './storage.service';

@Injectable()

export class StorageResolve implements Resolve<any> {

    constructor (
        private storageService: StorageService,
    ) {}

    resolve () {
        return this.storageService.getStoragesList();
    }

}
