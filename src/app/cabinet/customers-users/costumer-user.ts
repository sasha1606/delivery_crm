import { Pagination } from '@shared/interface/pagination';

export class CostumerUserGet {
    data: CostumerUser;
    status: string;
}

export interface CostumerUser {
    address: string;
    city: string;
    company: string;
    company_code: string;
    contract_tariff: string;
    country: string;
    discount: number;
    email: string;
    fio: string;
    login: string;
    phone: string;
    postal_address: string;
    region: string;
    type: number;
    typeNames: object;
    zip_code: string;

    email2?: string;
    status?: string;
    user_id?: number;
    mail_for_send?: string;
    managersNames?: object;
    phone2?: string;
    statusNames?: object;

}

export interface CostumerUserList {
    listmanagers: Managers[];
    listdata: ShortCostumerUser[];
    pagination: Pagination;
    status: string;
}

interface Managers {
    id: number;
    name: string;
}

interface ShortCostumerUser {
  address: string;
  city: string;
  company: string;
  company_code: string;
  contract_tariff: string;
  country: string;
  discount: number;
  email: string;
  fio: string;
  login: string;
  phone: string;
  postal_address: string;
  region: string;
  type: number;
  typeNames: object;
  zip_code: string;
}
