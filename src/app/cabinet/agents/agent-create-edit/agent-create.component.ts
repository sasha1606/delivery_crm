import { Component, OnInit, OnDestroy, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { AgentsService } from '../agents.service';

import { Agent, AgentGet, AgentModalGet, typePage } from '../agents';

import { FromValidation } from '@fromValidation';
import { maskPhone } from '@shared/const/masks';
import { ToastsManager } from 'ng2-toastr';

@Component ({
    selector: 'app-agent-create',
    templateUrl: './agent-create.component.html',
    styleUrls: ['./agent-create.component.css'],
})

export class AgentCreateComponent extends FromValidation implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    title: string;
    form: FormGroup;
    fromModal: FormGroup;
    modalRef: NgbModalRef;
    typePage: string;
    agentId: number;

    dynamicFormElem: object[] = [];

    agent: AgentGet;
    agentModal: AgentModalGet;

    public maskPhone =  maskPhone;

    constructor (
        private modalService: NgbModal,
        private activatedRoute: ActivatedRoute,
        public fb: FormBuilder,
        public agentsService: AgentsService,
        public router: Router,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef,
    ) {
      super(
        router,
        agentsService,
      );
      this.toastr.setRootViewContainerRef(vcr);
    }


    ngOnInit () {
        this.agent = this.activatedRoute.snapshot.data['agent'];
        this.typePage = this.activatedRoute.snapshot.data['data'];

        this.activatedRoute.params.subscribe(param => {
          this.agentId = +param['id'];
        });

        this.setTitle(this.typePage);

        this.buildForm();

        if (this.agent.data.directionsParams) {
            for (const key of Object.keys(this.agent.data.directionsParams)) {
              this.dynamicFormElem.push(this.agent.data.directionsParams[key]);
            }
        }
    }

    ngOnDestroy () {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

    public getInfoModal (content) {
        this.agentsService.dataAgentModal().takeUntil(this.destroy$).subscribe( res => {
          if (res.success === 'ok') {
            this.agentModal = res;
            this.buildModalForm();
            this.listeningResponsiblePerson();
            this.openModal(content);
          }
        });
    }

    public onSubmit (form): void {
      if (this.form.valid) {
        const objRequest: Agent | object = this.reformateRequestObj(this.form.value);

        let answer;
        if (this.typePage === typePage.edit) {
          answer = this.agentsService.editAgent({agent: objRequest,
                                                  id: this.agentId
                                                }).takeUntil(this.destroy$);
        } else if (this.typePage === typePage.create) {
          answer =  this.agentsService.createAgent( objRequest).takeUntil(this.destroy$);
        }

        answer.subscribe( res => {
          if (res.success === 'ok') {
            this.toastr.success('Has been edit successfully');
            setTimeout(() => {
              this.router.navigateByUrl('/cabinet/agents');
            }, 1000);
          }  else {
            this.toastr.error('Error');
            if (res.validation['model']) {
              this.validationFormBackEnd(res.validation['model'], this.form);
            }

            if (res.validation['dynamic_models']) {
              for (const key of Object.keys(res.validation['dynamic_models'])) {
                this.validationFormBackEnd(
                  {[key]: res.validation['dynamic_models'][key]},
                  this.form
                );
              }
            }
          }
        });

      } else {
        this.toastr.error('Error');
        this.validationFormsSubmit(this.form);
      }
    }

    public onSubmitModal (form): void {
      if (this.fromModal.valid) {
        this.agentsService.createAgentModal(this.fromModal.value).takeUntil(this.destroy$).subscribe( res => {
          if (res.success === 'ok') {
            this.modalRef.close();
            this.dynamicFormElem.push(res.data);
          }  else {
            this.validationFormBackEnd(res.validation, this.fromModal);
          }
        });
      } else {
        this.validationFormsSubmit(this.fromModal);
      }
    }

    public deleteDynamicElem(item: string): void {
      this.form.removeControl(item);
      let index: any =  item.indexOf('_');
      index = item.slice(index + 1);
      delete this.dynamicFormElem[index - 1];
    }

    private buildForm () {
        this.form = this.fb.group({
            name: [this.agent.data.name, [
                Validators.required
            ]],
            email: [this.agent.data.email, [
              Validators.required
            ]],
            phone: [this.agent.data.phone, [
              Validators.required
            ]],
            // typeParams: ['', [
            //     Validators.required
            // ]],
            responsible_person: this.agent.data.responsible_person,
        });
    }

    private buildModalForm () {
      this.fromModal = this.fb.group({
          name: ['', [
            Validators.required
          ]],
          email: ['', [
            Validators.required
          ]],
          phone: ['', [
            Validators.required
          ]],
        // typeParams: ['', [
        //     Validators.required
        // ]],
          responsible_person: '',
          comment: ''
      });
    }

    private openModal (content) {
      this.modalRef = this.modalService.open(content, {size: 'lg'});
    }

    private reformateRequestObj (obj) {
      const clone = {};
      for (const key  in obj) {
          if (Object.prototype.toString.call(obj[key]) === '[object Object]') {
            if (!clone['directionsParams']) {
              clone['directionsParams'] = {};
            }
            clone['directionsParams'][key] = obj[key];
          } else {
            clone[key] = obj[key];
          }
      }
      return clone;
    }

    private setTitle (type) {
      this.title = 'Добавить агента';
      if (type === typePage.edit) {
        this.title = ' Редактировать агента';
      }
    }

    private listeningResponsiblePerson () {
        this.fromModal.get('responsible_person').valueChanges.subscribe(val => {
            for ( const key of Object.keys(this.form.value) ) {
                if ( Object.prototype.toString.call(this.form.value[key]) === '[object Object]'  &&
                     this.form.value[key].responsible_person  === val ) {
                      this.fromModal.get('email').setValue(this.form.value[key].email);
                      this.fromModal.get('phone').setValue(this.form.value[key].phone);
                }
            }
        });
    }
}
