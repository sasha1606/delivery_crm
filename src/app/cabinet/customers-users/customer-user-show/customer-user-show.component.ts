import { Component, OnInit, OnDestroy, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { typePage } from '@shared/const/crud';

import { CustomersUsersService } from '../customers-users.service';

import { CostumerUserGet } from '../costumer-user';
import { ToastsManager } from 'ng2-toastr';

import { CustomerUserInfo  } from '@shared/interface/user-info';

@Component ({
    selector: 'app-customer-user-show',
    templateUrl: './customer-user-show.component.html',
    styleUrls: ['./customer-user-show.component.css']
})

export class CustomerUserShowComponent implements OnInit, OnDestroy {

    private destroy$: Subject<boolean> = new Subject<boolean>();

    public title = 'Просмотр пользователя';
    public form: FormGroup;

    public date = new Date();
    public newDate;

    public ordersList;
    public orderIds = [];

    public typePage;

    public user: CostumerUserGet;

    public userId: any;
    public modal: any;
    public modalTemplate: string;

    public modalRef: NgbModalRef;

    public triggerModalCreateBill = false;

    public currentEmail = '';

    constructor (
        private customersUsersService: CustomersUsersService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalService: NgbModal,
        private fb: FormBuilder,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef,
    ) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit () {
        this.activatedRoute.params.subscribe(param => {
            this.userId = +param['id'];
            this.getOrdersList();
        });
        this.user = this.activatedRoute.snapshot.data['user'];
        this.buildForm();
        this.form.disable();
        this.typePage = typePage.show;
    }

    ngOnDestroy () {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

    public getOrderIds (ids) {
      this.orderIds = ids;
    }

    public issueInvoice () {

      this.customersUsersService.sendIssueInvoice({ordersIds: this.orderIds}).takeUntil(this.destroy$).subscribe(res => {
        if (res.success === 'ok') {
          this.toastr.success('Successfully');
        }  else {
          this.toastr.error('Error');
        }
      });
    }

    public getDate (date): void {
        this.newDate = date;
    }

    public open (content): void {
       this.modalRef = this.modalService.open(content, {size: 'lg'});
    }

    public getTemplate (modal): void {
        this.customersUsersService.getTemplate(this.userId).takeUntil(this.destroy$).subscribe(res => {
            this.modalTemplate = res.template;
            this.open(modal);
        });

    }

    public sendLetter (): void {
        this.customersUsersService.sentLetter({clientsIds: [this.userId]}).takeUntil(this.destroy$).subscribe(res => {
            this.modalRef.close();
            // this.sendLetterTooltip = true;
            if (res.success === 'ok') {
              this.toastr.success('Successfully');
            }  else {
              this.toastr.error('Error');
            }
        },
        (error) => {
            console.error(error);
        },
        () => {
        });
    }

    public openModalCreateBill (email) {
      this.triggerModalCreateBill = true;
      this.currentEmail = email;
    }

    public closeModalCreateBill () {
      this.triggerModalCreateBill = false;
    }

    public navigateToUrl (event, url) {
      event.preventDefault();
      const userInfo: CustomerUserInfo = {
        fio: this.form.get('fio').value,
        company: this.form.get('company').value,
        phone: this.form.get('phone').value,
        email: this.form.get(this.form.get('mail_for_send').value).value,
      };
      this.router.navigate([url], { queryParams: userInfo });
    }


    private buildForm (): void {
      this.form = this.fb.group({
        type: [this.user.data.type],
        status: [this.user.data.status],
        login: [this.user.data.login],
        fio: [this.user.data.fio],
        email: [this.user.data.email],
        phone: [this.user.data.phone],
        company: [this.user.data.company],
        company_code: [this.user.data.company_code],
        country: [this.user.data.country],
        city: [this.user.data.city],
        address: [this.user.data.address],
        postal_address: [this.user.data.postal_address],
        region: [this.user.data.region],
        zip_code: [this.user.data.zip_code],
        discount: [this.user.data.discount],
        contract_tariff: [this.user.data.contract_tariff],
        email2: [this.user.data.email2],
        mail_for_send: [this.user.data.mail_for_send],
        phone2: [this.user.data.phone2],
        user_id: [this.user.data.user_id],
        filesUpload: '',
      });
    }

    private getOrdersList () {
      this.customersUsersService.getCustomersUserOrderList(this.userId).subscribe( response => {
        this.ordersList = response;
      });
    }
}
