import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';

import { DashboardResolve } from './dashboard.resolve';


const ROUTERS: Routes = [
    {
        path: '',
        component: DashboardComponent,
        resolve: {
          dashboardInfo: DashboardResolve,
        }
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class DashboardRoutingModule {}
