import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ModalAutocompleteComponent } from './modal-autocomplete.component';

import { PipeModule } from '@shared/pipes/pipe.module';
import { AutocompleteService } from '@shared/services/autocomplete.service';
import { StorageService } from '../../storage/storage.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PipeModule,
  ],
  declarations: [
    ModalAutocompleteComponent
  ],
  exports: [
    ModalAutocompleteComponent
  ],
  providers: [
    AutocompleteService,
    StorageService,
  ]
})

export class ModalAutocompleteModule {

}
