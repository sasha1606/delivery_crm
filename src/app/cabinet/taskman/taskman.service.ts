import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiSrc } from '@api/api.src';

import { ApiService } from '@api/api.service';

import { TaskmanGet, TaskmanModalGet } from './taskman';

@Injectable()

export class TaskmanService {

  constructor (
    private apiService: ApiService,
  ) {}

  public getTasks (): Observable<TaskmanGet> {
      return this.apiService.get(ApiSrc.tasks.list);
  }

  public editTasks ({id: id, value: value}): Observable<TaskmanGet> {
    return this.apiService.post(`${ApiSrc.tasks.edit}/${id}`, {status: value});
  }

  public creataTasks (): Observable<TaskmanModalGet> {
    return this.apiService.get(ApiSrc.tasks.create);
  }

  public getMonthCount (): Observable<TaskmanModalGet> {
    return this.apiService.get(ApiSrc.tasks.monthcount);
  }

  public getMonthOfDate (date): Observable<TaskmanModalGet> {
    return this.apiService.get(ApiSrc.tasks.monthcount + '?date=' + date);
  }

  public getFilterOfDate (date): Observable<TaskmanModalGet> {
    return this.apiService.get(ApiSrc.tasks.list + '?date=' + date);
  }


  public updateTasks (task): Observable<any> {
    return this.apiService.post(ApiSrc.tasks.create, task);
  }

  public sort ({page: page = 1, tasksfilter: tasksfilter = '', userId: userId = ''}): Observable<TaskmanGet> {
    return this.apiService.get(`${ApiSrc.tasks.list}?tasksfilter=${tasksfilter}&page=${page}&user_id=${userId}`);
  }
}
