import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgentsComponent } from './agents.component';
import { AgentCreateComponent } from './agent-create-edit/agent-create.component';
import { AgentShowComponent } from './agent-show/agent-show.component';

import { AgentsResolve } from './agents.resolve';
import { AgentCreateResolve } from './agent-create-edit/agent-create.resolve';
import { AgentGetResolve } from './agent-get.resolve';

import { typePage } from './agents';

const ROUTERS: Routes = [
    {
      path: '',
      component: AgentsComponent,
      resolve: {
        agents: AgentsResolve,
      }
    },
    {
      path: 'create',
      component: AgentCreateComponent,
      resolve: {
        agent: AgentCreateResolve,
      },
      data: {
        data: typePage.create,
      }
    },
    {
      path: 'show/:id',
      component: AgentShowComponent,
      resolve: {
        agent: AgentGetResolve,
      }
    },
    {
      path: 'edit/:id',
      component: AgentCreateComponent,
      resolve: {
        agent: AgentGetResolve,
      },
      data: {
        data: typePage.edit,
      }
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class AgentsRoutingModule {}
