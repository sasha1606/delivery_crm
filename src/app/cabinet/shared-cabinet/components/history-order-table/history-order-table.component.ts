import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component ({
  selector: 'app-history-order-table',
  templateUrl: 'history-order-table.component.html',
  styleUrls: ['history-order-table.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class HistoryOrderTableComponent implements OnInit{

  @Input() history;

  ngOnInit () {

  }

}
