import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';

import { StorageRoutingModule } from './storege-routing.module';

import { StorageResolve } from './storage.resolve';
import { StorageShowResolve } from './storage-show/storage-show.resolve';
import { StorageEditResolve } from './storage-edit/storage-edit.resolve';

import { StorageService } from './storage.service';
import { HelpersService } from '@shared/services/helpers.service';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';

import { StorageComponent } from './storage.component';
import { StorageEditComponent } from './storage-edit/storage-edit.component';
import { StorageShowComponent } from './storage-show/storage-show.component';

import { PipeModule } from '@shared/pipes/pipe.module';
import { PaginationModule } from '@shared/pagination/pagination.module';
import { SearchTableModule } from '@shared/search-table/search-table.module';
import { FormAutocompleteModule } from '@shared/form-autocomplete/form-autocomplete.module';
import { FormFileUploadModule } from '@shared/form-file-upload/form-file-upload.module';
import { DatepikerDirectiveModule } from '@shared/directives/datepiker.directive.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { DatepickerModule } from '@shared/datepicker/datepicker.module';
import { ModalAutocompleteModule } from '@shared/modal-autocomplete/modal-autocomplete.module';
import { HistoryOrderTableModule } from '@shared/components/history-order-table/history-order-table.module';
import { AutocompleteCityModule } from '@shared/components/autocomplete-city/autocomplete-city.module';
import { ParcelFormParamsModule } from '@shared/components/parcel-form-params/parcel-form-params.module';
import {InsuranceFormElemModule} from '@shared/components/insurance-form-elem/insurance-form-elem.module';

@NgModule ({
    imports: [
        CommonModule,
        StorageRoutingModule,
        PipeModule,
        PaginationModule,
        SearchTableModule,
        FormAutocompleteModule,
        DatepikerDirectiveModule,
        NgSelectModule,
        Ng2AutoCompleteModule,
        DatepickerModule,
        ReactiveFormsModule,
        FormsModule,
        ModalAutocompleteModule,
        FormFileUploadModule,
        TextMaskModule,
        HistoryOrderTableModule,
        AutocompleteCityModule,
        ParcelFormParamsModule,
        InsuranceFormElemModule,
    ],
    declarations: [
        StorageComponent,
        StorageEditComponent,
        StorageShowComponent,
    ],
    providers: [
        StorageResolve,
        StorageShowResolve,
        StorageEditResolve,
        StorageService,
        HelpersService,
        FindToAutocompleteService,
    ]
})

export class StorageModule {

}
