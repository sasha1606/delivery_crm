import { TariffsZones } from '../shared-tariffs/tariffs-zones/tariffs-zones';
import { TariffsTransporters } from '../shared-tariffs/tariffs-transporters/tariffs-transporters';

export interface TariffsDkl {
  success: string;
  zones: TariffsZones[];
  documents: TariffsTransporters[];
  documents_packets: TariffsTransporters[];
  packets: TariffsTransporters[];
}
