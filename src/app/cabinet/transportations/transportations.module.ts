import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { TextMaskModule } from 'angular2-text-mask';
import {NgSelectModule} from '@ng-select/ng-select';

import { TransportationsRoutingModule } from './transportations-routing.module';

import { TransportationsService } from './transportations.service';

import { TransportationCreateResolve } from './transportation-create-edit/transportation-create.resolve';
import { TransportationsResolve } from './transportations.resolve';
import { TransportationGetResolve } from './transportation-get.resolve';

import { TransportationsComponent } from './transportations.component';
import { TransportationCreateComponent } from './transportation-create-edit/transportation-create.component';
import { TransportationShowComponent } from './transportation-show/transportation-show.component';
import { TransportationEditComponent } from './transportation-edit_delete/transportation-edit.component';
import { TransportationFormElementComponent } from './transportation-form-element/transportation-form-element.component';
import { TransportationFormElementShowComponent } from './transportation-form-element-show/transportation-form-element-show.component';

import { SelectModule } from '@shared/select/select.module';
import { MultiSelectModule } from '@shared/multi-select/multi-select.module';
import { PaginationModule } from '@shared/pagination/pagination.module';
import { PipeModule } from '@shared/pipes/pipe.module';
import { FormAutocompleteModule } from '@shared/form-autocomplete/form-autocomplete.module';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';
import { QueriesPriceListService } from '@shared/services/queries-price-list.service';
import { AutocompleteCityModule } from '@shared/components/autocomplete-city/autocomplete-city.module';
import { ParcelFormParamsModule } from '@shared/components/parcel-form-params/parcel-form-params.module';
import { InsuranceFormElemModule } from '@shared/components/insurance-form-elem/insurance-form-elem.module';

@NgModule ({
    imports: [
        CommonModule,
        TransportationsRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        SelectModule,
        MultiSelectModule,
        MultiselectDropdownModule,
        PaginationModule,
        PipeModule,
        TextMaskModule,
        FormAutocompleteModule,
        NgSelectModule,
        AutocompleteCityModule,
        ParcelFormParamsModule,
        InsuranceFormElemModule,
    ],
    declarations: [
        TransportationsComponent,
        TransportationCreateComponent,
        TransportationShowComponent,
        TransportationEditComponent,
        TransportationFormElementComponent,
        TransportationFormElementShowComponent,
    ],
    providers: [
        TransportationsService,
        TransportationCreateResolve,
        TransportationsResolve,
        TransportationGetResolve,
        FindToAutocompleteService,
        QueriesPriceListService,
    ]
})

export class TransportationsModule {}
