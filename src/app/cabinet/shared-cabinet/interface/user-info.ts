export interface UserInfoGet {
    data: UserInfo;
    success: string;
}

export interface UserInfo {
    firstname: string;
    lastname: string;
    fullname: string;
    email: string;
    phone: string;
    role: string;
    type: string;
}

export interface CustomerUserInfo {
  fio: string;
  company: string;
  phone: string;
  email: string;
}
