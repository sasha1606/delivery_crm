import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

import { OrdersService } from '../orders.service';

@Injectable()

export class OrderShowResolve implements Resolve<any> {

  constructor (
    private ordersService: OrdersService,
    private router: Router
  ) {}

  resolve ( activatedRouteSnapshot: ActivatedRouteSnapshot ) {
    return this.ordersService.getOrders(Number(activatedRouteSnapshot.paramMap.get('id'))).catch(err => {
      this.router.navigateByUrl('/cabinet/orders');
      return Observable.of({error: err});
    });
  }

}
