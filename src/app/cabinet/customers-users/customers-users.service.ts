import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiService } from '@api/api.service';

import { ApiSrc } from '@api/api.src';

import { CostumerUser, CostumerUserGet, CostumerUserList } from './costumer-user';
import { Response } from '@shared/interface/response';

@Injectable()

export class CustomersUsersService {

    constructor (
        private apiService: ApiService
    ) {}

    public getUsersList (): Observable<CostumerUserList> {
        return this.apiService.post(ApiSrc.customerUsers.list);
    }

    public search (sort: string = '', key: string = '', page: number = 1, date = '', orders = '', userId = ''): Observable<CostumerUserList> {
        // for IE
        const  keyword = encodeURIComponent(key);
        return this.apiService.get(`${ApiSrc.customerUsers.list}?sort=${sort}&keyword=${keyword}&page=${page}&date=${date}&orders=${orders}&user_id=${userId}`);
    }

    public sentLetter (ids: object): Observable<Response> {
        return this.apiService.post(ApiSrc.customerUsers.sentLetter, ids);
    }

    public getTemplate (id: number): Observable<any> {
        return this.apiService.post(ApiSrc.customerUsers.getTemplate, {client_id: id});
    }



    public createUser (user: CostumerUser): Observable<Response> {
        return this.apiService.post(ApiSrc.customerUsers.createUser, user);
    }

    public dataCreateUser (): Observable<CostumerUserGet> {
        return this.apiService.get(ApiSrc.customerUsers.createUser);
    }

    public showUser (userId: number): Observable<CostumerUserGet> {
      return this.apiService.get(`${ApiSrc.customerUsers.showUser}/${userId}`);
    }

    public getUser ( userId: number ): Observable<CostumerUserGet> {
        return this.apiService.get(`${ApiSrc.customerUsers.editUser}/${userId}`);
    }

    public updateUser (user: CostumerUser, userId: number): Observable<Response> {
        return this.apiService.post(`${ApiSrc.customerUsers.editUser}/${userId}`, user);
    }



    public getCustomersUserOrderList (id): Observable<any> {
      return this.apiService.post(`${ApiSrc.customersUserOrder.list}/${id}`);
    }

    public searchCustomersUserOrder ({id, sort = '', key = '', page = 1, date = '', orders = '', status = '', invoiceStatus = ''}): Observable<any> {
      return this.apiService.get(`${ApiSrc.customersUserOrder.list}/${id}?sort=${sort}&keyword=${key}&page=${page}&date=${date}&orders=${orders}&status=${status}&invoice_status=${invoiceStatus}`);
    }

    // public sentLetterCustomersUserOrder (ids: object): Observable<Response> {
      // return this.apiService.post(ApiSrc.customerUsers.sentLetter, ids);
    // }

  public sendIssueInvoice (ids): Observable<Response> {
    return this.apiService.post(ApiSrc.customerUsers.issueInvoice, ids);
  }


    public checkboxIds (id: number, ids = []): number[] {
      const i = ids.indexOf(id);

      if (i === -1) {
        ids.push(id);
      } else  {
        ids.splice(i, 1);
      }

      return ids;
    }
}
