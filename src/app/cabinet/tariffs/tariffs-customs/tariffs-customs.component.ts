import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { TariffsCustoms } from './tariffs-customs';

import { TariffsCustomsService } from './tariffs-customs.service';

@Component({
  selector: 'app-tariffs-customs',
  templateUrl: './tariffs-customs.component.html',
  styleUrls: ['./tariffs-customs.component.css']
})

export class TariffsCustomsComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();

  title = 'Таможенные тарифы';
  tariffs: TariffsCustoms;
  sort = '';

  constructor (
    private tariffsCustomsService: TariffsCustomsService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.tariffs = this.activatedRoute.snapshot.data['tariffs'];
  }

  ngOnDestroy () {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  public tableSort (param: string): void {
    if (this.sort.length > 0 && param === this.sort) {
      param = `-${param}`;
    }
    this.sort = param;
    this.sendSort();
  }

  private sendSort (): void {
    this.tariffsCustomsService.sortTariffsCustoms(this.sort).takeUntil(this.destroy$).subscribe( res => {
      this.tariffs = res;
    });
  }

}
