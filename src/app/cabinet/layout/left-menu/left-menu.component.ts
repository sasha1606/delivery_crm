import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, Event } from '@angular/router';
import 'rxjs/add/operator/pairwise';

import { LeftMenuInterface } from './left-menu.interface';

import { UserInfoService } from '../../user.info.service';

import { userRole } from '@shared/const/user-role';

import { Observable } from 'rxjs/Observable';
import {map, switchMap, tap, mergeMap, skipWhile, takeUntil} from 'rxjs/operators';
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/timer';
// import { of } from 'rxjs/observable/of';

import {OrdersCountService} from '@shared/services/orders-count.service';
import {Subject} from 'rxjs/Subject';

@Component ({
    selector: 'app-left-menu',
    templateUrl: './left-menu.component.html',
    styleUrls: ['./left-menu.component.css'],
})

export class LeftMenuComponent implements OnInit, OnDestroy {

    // @Input ()
    // stateMenu: boolean

    // public userRole;
    // public constUserRole = userRole;
    private destroy$: Subject<boolean> = new Subject<boolean>();

    menuElems: Array<LeftMenuInterface> = [
        {
            name: 'Dashboard',
            link: '/cabinet/dashboard',
            icon: 'fa-home',
        },
        {
            name: 'Пользователи',
            link: '',
            icon: 'fa-user',
            subMenu: [
                {
                    name: 'Клиенты',
                    link: '/cabinet/customers-users',
                },
                {
                    name: 'Андмин персонал',
                    link: '/cabinet/admin-users',
                    permission: [userRole.admin],
                },
            ],
        },
        {
            name: 'Запросы от клиентов',
            link: '/cabinet/queries',
            icon: 'fa-home',
        },
        {
            name: 'Заказы',
            link: '/cabinet/orders',
            icon: 'fa-map',
            count: 0,
        },
        {
            name: 'Склад',
            link: '/cabinet/storage',
            icon: 'fa-signal',
        },
        {
            name: 'Задачник',
            link: '/cabinet/taskman',
            icon: 'fa-calendar',
        },
        {
            name: 'Запросы перевозчикам',
            link: '/cabinet/queries-price',
            icon: 'fa-usd',
        },
        {
            name: 'Перевозчики',
            link: '/cabinet/transportations',
            icon: 'fa-truck',
        },
        {
            name: 'Агенты',
            link: '/cabinet/agents',
            icon: 'fa-users',
        },
        {
            name: 'Курьеры',
            link: '/cabinet/couriers',
            icon: 'fa-archive',
        },
        {
            name: 'Бухгалтерия',
            link: '',
            icon: 'fa-clipboard',
            subMenu: [
                {
                    name: 'Бухгалтерия',
                    link: '/cabinet/accounting',
                },
                {
                    name: 'Аналитика',
                    link: '/cabinet/analytics',
                },
            ],
        },
        {
            name: 'Тарифный справочник',
            link: '',
            icon: 'fa-pie-chart',
            subMenu: [
                {
                    name: 'Тарифы DKL',
                    link: '/cabinet/tariffs-dkl',
                },
                {
                    name: 'Тарифы перевозчиков',
                    link: '/cabinet/tariffs-transportations',
                },
                {
                    name: 'Складские услуги',
                    link: '/cabinet/storage-deal',
                },
                {
                    name: 'Таможенные тарифы',
                    link: '/cabinet/tariffs-customs',
                },
            ],
        },
    ];

    constructor (
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private userInfoService: UserInfoService,
        private ordersCountService: OrdersCountService,
    ) {}

    ngOnInit () {
        this.getCountNewOrders();
        this.activatedRoute.url.subscribe( url => {
            const elem = Array.from(document.querySelectorAll('.nav-second-level'));
            if (elem) {
                elem.forEach( (item, i) => {
                    item.closest('li').classList.remove('active');
                    item.classList.remove('in');
                });
            }
        });
        // this.getUserRole();

        // this.router.events.subscribe( (event: Event) => {
        //     if (event instanceof NavigationEnd) {
        //         const elem = Array.from(document.querySelectorAll('.nav-second-level'));
        //         if (elem) {
        //             elem.forEach( (item, i) => {
        //                 item.closest('li').classList.remove('active');
        //                 item.classList.remove('in');
        //             });
        //         }
        //     }
        // });
    }

    ngOnDestroy(): void {
      this.destroy$.next(true);
      this.destroy$.unsubscribe();
    }

    toggleMenu (event) {
          event.preventDefault();
          const parent = event.target.closest('li');
          parent.classList.toggle('active');
          parent.querySelector('.nav-second-level').classList.toggle('in');
      }

      // private getUserRole () {
      //   this.userInfoService.getUserInfo().subscribe( response => {
      //     this.userRole = response.data.type;
      //   });
      // }

      // private isAdminRole () {
      //   if (this.userRole === this.constUserRole.USERTYPE_ADMIN) {
      //     return true;
      //   }
      //   return false;
      // }

      private getCountNewOrders () {
        Observable.timer(0, 10000).pipe(
          takeUntil(this.destroy$),
        //   // tap(data => console.log(123)),
        //   // map( item => item),
        //   // skipWhile( () => a),
        //   // mergeMap( (data) => {
        //   //   return of('data');
        //   // }),
          switchMap( () => {
        //
             return this.ordersCountService.getCountsNewOrders();
          }),
        ).subscribe( (response) => {
          this.menuElems.map((item) => {
            if (item.name === 'Заказы') {
              item.count = response[0].count;
              return item;
            }
            return item;
          });
        });
    }
}
