import { Component, OnInit, OnDestroy, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormControl, FormBuilder, FormGroup} from '@angular/forms';
import { typePage } from '@shared/const/crud';

import { OrdersGet } from '../orders';
import { HelpersService } from '@shared/services/helpers.service';
import { OrdersService } from '../orders.service';
import { Subject } from 'rxjs/Subject';
import { ToastsManager } from 'ng2-toastr';

@Component ({
  selector: 'app-order-show',
  templateUrl: './order-show.component.html',
  styleUrls: ['./order-show.component.css']
})

export class OrderShowComponent implements OnInit, OnDestroy {

  private  destroy$: Subject<boolean> = new Subject<boolean>();

  public title = 'Просмотр заказа';
  public form: FormGroup;
  public isWhoPay = false;
  public typePage;

  public order: OrdersGet;
  public idUser: number;

  public orderHistory;

  public fromNames = {
    type: {
      name: 'transporter_type',
      value: '',
    },
    id: {
      name: 'transporter_id',
      value: '',
    },
    direction_id: {
      name: 'transporter_direction_id',
      value: '',
    },
    direction_name: {
      name: 'transporter_direction_name',
      value: '',
    },
    direction_phone: {
      name: 'transporter_direction_phone',
      value: '',
    },
    direction_email: {
      name: 'transporter_direction_email',
      value: '',
    },
    direction_resperson: {
      name: 'transporter_direction_resperson',
      value: '',
    },
  };

  constructor (
    public router: Router,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private helpersService: HelpersService,
    public ordersService: OrdersService,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit () {
    this.order = this.activatedRoute.snapshot.data['order'];
    this.orderHistory = this.order.data.ordersinfoTable;
    this.activatedRoute.params.subscribe(param => {
      this.idUser = +param['id'];
    });
    this.buildFrom();
    this.form.disable();
    this.checkWhoPaythis();
    this.setValueAutocomplete();
    this.typePage = typePage.show;
  }

  ngOnDestroy () {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  public sendAgain () {
      this.ordersService.sendAgain(this.idUser).takeUntil(this.destroy$).subscribe( (response) => {
        if (response.success) {
          this.toastr.success('Successfully');
        } else {
          this.toastr.error('Error');
        }
      });
  }

  private buildFrom (): void {
    this.form = this.fb.group({
      code_dkl: this.order.data.code_dkl,
      code_transporter: this.order.data.code_transporter,
      name: this.order.data.name,
      courier_id: this.order.data.courier_id,
      company: this.order.data.company,
      email: this.order.data.email,
      phone: this.order.data.phone,
      city_from: this.order.data.city_from,
      country_from: this.order.data.country_from,
      region_from: this.order.data.region_from,
      zip_code_from: this.order.data.zip_code_from,
      name_receiver: this.order.data.name_receiver,
      company_receiver: this.order.data.company_receiver,
      email_receiver: this.order.data.email_receiver,
      phone_receiver: this.order.data.phone_receiver,
      city_to: this.order.data.city_to,
      country_to: this.order.data.country_to,
      region_to: this.order.data.region_to,
      zip_code_to: this.order.data.zip_code_to,
      type: this.order.data.type,
      // insurance: this.order.data.insurance,
      // insurance_currency: this.order.data.insurance_currency,
      // insurance_price: this.order.data.insurance_price,
      export_cause: this.order.data.export_cause,
      export_terms: this.order.data.export_terms,
      stock_pay_type: this.order.data.stock_pay_type,
      stock_pay: this.order.data.stock_pay,
      stock_price: this.order.data.stock_price,
      stock_price_currency: this.order.data.stock_price_currency,
      stock_comment: this.order.data.stock_comment,
      stock_delivery_fact_pay: this.order.data.stock_delivery_fact_pay,
      delivery_price_currency: this.order.data.delivery_price_currency,
      delivery_price: this.order.data.delivery_price,
      // pickup_date: this.order.data.pickup_date,
      pickup_timefrom: this.order.data.pickup_timefrom,
      pickup_timeto: this.order.data.pickup_timeto,
      client_comment: this.order.data.client_comment,
      items_count: this.order.data.items_count,
      items_totalweight: this.order.data.items_totalweight,
      items_type: this.order.data.items_type,
      items_price: this.order.data.items_price,
      items_price_currency: this.order.data.items_price_currency,
      delivery_type: this.order.data.delivery_type,
      status: this.order.data.status,
      thirdside_name: this.order.data.thirdside_name,
      thirdside_company: this.order.data.thirdside_company,
      thirdside_email: this.order.data.thirdside_email,
      thirdside_phone: this.order.data.thirdside_phone,
      thirdside_city: this.order.data.thirdside_city,
      thirdside_country: this.order.data.thirdside_country,
      thirdside_region: this.order.data.thirdside_region,
      thirdside_zip_code: this.order.data.thirdside_zip_code,
      invoice_number: this.order.data.invoice_number,
      invoice_date: this.order.data.invoice_date,
      invoice_status: this.order.data.invoice_status,
      stock_date_come: this.order.data.stock_date_come,
      stock_delivery_type: this.order.data.stock_delivery_type,
      stock_date_send: this.order.data.stock_date_send,
      stock_person_comment: this.order.data.stock_person_comment,
      user_comment: this.order.data.user_comment,
      stock_date_delivery: this.order.data.stock_date_delivery,
      stock_delivery_time: this.order.data.stock_delivery_time,
      stock_person: this.order.data.stock_person,
      // itemsParams: this.fb.group({
      //   client_comment_1: this.order.data.itemsParams ? this.order.data.itemsParams.client_comment_1 : '',
      //   items_type_1: this.order.data.itemsParams ? this.order.data.itemsParams.items_type_1 : '',
      //   delivery_type_1: this.order.data.itemsParams ? this.order.data.itemsParams.delivery_type_1 : '',
      //   items_count_1: this.order.data.itemsParams ? this.order.data.itemsParams.items_count_1 : '',
      //   items_weight_1: this.order.data.itemsParams ? this.order.data.itemsParams.items_weight_1 : '',
      //   items_length_1: this.order.data.itemsParams ? this.order.data.itemsParams.items_length_1 : '',
      //   items_width_1: this.order.data.itemsParams ? this.order.data.itemsParams.items_width_1 : '',
      //   items_height_1: this.order.data.itemsParams ? this.order.data.itemsParams.items_height_1 : '',
      //   items_price_1: this.order.data.itemsParams ? this.order.data.itemsParams.items_price_1 : '',
      //   items_price_currency_1: this.order.data.itemsParams ? this.order.data.itemsParams.items_price_currency_1 : '',
      //
      //   client_comment_2: this.order.data.itemsParams ? this.order.data.itemsParams.client_comment_2 : '',
      //   items_type_2: this.order.data.itemsParams ? this.order.data.itemsParams.items_type_2 : '',
      //   delivery_type_2: this.order.data.itemsParams ? this.order.data.itemsParams.delivery_type_2 : '',
      //   items_count_2: this.order.data.itemsParams ? this.order.data.itemsParams.items_count_2 : '',
      //   items_weight_2: this.order.data.itemsParams ? this.order.data.itemsParams.items_weight_2 : '',
      //   items_length_2: this.order.data.itemsParams ? this.order.data.itemsParams.items_length_2 : '',
      //   items_width_2: this.order.data.itemsParams ? this.order.data.itemsParams.items_width_2 : '',
      //   items_height_2: this.order.data.itemsParams ? this.order.data.itemsParams.items_height_2 : '',
      //   items_price_2: this.order.data.itemsParams ? this.order.data.itemsParams.items_price_2 : '',
      //   items_price_currency_2: this.order.data.itemsParams ? this.order.data.itemsParams.items_price_currency_2 : '',
      //
      //   client_comment_3: this.order.data.itemsParams ? this.order.data.itemsParams.client_comment_3 : '',
      //   items_type_3: this.order.data.itemsParams ? this.order.data.itemsParams.items_type_3 : '',
      //   delivery_type_3: this.order.data.itemsParams ? this.order.data.itemsParams.delivery_type_3 : '',
      //   items_count_3: this.order.data.itemsParams ? this.order.data.itemsParams.items_count_3 : '',
      //   items_weight_3: this.order.data.itemsParams ? this.order.data.itemsParams.items_weight_3 : '',
      //   items_length_3: this.order.data.itemsParams ? this.order.data.itemsParams.items_length_3 : '',
      //   items_width_3: this.order.data.itemsParams ? this.order.data.itemsParams.items_width_3 : '',
      //   items_height_3: this.order.data.itemsParams ? this.order.data.itemsParams.items_height_3 : '',
      //   items_price_3: this.order.data.itemsParams ? this.order.data.itemsParams.items_price_3 : '',
      //   items_price_currency_3: this.order.data.itemsParams ? this.order.data.itemsParams.items_price_currency_3 : '',
      //
      //   client_comment_4: this.order.data.itemsParams ? this.order.data.itemsParams.client_comment_4 : '',
      //   items_type_4: this.order.data.itemsParams ? this.order.data.itemsParams.items_type_4 : '',
      //   delivery_type_4: this.order.data.itemsParams ? this.order.data.itemsParams.delivery_type_4 : '',
      //   items_count_4: this.order.data.itemsParams ? this.order.data.itemsParams.items_count_4 : '',
      //   items_weight_4: this.order.data.itemsParams ? this.order.data.itemsParams.items_weight_4 : '',
      //   items_length_4: this.order.data.itemsParams ? this.order.data.itemsParams.items_length_4 : '',
      //   items_width_4: this.order.data.itemsParams ? this.order.data.itemsParams.items_width_4 : '',
      //   items_height_4: this.order.data.itemsParams ? this.order.data.itemsParams.items_height_4 : '',
      //   items_price_4: this.order.data.itemsParams ? this.order.data.itemsParams.items_price_4 : '',
      //   items_price_currency_4: this.order.data.itemsParams ? this.order.data.itemsParams.items_price_currency_4 : '',
      // }),
    });
  }

  private  checkWhoPaythis () {
    const val = this.form.get('stock_pay').value;
    this.isWhoPay = this.helpersService.whoPay(val);
  }

  setValueAutocomplete () {
   for (let item of Object.keys(this.fromNames)) {
      this.fromNames[item].value = this.order.data[this.fromNames[item].name];
   }
  }



}
