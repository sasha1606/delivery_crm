import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';

import { ApiSrc } from '@api/api.src';

import { ApiService } from '@api/api.service';

import { TariffsCustoms } from './tariffs-customs';

@Injectable()

export class TariffsCustomsService {

  constructor (
    private apiService: ApiService,
  ) {}

  public getTariffsCustoms (): Observable<TariffsCustoms> {
    return this.apiService.get(ApiSrc.tariffsCustoms.tariffs);
  }

  public sortTariffsCustoms (sort: string = ''): Observable<TariffsCustoms> {
    return this.apiService.get(`${ApiSrc.tariffsCustoms.tariffs}?sort=${sort}`);
  }

}
