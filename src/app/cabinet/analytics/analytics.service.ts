import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiService } from '@api/api.service';

import { ApiSrc } from '@api/api.src';

import { AnalyticsList } from './analytics';

@Injectable()

export class AnalyticsService {

  constructor (
    private apiService: ApiService
  ) {}

  public getAnaliticsList (): Observable<AnalyticsList> {
    return this.apiService.get(ApiSrc.analitics);
  }

  public search ({sort = '', key = '', page = 1, date = ''}): Observable<AnalyticsList> {
    // for IE
    const  keyword = encodeURIComponent(key);
    return this.apiService.get(`${ApiSrc.analitics}?sort=${sort}&keyword=${keyword}&page=${page}&date=${date}`);
  }
}
