import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TariffsDklRoutingModule } from './tariffs-dkl-routing.module';

import { TariffsDklResolve } from './tariffs-dkl.resolve';
import { TariffsDklService } from './tariffs-dkl.service';

import { TariffsDklComponent } from './tariffs-dkl.component';

import { TariffsZonesModule } from '../shared-tariffs/tariffs-zones/tariffs-zones.module';
import { TariffsTransportersModule } from '../shared-tariffs/tariffs-transporters/tariffs-transporters.module';

@NgModule ({
    imports: [
        CommonModule,
        TariffsDklRoutingModule,
        TariffsZonesModule,
        TariffsTransportersModule,
    ],
    declarations: [
        TariffsDklComponent,
    ],
    providers: [
        TariffsDklResolve,
        TariffsDklService,
    ]
})

export class TariffsDklModule {}
