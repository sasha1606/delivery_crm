import {Component, OnInit, OnDestroy, ViewContainerRef} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { QueriesPrice, QueriesPriceGet } from '../queries-price';
import { QueriesPriceService } from '../queries-price.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { typePage } from '@shared/const/crud';
import {Subject} from 'rxjs/Subject';
import {ToastsManager} from 'ng2-toastr';

@Component ({
    selector: 'app-query-price-show',
    templateUrl: './query-price-show.component.html',
    styleUrls: ['./query-price-show.component.css'],
})

export class QueryPriceShowComponent implements OnInit, OnDestroy {

    private destroy$: Subject<boolean> = new Subject<boolean>();

    title = 'Ценновой запрос';
    form: FormGroup;
    queryId: number;

    typePage;

    queriesPrice: QueriesPriceGet;

    constructor (
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private fb: FormBuilder,
        private modalService: NgbModal,
        private queriesPriceService: QueriesPriceService,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef,
    ) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit () {
        this.queriesPrice = this.activatedRoute.snapshot.data['queriesPrice'];
        this.buildForm();
        this.form.disable();

        this.activatedRoute.params.subscribe( params => {
            this.queryId = +params['id'];
        });

        this.typePage = typePage.show;
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

    public buildForm () {
        this.form = this.fb.group({
                transporter_type: this.queriesPrice.data.transporter_type,
                transporter_id: this.queriesPrice.data.transporter_id,
                transporter_direction_id: this.queriesPrice.data.transporter_direction_id,
                transporter_direction_name: this.queriesPrice.data.transporter_direction_name,
                transporter_direction_email: this.queriesPrice.data.transporter_direction_email,
                transporter_direction_resperson: this.queriesPrice.data.transporter_direction_resperson,
                transporter_direction_phone: this.queriesPrice.data.transporter_direction_phone,
            city_from: this.queriesPrice.data.city_from,
            country_from: this.queriesPrice.data.city_from,
            region_from: this.queriesPrice.data.region_from,
            zip_code_from: this.queriesPrice.data.zip_code_from,
            type: this.queriesPrice.data.type,
            // insurance: this.queriesPrice.data.insurance,
            // insurance_currency: this.queriesPrice.data.insurance_currency,
            // insurance_price: this.queriesPrice.data.insurance_price,
            items_price: this.queriesPrice.data.items_price,
            city_to: this.queriesPrice.data.city_to,
            country_to: this.queriesPrice.data.country_to,
            region_to: this.queriesPrice.data.region_to,
            zip_code_to: this.queriesPrice.data.zip_code_to,
            items_count: this.queriesPrice.data.items_count,
            items_totalweight: this.queriesPrice.data.items_totalweight,
            // itemsParams: this.fb.group({
            //   client_comment_1: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.client_comment_1 : '',
            //   items_type_1: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_type_1 : '',
            //   delivery_type_1: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.delivery_type_1 : '',
            //   items_count_1: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_count_1 : '',
            //   items_weight_1: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_weight_1 : '',
            //   items_length_1: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_length_1 : '',
            //   items_width_1: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_width_1 : '',
            //   items_height_1: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_height_1 : '',
            //   items_price_1: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_1 : '',
            //   items_price_currency_1: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_currency_1 : '',
            //
            //   client_comment_2: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.client_comment_2 : '',
            //   items_type_2: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_type_2 : '',
            //   delivery_type_2: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.delivery_type_2 : '',
            //   items_count_2: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_count_2 : '',
            //   items_weight_2: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_weight_2 : '',
            //   items_length_2: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_length_2 : '',
            //   items_width_2: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_width_2 : '',
            //   items_height_2: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_height_2 : '',
            //   items_price_2: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_2 : '',
            //   items_price_currency_2: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_currency_2 : '',
            //
            //   client_comment_3: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.client_comment_3 : '',
            //   items_type_3: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_type_3 : '',
            //   delivery_type_3: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.delivery_type_3 : '',
            //   items_count_3: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_count_3 : '',
            //   items_weight_3: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_weight_3 : '',
            //   items_length_3: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_length_3 : '',
            //   items_width_3: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_width_3 : '',
            //   items_height_3: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_height_3 : '',
            //   items_price_3: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_3 : '',
            //   items_price_currency_3: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_currency_3 : '',
            //
            //   client_comment_4: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.client_comment_4 : '',
            //   items_type_4: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_type_4 : '',
            //   delivery_type_4: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.delivery_type_4 : '',
            //   items_count_4: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_count_4 : '',
            //   items_weight_4: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_weight_4 : '',
            //   items_length_4: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_length_4 : '',
            //   items_width_4: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_width_4 : '',
            //   items_height_4: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_height_4 : '',
            //   items_price_4: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_4 : '',
            //   items_price_currency_4: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_currency_4 : '',
            // }),
            items_type: this.queriesPrice.data.items_type,
            delivery_price: this.queriesPrice.data.delivery_price,
            client_comment: this.queriesPrice.data.client_comment,
            filesList: '',
        });
    }

    public sendAgainRequest () {
        this.queriesPriceService.sendAgainRequest(this.queryId).takeUntil(this.destroy$).subscribe((response) => {
            if (response.error ) {
                this.toastr.error('Error');
            } else {
                this.toastr.success('Has been send successfully');
            }
        });
    }
}
