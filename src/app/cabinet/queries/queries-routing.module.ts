import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QueriesComponent } from './queries.component';

import { QueryCreateComponent } from './query-create/query-create.component';
import { QueryShowComponent } from './query-show/query-show.component';
import { QueryEditComponent } from './query-edit/query-edit.component';

import { QueryShowResolve } from './query-show/query-show.resolve';
import { QueryCreateResolve } from './query-create/query-create.resolve';
import { QueriesResolve } from './queries.resolve';

const ROUTERS: Routes = [
    {
        path: '',
        component: QueriesComponent,
        resolve: {
            queryList: QueriesResolve,
        }
    },
    {
        path: 'create',
        component: QueryCreateComponent,
        resolve: {
            query: QueryCreateResolve,
        }
    },
    {
        path: 'show/:id',
        component: QueryShowComponent,
        resolve: {
          query: QueryShowResolve,
        }
    },
    {
        path: 'edit/:id',
        component: QueryEditComponent,
        resolve: {
          query: QueryShowResolve,
        }
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class QueriesRoutingModule {}
