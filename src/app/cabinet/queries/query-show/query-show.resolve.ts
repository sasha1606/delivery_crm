import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve} from '@angular/router';
import { Router } from '@angular/router';

import { QueriesService } from '../queries.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';

@Injectable()

export class QueryShowResolve implements Resolve<any> {

    constructor (
        private queriesService: QueriesService,
        private router: Router,
    ) {}

    resolve (router: ActivatedRouteSnapshot) {
      return this.queriesService.getQueries(Number(router.paramMap.get('id'))).catch(err => {
        this.router.navigateByUrl('/cabinet/queries');
        return Observable.of({error: err});
      });
    }
}
