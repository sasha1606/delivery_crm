import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { QueriesPriceService } from '../queries-price.service';

@Injectable()

export class QueryPriceCreateResolve implements Resolve<any> {

  constructor (
      private queriesPriceService: QueriesPriceService
  ) {}

  resolve () {
    return this.queriesPriceService.dataQueriesPrice();
  }
}
