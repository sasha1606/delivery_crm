import { Component, OnInit, AfterViewInit, ViewContainerRef, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { AgentsService } from '../agents.service';

import { AgentGet } from '../agents';

import { FromValidation } from '@fromValidation';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';
import { QueriesPriceListService } from '@shared/services/queries-price-list.service';
import { ToastsManager } from 'ng2-toastr';
import { Subject } from 'rxjs/Subject';
import { ValidateOnlyNumber } from '@shared/formValidators/validateOnlyNumber';
import { debounceTime, switchMap, tap } from 'rxjs/operators';

@Component ({
    selector: 'app-agent-show',
    templateUrl: './agent-show.component.html',
    styleUrls: ['./agent-show.component.css'],
})

export class AgentShowComponent extends FromValidation implements OnInit, AfterViewInit, OnDestroy {

    private destroy$: Subject<boolean> = new Subject<boolean>();

    form: FormGroup;
    formModal: FormGroup;
    agentId: number;
    agent: AgentGet;
    countTypeService = 0;

    public queriesPrice;
    public modalRef: NgbModalRef;

    public autocompleteCityFrom: AutocompleteDataCity = {
      loading: false,
      local: [],
      autocompleteSubject$: new Subject()
    };

    public autocompleteCityTo: AutocompleteDataCity = {
      loading: false,
      local: [],
      autocompleteSubject$: new Subject()
    };

    // public autocompleteCityFromFormComponent: AutocompleteCity;
    // public autocompleteCityToFormComponent: AutocompleteCity;

    constructor (
      public router: Router,
      public queriesPriceListService: QueriesPriceListService,
      private agentsService: AgentsService,
      private activatedRoute: ActivatedRoute,
      private modalService: NgbModal,
      private fb: FormBuilder,
      private findToAutocompleteService: FindToAutocompleteService,
      private toastr: ToastsManager,
      private vcr: ViewContainerRef,
    ) {
      super(
        router,
        queriesPriceListService,
      );
      this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit () {
        this.agent = this.activatedRoute.snapshot.data['agent'];

        this.activatedRoute.params.subscribe( params => {
            this.agentId = params['id'];
        });

        if (this.agent.data.typeParams) {
          this.countTypeService = this.agent.data.typeParams.length;
        }

        this.buildForm();
        this.form.disable();

        this.autocompleteSubscribeCity(this.autocompleteCityFrom);
        this.autocompleteSubscribeCity(this.autocompleteCityTo);

        // this.buildAutocompleteCityToFormComponent();
        // this.buildAutocompleteCityFromFormComponent();
    }

    ngOnDestroy () {
       this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }
    ngAfterViewInit () {
      // this.form.disable();
    }

    public openModal (content, data) {
      this.queriesPriceListService.dataQueriesPrice().subscribe((response) => {
        this.queriesPrice = response;
        this.modalRef = this.modalService.open(content, {size: 'lg'});
        this.buildModalForm();
        // this.setValueAutocomplete();
        // if ( data ) {
        // this.formModal.get('client_comment').setValue(data.comment);
        // this.formModal.get('type').setValue(data.typeParams);


        this.formModal.get('transporter_type').setValue('agents');
        this.formModal.get('transporter_id').setValue(this.agent.data.id);
        this.formModal.get('transporter_direction_id').setValue(data.id);
        this.formModal.get('transporter_direction_name').setValue(data.name);
        this.formModal.get('transporter_direction_email').setValue(data.email);
        this.formModal.get('transporter_direction_phone').setValue(data.phone);
        this.formModal.get('transporter_direction_resperson').setValue(data.responsible_person);
        // } else {
        //
        //   this.formModal.get('items_count').setValue(this.transportationsService.getLengthObj(this.transportations.data.directionsParams));
        //   this.formModal.get('type').setValue(this.transportations.data.typeParams);
        //   this.formModal.get('transporter_type').setValue('carriers');
        //   this.formModal.get('transporter_type').setValue('carriers');
        //   this.formModal.get('transporter_type').setValue('carriers');
        // }
        // }
      });
    }

    public onSubmitQueriesPrice () {
      if (this.formModal.valid) {
        this.queriesPriceListService.createQueriesPrice(this.formModal.value).takeUntil(this.destroy$).subscribe( res => {
          if (res.success === 'ok') {
            this.toastr.success('Has been created successfully');
            this.modalRef.close();
            this.autocompleteCityFrom.local = [];
            this.autocompleteCityTo.local = [];
          }  else {
            this.toastr.error('Error');
            this.validationFormBackEnd(res.validation, this.formModal);
          }
        });

      } else {
        this.toastr.error('Error');
        this.validationFormsSubmit(this.formModal);
      }
    }

    /*========= LOCATION =========*/
    public onGetLocation(event, dataAutocomplete): void {
      dataAutocomplete.local = [];
      dataAutocomplete.autocompleteSubject$.next(event.target.value);
    }

    onSelectLocationFrom(event) {
      if (!event) {
        return false;
      }
      this.formModal.get('country_from').setValue(event.countryName);
      this.formModal.get('zip_code_from').setValue(event.postal_code);
      this.formModal.get('region_from').setValue(event.region);
    }

    onSelectLocationTo(event) {
      if (!event) {
        return false;
      }
      this.formModal.get('country_to').setValue(event.countryName);
      this.formModal.get('zip_code_to').setValue(event.postal_code);
      this.formModal.get('region_to').setValue(event.region);
    }

    /*========= / LOCATION =========*/

    private buildForm () {
      this.form = this.fb.group({
        typeService: this.agent.data.typesFormated,
        responsible_person: this.agent.data.responsible_person,
        email: this.agent.data.email,
        phone: this.agent.data.phone,
        countDirections: this.agentsService.getLengthObj(this.agent.data.directionsParams),
      });
    }

    private buildModalForm () {
      this.formModal = this.fb.group({
        city_from: '',
        country_from: '',
        region_from: '',
        zip_code_from: '',
        type: '',
        // insurance: '',
        items_price: '',
        city_to: '',
        country_to: '',
        region_to: '',
        zip_code_to: '',
        items_count: '',
        items_totalweight: '',
        // insurance_price: this.queriesPrice.data.insurance_price,
        // insurance_currency: this.queriesPrice.data.insurance_currency,
        transporter_type: '',
        transporter_id: '',
        transporter_direction_id: '',
        transporter_direction_name: '',
        transporter_direction_email: '',
        transporter_direction_phone: '',
        transporter_direction_resperson: '',
        // itemsParams: this.fb.group({
        //   client_comment_1: '',
        //   items_type_1: '',
        //   delivery_type_1: '',
        //   items_count_1: ['', ValidateOnlyNumber],
        //   items_weight_1: ['', ValidateOnlyNumber],
        //   items_length_1: ['', ValidateOnlyNumber],
        //   items_width_1: ['', ValidateOnlyNumber],
        //   items_height_1: ['', ValidateOnlyNumber],
        //   items_price_1: ['', ValidateOnlyNumber],
        //   items_price_currency_1: '',
        //
        //   client_comment_2: '',
        //   items_type_2: '',
        //   delivery_type_2: '',
        //   items_count_2: ['', ValidateOnlyNumber],
        //   items_weight_2: ['', ValidateOnlyNumber],
        //   items_length_2: ['', ValidateOnlyNumber],
        //   items_width_2: ['', ValidateOnlyNumber],
        //   items_height_2: ['', ValidateOnlyNumber],
        //   items_price_2: ['', ValidateOnlyNumber],
        //   items_price_currency_2: '',
        //
        //   client_comment_3: '',
        //   items_type_3: '',
        //   delivery_type_3: '',
        //   items_count_3: ['', ValidateOnlyNumber],
        //   items_weight_3: ['', ValidateOnlyNumber],
        //   items_length_3: ['', ValidateOnlyNumber],
        //   items_width_3: ['', ValidateOnlyNumber],
        //   items_height_3: ['', ValidateOnlyNumber],
        //   items_price_3: ['', ValidateOnlyNumber],
        //   items_price_currency_3: '',
        //
        //   client_comment_4: '',
        //   items_type_4: '',
        //   delivery_type_4: '',
        //   items_count_4: ['', ValidateOnlyNumber],
        //   items_weight_4: ['', ValidateOnlyNumber],
        //   items_length_4: ['', ValidateOnlyNumber],
        //   items_width_4: ['', ValidateOnlyNumber],
        //   items_height_4: ['', ValidateOnlyNumber],
        //   items_price_4: ['', ValidateOnlyNumber],
        //   items_price_currency_4: '',
        // }),
        items_type: this.queriesPrice.data.phone,
        delivery_price: this.queriesPrice.data.phone,
        client_comment: this.queriesPrice.data.phone,
        filesUpload: '',
        deleteFiles: '',
      });
    }

  // private buildAutocompleteCityToFormComponent () {
  //   this.autocompleteCityToFormComponent = {
  //     searchSelectAutocomplete: {
  //       formControlName: 'city_to',
  //       label: 'Город получателя',
  //       value: '',
  //     },
  //     searchInfoAutocomplete: {
  //       country: {
  //         formControlName: 'country_to',
  //         label: 'Страна получателя',
  //         value: '',
  //         searchEvent: 'countryName',
  //       },
  //       region: {
  //         formControlName: 'region_to',
  //         label: 'Район/Штат получателя',
  //         value: '',
  //         searchEvent: 'region',
  //       },
  //       zip_code: {
  //         formControlName: 'zip_code_to',
  //         label: 'Индекс получателя',
  //         value: '',
  //         searchEvent: 'postal_code',
  //       }
  //     }
  //   }
  // }

  // private buildAutocompleteCityFromFormComponent () {
  //   this.autocompleteCityFromFormComponent = {
  //     searchSelectAutocomplete: {
  //       formControlName: 'city_from',
  //       label: 'Город отправителя',
  //       value: '',
  //     },
  //     searchInfoAutocomplete: {
  //       country: {
  //         formControlName: 'country_from',
  //         label: 'Страна отправителя',
  //         value: '',
  //         searchEvent: 'countryName',
  //       },
  //       region: {
  //         formControlName: 'region_from',
  //         label: 'Район/Штат отправителя',
  //         value: '',
  //         searchEvent: 'region',
  //       },
  //       zip_code: {
  //         formControlName: 'zip_code_from',
  //         label: 'Индекс отправителя',
  //         value: '',
  //         searchEvent: 'postal_code',
  //       }
  //     }
  //   }
  // }

  private autocompleteSubscribeCity (dataAutocomplete): void {
    dataAutocomplete.autocompleteSubject$.pipe(
      tap(() => dataAutocomplete.loading = true),
      debounceTime(400),
      switchMap( (city) => {
        return this.findToAutocompleteService.findLocation(city);
      })
    ).subscribe(res => {
      dataAutocomplete.local = res.data;
      dataAutocomplete.loading = false;
    });
  }
}

interface AutocompleteDataCity {
  loading: boolean;
  local: object[];
  autocompleteSubject$: object;
}
