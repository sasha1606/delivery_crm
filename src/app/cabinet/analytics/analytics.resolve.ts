import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { AnalyticsService } from './analytics.service';

@Injectable()

export class AnalyticsResolve implements Resolve<any> {

  constructor (
    private analyticsService: AnalyticsService,
  ) {}

  resolve () {
    return this.analyticsService.getAnaliticsList();
  }

}
