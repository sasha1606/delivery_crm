import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CourierComponent } from './couriers.component';
import { CourierCrudComponent } from './courier-crud/courier-crud.component';
// import { CourierShowComponent } from './courier-show_delete/courier-show.component';
// import { CourierEditComponent } from './courier-edit_delete/courier-edit.component';

import { CourierCreateResolve } from './courier-crud/courier-create.resolve';
import { CourierShowResolve } from './courier-crud/courier-show.resolve';
import { CourierEditResolve } from './courier-crud/courier-edit.resolve';
import { CouriersResolve } from './couriers.resolve';

import { crud } from './couriers';


const ROUTERS: Routes = [
    {
        path: '',
        component: CourierComponent,
        resolve: {
          couriers: CouriersResolve
        }
    },
    {
        path: 'create',
        component: CourierCrudComponent,
        resolve: {
            courier: CourierCreateResolve
        },
        data: {
          crud: crud.create
        }
    },
    {
        path: 'show/:id',
        component: CourierCrudComponent,
        resolve: {
          courier: CourierShowResolve
        },
        data: {
          crud: crud.show
        }
    },
    {
        path: 'edit/:id',
        component: CourierCrudComponent,
        resolve: {
          courier: CourierEditResolve
        },
        data: {
          crud: crud.edit
        }
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class CouriersRoutingModule {}
