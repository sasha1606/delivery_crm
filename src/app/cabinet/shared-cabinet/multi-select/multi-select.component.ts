import { Component, OnInit, Input } from '@angular/core';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { FormControl, FormBuilder } from '@angular/forms';

@Component ({
  selector: 'app-multi-select',
  templateUrl: './multi-select.component.html',
  styleUrls: ['./multi-select.component.css'],
})

export class MultiSelectComponent implements OnInit {

  @Input() data;
  @Input() form;
  @Input() dataValue;
  @Input() dataName;
  @Input() theme = '';

  myOptions: IMultiSelectOption[] = [];
  mySettings: IMultiSelectSettings;
  myTexts: IMultiSelectTexts;

  constructor () {}

  ngOnInit () {
    this.setItemToSelect();

    // Settings configuration
    this.mySettings = {
      dynamicTitleMaxItems: 2,
    };

    // Text configuration
    this.myTexts = {
      checked: 'Выбрано',
      checkedPlural: 'Выбрано',
      defaultTitle: 'Выберете',
    };

    this.form.addControl(this.dataName, new FormControl(this.dataValue));
  }


  private setItemToSelect () {
    for (const key of Object.keys(this.data)) {
    // for (const key in this.data) {
      this.myOptions.push({
        id: key,
        name: this.data[key],
      });
    }
  }

}
