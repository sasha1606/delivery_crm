import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { CouriersService } from './couriers.service';

@Injectable()

export class CouriersResolve implements Resolve<any> {

  constructor (
      private couriersService: CouriersService,
  ) {}

  resolve () {
    return this.couriersService.getCouriersList();
  }

}
