import { Pagination } from '@shared/interface/pagination';

export interface AccountingList {
  listdata: ShortAccounting[];
  pagination: Pagination;
  success: string;
}

interface ShortAccounting {
  code_dkl: number;
  created_at: number;
  delivery_price: number;
  fuel_comission: number;
  id: number;
  invoice_account: string;
  invoice_nds: string;
  items_count: number;
  items_price: number;
  nds: number;
  nds_percent: number;
  tarif: number;
  typeName: string;
}
