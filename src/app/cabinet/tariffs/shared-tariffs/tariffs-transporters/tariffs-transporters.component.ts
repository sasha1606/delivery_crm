import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tariffs-transporters',
  templateUrl: './tariffs-transporters.component.html',
  styleUrls: ['./tariffs-transporters.component.css'],
})

export class TariffsTransportersComponent implements OnInit {

  @Input() transporters = '';
  @Input() title = '';

  ngOnInit () {
  }

}
