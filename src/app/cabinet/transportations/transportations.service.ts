import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiSrc } from '@api/api.src';

import { ApiService } from '@api/api.service';

import {
  TransportationsGet,
  Transportations,
  TransportationsModalGet,
  TransportationsModal,
  TransportationsListGet
} from './transportations';

import { Response } from '@shared/interface/response';
import { AdminList } from '../admin-users/admin-user';

@Injectable()

export class TransportationsService {

  constructor (
      private apiService: ApiService
  ) {}

  public getTransportationsList (): Observable<TransportationsListGet> {
    return this.apiService.post(ApiSrc.transportations.list, {});
  }

  public search ({page: page = 1, keyword: keyword = '', sort: sort = ''}) {
    // for IE
    const  key = encodeURIComponent(keyword);
    return this.apiService.get(`${ApiSrc.transportations.list}?keyword=${key}&page=${page}&sort=${sort}`);
  }


  public getTransportation (id: number): Observable<TransportationsGet> {
      return this.apiService.get(`${ApiSrc.transportations.show}/${id}`);
  }

  public dataCreateTransportation (): Observable<TransportationsGet> {
    return this.apiService.get(ApiSrc.transportations.createTransportations);
  }

  public createTransportation (transportation: Transportations | object): Observable<TransportationsGet> {
    return this.apiService.post(`${ApiSrc.transportations.createTransportations}`, transportation);
  }

  public editTransportation ({transportation, id}): Observable<TransportationsGet> {
    return this.apiService.post(`${ApiSrc.transportations.edit}/${id}`, transportation);
  }

  public dataTransportationModal (): Observable<TransportationsModalGet> {
    return this.apiService.get(ApiSrc.transportations.createTransportationsModal);
  }

  public createTransportationModal (transportation: TransportationsModal): Observable<TransportationsModalGet> {
    return this.apiService.post(ApiSrc.transportations.createTransportationsModal, transportation);
  }


  public getLengthObj (params) {
    if (params) {
      return Object.keys(params).length;
    }
    return 0;
  }

}
