import { Component, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component ({
  selector: 'app-agent-form-element-show-component',
  templateUrl: './agent-form-element-show.component.html',
  styleUrls: ['./agent-form-element-show.component.css']
})

export class AgentFormElementShowComponent implements OnInit {

  @Input() dataForm;
  @Input() form;

  countTypeService: 1;

  constructor (
    private fb: FormBuilder,
  ) {}

  ngOnInit () {
    if (this.dataForm.value.typeParams) {
      this.countTypeService = this.dataForm.value.typeParams.length;
    }
    this.fromBuilder();
    this.form.disable();
  }

  private fromBuilder () {
    this.form.addControl(this.dataForm.key, this.fb.group({
      'name': this.dataForm.value.name,
      'email': [this.dataForm.value.email, Validators.required],
      'comment': this.dataForm.value.comment,
      'phone': [this.dataForm.value.phone],
      'responsible_person': this.dataForm.value.responsible_person,
      'typeParams': this.dataForm.value.typesFormated,
    }));
  }
}
