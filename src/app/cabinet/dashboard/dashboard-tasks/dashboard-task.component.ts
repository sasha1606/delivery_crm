import { Component, Input, OnInit } from '@angular/core';

@Component ({
    selector: 'app-dashboard-task',
    templateUrl: './dashboard-task.component.html',
    styleUrls: ['./dashboard-task.component.css'],
})

export class DashboardTaskComponent implements OnInit {

    @Input() orders;

    // public polarAreaChartData: number[] = [100, 40, 120];
    public polarAreaChartLabels: string[] = ['Выехал курьер', 'Получен', 'На складе'];
    public polarAreaChartData: string[];
    public polarAreaLegend = false;

    public polarAreaChartType = 'polarArea';

    ngOnInit () {
      this.polarAreaChartData = this.orders;
    }
}
