import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiSrc } from '@api/api.src';
import { ApiService } from '@api/api.service';
import {QueriesPriceGet} from '../../queries-price/queries-price';

@Injectable()

export class QueriesPriceListService {

  constructor (
    private apiService: ApiService
  ) {}

  public dataQueriesPrice (): Observable<any> {
    return this.apiService.get(ApiSrc.queriesPrice.create);
  }
  public createQueriesPrice (data): Observable<any> {
    return this.apiService.post(ApiSrc.queriesPrice.create, data);
  }
}
