import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormFileUploadComponent } from './form-file-upload.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    FormFileUploadComponent
  ],
  declarations: [
    FormFileUploadComponent
  ]
})

export class FormFileUploadModule {

}
