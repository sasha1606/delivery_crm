import { Pagination } from '@shared/interface/pagination';

export interface AnalyticsList {
  listdata: ShortAnalytics[];
  pagination: Pagination;
  success: string;
  sum: SumList;
}

interface ShortAnalytics {
  code_dkl: number;
  id: number;
  profit: number;
  pay_to_agents: number;
  stock_price: number;
  delivery_price: string;
}
interface SumList {
  delivery_price: number;
  pay_to_agents: number;
  profit: number;
  stock_price: number;
}
