import { Component, OnInit, OnDestroy, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormControl, FormBuilder, FormGroup} from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';

import { ToastsManager } from 'ng2-toastr';

import { maskPhone } from '@shared/const/masks';

import { QueriesService } from '../queries.service';

import { QueriesGet } from '../queries';
import {AutocompleteCity} from '@shared/interface/autocomplete-city';

// TODO add modal-autocomplete shared!!

@Component ({
    selector: 'app-query-show',
    templateUrl: '../query-crud.component.html',
    styleUrls: ['./query-show.component.css']
})

export class QueryShowComponent implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    title = 'Просмотр запроса';
    form: FormGroup;

    public showFrom: boolean;
    public ordersinfoTable;
    public maskPhone =  maskPhone;
    private modalRef: NgbModalRef;
    public formModal: FormGroup;
    public transporters: any;
    public directions: any;
    // public titleCity_from = '';
    // public titleCountry_from = '';
    // public postalCode_from;
    // public titleRegion_from;
    public local_from = [];
    // public localMain_from = [];

    // public titleCity_to = '';
    // public titleCountry_to = '';
    // public postalCode_to;
    // public titleRegion_to;
    public local_to = [];
    // public localMain_to = [];
    public test1;
    public userInfo;

    query: QueriesGet;
    idUser: number;

    editFrom: boolean;

    public autocompleteCityFromFormComponent: AutocompleteCity;
    public autocompleteCityToFormComponent: AutocompleteCity;

    constructor (
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private modalService: NgbModal,
        private vcr: ViewContainerRef,
        private fb: FormBuilder,
        private queriesService: QueriesService,
        private findToAutocompleteService: FindToAutocompleteService,
        private toastr: ToastsManager,
    ) {
      this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit () {
        this.editFrom = false;
        this.showFrom = true;
        this.query = this.activatedRoute.snapshot.data['query'];
        this.ordersinfoTable = this.query.data.ordersinfoTable;
        this.buildFrom();
        this.form.disable();

        this.activatedRoute.params.subscribe(param => {
            this.idUser = +param['id'];
        });
        this.buildAutocompleteCityFromFormComponent();
        this.buildAutocompleteCityToFormComponent();
    }

    ngOnDestroy () {
      this.destroy$.next(true);
      this.destroy$.unsubscribe();
    }

    public sentPriceLetter () {
      this.queriesService.sentLetter(this.idUser).takeUntil(this.destroy$).subscribe(res => {
        if (res.success === 'ok') {
          this.toastr.success('Successfully');
        } else {
          this.toastr.error('Error');
        }
      });
    }

    public open (content): void {
      this.modalRef = this.modalService.open(content, {size: 'lg'});
      this.buildModalForm();
      this.changeModalSelect();
    }


  /*========= LOCATION =========*/
  //
  //
  //
  // public isLiction;
  // onGetLocation(city) {
  //   this.isLiction = this.findToAutocompleteService.findLocation(city)
  //     .subscribe(res => {
  //       this.local_from = res.data;
  //       this.local_to = res.data;
  //       if(res !== 0 || res !== '' || res !== 'null') {
  //
  //       }
  //     });
  //
  // }
  //
  // onGetLocationFrom(event) {
  //   this.local_from = [];
  //   const test2 = event.target.value;
  //   this.onGetLocation(test2);
  // }
  //
  // onSelectLocationFrom(event) {
  //   // this.titleCountry_from = event.countryName;
  //   // this.postalCode_from = event.postal_code;
  //   // this.titleRegion_from = event.region;
  //   if (!event) {
  //     return false;
  //   }
  //   this.form.get('country_from').setValue(event.countryName);
  //   this.form.get('zip_code_from').setValue(event.postal_code);
  //   this.form.get('region_from').setValue(event.region);
  //
  // }
  //
  // onGetLocationTo(event) {
  //   this.local_to = [];
  //   const test2 = event.target.value || this.test1;
  //   this.onGetLocation(test2);
  // }
  //
  // onSelectLocationTo(event) {
  //   // this.titleCountry_to = event.countryName;
  //   // this.postalCode_to = event.postal_code;
  //   // this.titleRegion_to = event.region;
  //   if (!event) {
  //     return false;
  //   }
  //   this.form.get('country_to').setValue(event.countryName);
  //   this.form.get('zip_code_to').setValue(event.postal_code);
  //   this.form.get('region_to').setValue(event.region);
  // }

  /*========= / LOCATION =========*/

  /*------FindUser----------*/

  onGetUserInfo(event) {
    this.findToAutocompleteService.findUser(event.target.value)
      .subscribe(res => {
        this.userInfo = res['data'];
      });
  }

  onSelectUserInfo(event) {
    // this.userCompany = event.company;
    // this.userEmail = event.email;
    // this.userPhone = event.phone;
    if (!event) {
      return false;
    }
    this.form.get('company').setValue(event.company);
    this.form.get('email').setValue(event.email);
    this.form.get('phone').setValue(event.phone);
  }

  /*------ / FindUser----------*/



    // for build --prod
    public onSubmit (from) {

    }

    private buildFrom (): void {
      this.form = this.fb.group({
        status: [this.query.data.status],
        name: [this.query.data.name],
        company: [this.query.data.company],
        email: this.query.data.email,
        phone: this.query.data.phone,
        // country_from: this.query.data.country_from,
        // city_from: this.query.data.city_from,
        address_from: this.query.data.address_from,
        // region_from: this.query.data.region_from,
        // zip_code_from: this.query.data.zip_code_from,
        type: this.query.data.type,
        // insurance: this.query.data.insurance,
        // insurance_price: this.query.data.insurance_price,
        items_price: this.query.data.items_price,
        // country_to: this.query.data.country_to,
        // city_to: this.query.data.city_to,
        address_to: this.query.data.address_to,
        // region_to: this.query.data.region_to,
        // zip_code_to: this.query.data.zip_code_to,
        items_count: this.query.data.items_count,
        items_totalweight: this.query.data.items_totalweight,
        // insurance_currency: this.query.data.insurance_currency,
        // itemsParams: this.fb.group({
        //   client_comment_1: this.query.data.itemsParams ? this.query.data.itemsParams.client_comment_1 : '',
        //   items_type_1: this.query.data.itemsParams ? this.query.data.itemsParams.items_type_1 : '',
        //   delivery_type_1: this.query.data.itemsParams ? this.query.data.itemsParams.delivery_type_1 : '',
        //   items_count_1: this.query.data.itemsParams ? this.query.data.itemsParams.items_count_1 : '',
        //   items_weight_1: this.query.data.itemsParams ? this.query.data.itemsParams.items_weight_1 : '',
        //   items_length_1: this.query.data.itemsParams ? this.query.data.itemsParams.items_length_1 : '',
        //   items_width_1: this.query.data.itemsParams ? this.query.data.itemsParams.items_width_1 : '',
        //   items_height_1: this.query.data.itemsParams ? this.query.data.itemsParams.items_height_1 : '',
        //   items_price_1: this.query.data.itemsParams ? this.query.data.itemsParams.items_price_1 : '',
        //   items_price_currency_1: this.query.data.itemsParams ? this.query.data.itemsParams.items_price_currency_1 : '',
        //
        //   client_comment_2: this.query.data.itemsParams ? this.query.data.itemsParams.client_comment_2 : '',
        //   items_type_2: this.query.data.itemsParams ? this.query.data.itemsParams.items_type_2 : '',
        //   delivery_type_2: this.query.data.itemsParams ? this.query.data.itemsParams.delivery_type_2 : '',
        //   items_count_2: this.query.data.itemsParams ? this.query.data.itemsParams.items_count_2 : '',
        //   items_weight_2: this.query.data.itemsParams ? this.query.data.itemsParams.items_weight_2 : '',
        //   items_length_2: this.query.data.itemsParams ? this.query.data.itemsParams.items_length_2 : '',
        //   items_width_2: this.query.data.itemsParams ? this.query.data.itemsParams.items_width_2 : '',
        //   items_height_2: this.query.data.itemsParams ? this.query.data.itemsParams.items_height_2 : '',
        //   items_price_2: this.query.data.itemsParams ? this.query.data.itemsParams.items_price_2 : '',
        //   items_price_currency_2: this.query.data.itemsParams ? this.query.data.itemsParams.items_price_currency_2 : '',
        //
        //   client_comment_3: this.query.data.itemsParams ? this.query.data.itemsParams.client_comment_3 : '',
        //   items_type_3: this.query.data.itemsParams ? this.query.data.itemsParams.items_type_3 : '',
        //   delivery_type_3: this.query.data.itemsParams ? this.query.data.itemsParams.delivery_type_3 : '',
        //   items_count_3: this.query.data.itemsParams ? this.query.data.itemsParams.items_count_3 : '',
        //   items_weight_3: this.query.data.itemsParams ? this.query.data.itemsParams.items_weight_3 : '',
        //   items_length_3: this.query.data.itemsParams ? this.query.data.itemsParams.items_length_3 : '',
        //   items_width_3: this.query.data.itemsParams ? this.query.data.itemsParams.items_width_3 : '',
        //   items_height_3: this.query.data.itemsParams ? this.query.data.itemsParams.items_height_3 : '',
        //   items_price_3: this.query.data.itemsParams ? this.query.data.itemsParams.items_price_3 : '',
        //   items_price_currency_3: this.query.data.itemsParams ? this.query.data.itemsParams.items_price_currency_3 : '',
        //
        //   client_comment_4: this.query.data.itemsParams ? this.query.data.itemsParams.client_comment_4 : '',
        //   items_type_4: this.query.data.itemsParams ? this.query.data.itemsParams.items_type_4 : '',
        //   delivery_type_4: this.query.data.itemsParams ? this.query.data.itemsParams.delivery_type_4 : '',
        //   items_count_4: this.query.data.itemsParams ? this.query.data.itemsParams.items_count_4 : '',
        //   items_weight_4: this.query.data.itemsParams ? this.query.data.itemsParams.items_weight_4 : '',
        //   items_length_4: this.query.data.itemsParams ? this.query.data.itemsParams.items_length_4 : '',
        //   items_width_4: this.query.data.itemsParams ? this.query.data.itemsParams.items_width_4 : '',
        //   items_height_4: this.query.data.itemsParams ? this.query.data.itemsParams.items_height_4 : '',
        //   items_price_4: this.query.data.itemsParams ? this.query.data.itemsParams.items_price_4 : '',
        //   items_price_currency_4: this.query.data.itemsParams ? this.query.data.itemsParams.items_price_currency_4 : '',
        // }),
        items_type: this.query.data.items_type,
        client_comment: this.query.data.client_comment,
        delivery_price: this.query.data.delivery_price,
        rate: this.query.data.rate,
        user_comment: this.query.data.user_comment,
      });
    }

    private buildModalForm () {
      this.formModal = this.fb.group({
        transportersType: '',
        transporterId: '',
        directionId: '',
        name: '',
        email: '',
        phone: '',
        responsible_person: '',
        orderId: this.idUser,
      });
    }


    changeModalSelect () {

        this.formModal.get('transportersType').valueChanges.subscribe(type => {
            if (!type) {
              return;
            }

            this.formModal.get('transporterId').setValue(null);
            this.formModal.get('name').setValue(null);
            this.formModal.get('phone').setValue(null);
            this.formModal.get('email').setValue(null);
            this.formModal.get('responsible_person').setValue(null);
            this.directions = null;
            this.transporters = null;

            this.queriesService.changetransportersType(type).takeUntil(this.destroy$).subscribe( res => {
              if (res.success === 'ok') {
                this.transporters = res;
              }
            });
        });

        this.formModal.get('transporterId').valueChanges.subscribe(id => {
            if (!id) {
              return;
            }
            this.formModal.get('directionId').setValue(null);
            this.formModal.get('name').setValue(null);
            this.formModal.get('phone').setValue(null);
            this.formModal.get('email').setValue(null);
            this.formModal.get('responsible_person').setValue(null);
            this.directions = null;

            this.queriesService.changeTransporterId({
                transportersType: this.formModal.get('transportersType').value,
                transporterId: id,
               })
              .takeUntil(this.destroy$).subscribe( res => {
              if (res.success === 'ok') {
                this.directions = res;
              }
            });
        });

        this.formModal.get('directionId').valueChanges.subscribe(id => {
            if (!id) {
              return;
            }

            this.queriesService.changeDirections({
              transportersType: this.formModal.get('transportersType').value,
              transporterId: this.formModal.get('transporterId').value,
              directionId: id,
            })
              .takeUntil(this.destroy$).subscribe( res => {
                this.formModal.get('name').setValue(res.directioninfo.name);
                this.formModal.get('phone').setValue(res.directioninfo.phone);
                this.formModal.get('email').setValue(res.directioninfo.email);
                this.formModal.get('responsible_person').setValue(res.directioninfo.responsible_person);
            });
        });
    }

  public onSubmitModal (): void {
    if (this.formModal.valid) {
      this.queriesService.modalSendForm(this.formModal.value).takeUntil(this.destroy$).subscribe( res => {
        if (res.success === 'ok') {
          this.toastr.success('Successfully');
          this.modalRef.close();
          // this.router.navigateByUrl('/cabinet/queries');
        }  else {
          this.toastr.error('Error');
          // this.validationFormBackEnd(res.validation, this.form);
        }
      });
    //
    } else {
      // this.validationFormsSubmit(this.form);
    }
  }

  private buildAutocompleteCityToFormComponent () {
    this.autocompleteCityToFormComponent = {
      searchSelectAutocomplete: {
        formControlName: 'city_to',
        label: 'Город получателя',
        value: this.query.data.city_to,
      },
      searchInfoAutocomplete: {
        country: {
          formControlName: 'country_to',
          label: 'Страна получателя',
          value: this.query.data.country_to,
          searchEvent: 'countryName',
        },
        region: {
          formControlName: 'region_to',
          label: 'Район/Штат получателя',
          value: this.query.data.region_to,
          searchEvent: 'region',
        },
        zip_code: {
          formControlName: 'zip_code_to',
          label: 'Почтовый индекс получателя',
          value: this.query.data.zip_code_to,
          searchEvent: 'postal_code',
        }
      }
    };
  }

  private buildAutocompleteCityFromFormComponent () {
    this.autocompleteCityFromFormComponent = {
      searchSelectAutocomplete: {
        formControlName: 'city_from',
        label: 'Город отправителя',
        value: this.query.data.city_from,
      },
      searchInfoAutocomplete: {
        country: {
          formControlName: 'country_from',
          label: 'Страна отправителя',
          value: this.query.data.country_from,
          searchEvent: 'countryName',
        },
        region: {
          formControlName: 'region_from',
          label: 'Район/Штат отправителя',
          value: this.query.data.region_from,
          searchEvent: 'region',
        },
        zip_code: {
          formControlName: 'zip_code_from',
          label: 'Почтовый индекс получателя',
          value: this.query.data.zip_code_from,
          searchEvent: 'postal_code',
        }
      }
    };
  }
}
