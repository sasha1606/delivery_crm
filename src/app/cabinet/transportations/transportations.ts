import { Pagination } from '@shared/interface/pagination';

export interface Transportations {
  name: string | null;
  email: string | null;
  phone: string | null;
  responsible_person: string | null;
  typeNames: object;
  typeParams?: string[] | null;
  typesFormated: string;
  id: number;
  directionsParams: TransportationsModal[] | null;
}

export interface TransportationsListGet {
  listdata: ShortTransportation[];
  success: string;
  pagination: Pagination;
}

export interface TransportationsGet {
  data: Transportations;
  success?: string;
  error?: string;
  validation?: object;
  id: number;
}

export interface TransportationsModal {
  carrier_id: number | null;
  comment: string | null;
  email: string | null;
  id: number | null;
  name: string | null;
  phone: string | null;
  responsible_person: string | null;
  typeParams: string[];
  typeNames: object | null;
}

export interface TransportationsModalGet {
  data: TransportationsModal;
  success?: string;
  error?: string;
  validation?: object;
}

interface ShortTransportation {
  email: number;
  id: string;
  name: string;
  phone: string;
  responsible_person: string;
  typesFormated: string;
}

export const typePage = {
  edit: 'edit',
  create: 'create'
};


