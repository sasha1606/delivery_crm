import { FormControl } from '@angular/forms';

const rgxNumberAndDot = /^[0-9]*\.?[0-9]*$/;

export function ValidateOnlyNumber(control: FormControl) {
  if (control.value && !rgxNumberAndDot.test(control.value)) {
    return { notOnlyNumber: true };
  }
  return null;
}
