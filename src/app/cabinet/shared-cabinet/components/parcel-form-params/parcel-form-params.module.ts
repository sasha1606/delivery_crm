import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParcelFormParamsComponent } from './parcel-form-params.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PipeModule } from '@shared/pipes/pipe.module';

@NgModule ({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PipeModule,
  ],
  declarations: [
    ParcelFormParamsComponent
  ],
  exports: [
    ParcelFormParamsComponent
  ],
})

export class ParcelFormParamsModule {
}
