import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormControl, FormBuilder, FormGroup} from '@angular/forms';

import { HelpersService } from '@shared/services/helpers.service';

import { StorageGet } from '../storage';

import { typePage } from '@shared/const/crud';

@Component ({
  selector: 'app-storage-show',
  templateUrl: './storage-show.component.html',
  styleUrls: ['./storage-show.component.css']
})

export class StorageShowComponent implements OnInit {

  title = 'Редактирование';
  form: FormGroup;
  public isWhoPay = false;

  public storageHistory = [];

  public typePage;

  storage: StorageGet;
  storageId: number;

  public fromNames = {
    type: {
      name: 'transporter_type',
      value: '',
    },
    id: {
      name: 'transporter_id',
      value: '',
    },
    direction_id: {
      name: 'transporter_direction_id',
      value: '',
    },
    direction_name: {
      name: 'transporter_direction_name',
      value: '',
    },
    direction_phone: {
      name: 'transporter_direction_phone',
      value: '',
    },
    direction_email: {
      name: 'transporter_direction_email',
      value: '',
    },
    direction_resperson: {
      name: 'transporter_direction_resperson',
      value: '',
    },
  };

  constructor (
    public router: Router,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private helpersService: HelpersService,
  ) {
  }

  ngOnInit () {
    this.storage = this.activatedRoute.snapshot.data['storage'];
    this.storageHistory = this.storage.data.ordersinfoTable;
    this.activatedRoute.params.subscribe(param => {
      this.storageId = +param['id'];
    });
    this.buildFrom();
    this.form.disable();
    this.checkWhoPaythis();
    this.setValueAutocomplete();
    this.typePage = typePage.show;
  }

  public sendAgain () {

  }

  private buildFrom (): void {
    this.form = this.fb.group({
      code_dkl: this.storage.data.code_dkl,
      code_transporter: this.storage.data.code_transporter,
      name: this.storage.data.name,
      courier_id: this.storage.data.courier_id,
      company: this.storage.data.company,
      email: this.storage.data.email,
      phone: this.storage.data.phone,
      city_from: this.storage.data.city_from,
      country_from: this.storage.data.country_from,
      region_from: this.storage.data.region_from,
      zip_code_from: this.storage.data.zip_code_from,
      name_receiver: this.storage.data.name_receiver,
      company_receiver: this.storage.data.company_receiver,
      email_receiver: this.storage.data.email_receiver,
      phone_receiver: this.storage.data.phone_receiver,
      city_to: this.storage.data.city_to,
      country_to: this.storage.data.country_to,
      region_to: this.storage.data.region_to,
      zip_code_to: this.storage.data.zip_code_to,
      type: this.storage.data.type,
      // insurance: this.storage.data.insurance,
      // insurance_currency: this.storage.data.insurance_currency,
      // insurance_price: this.storage.data.insurance_price,
      export_cause: this.storage.data.export_cause,
      export_terms: this.storage.data.export_terms,
      stock_pay_type: this.storage.data.stock_pay_type,
      stock_pay: this.storage.data.stock_pay,
      stock_price: this.storage.data.stock_price,
      stock_price_currency: this.storage.data.stock_price_currency,
      stock_comment: this.storage.data.stock_comment,
      stock_delivery_fact_pay: this.storage.data.stock_delivery_fact_pay,
      delivery_price_currency: this.storage.data.delivery_price_currency,
      delivery_price: this.storage.data.delivery_price,
      // pickup_date: this.order.data.pickup_date,
      pickup_timefrom: this.storage.data.pickup_timefrom,
      pickup_timeto: this.storage.data.pickup_timeto,
      client_comment: this.storage.data.client_comment,
      items_count: this.storage.data.items_count,
      items_totalweight: this.storage.data.items_totalweight,
      items_type: this.storage.data.items_type,
      items_price: this.storage.data.items_price,
      items_price_currency: this.storage.data.items_price_currency,
      delivery_type: this.storage.data.delivery_type,
      status: this.storage.data.status,
      thirdside_name: this.storage.data.thirdside_name,
      thirdside_company: this.storage.data.thirdside_company,
      thirdside_email: this.storage.data.thirdside_email,
      thirdside_phone: this.storage.data.thirdside_phone,
      thirdside_city: this.storage.data.thirdside_city,
      thirdside_country: this.storage.data.thirdside_country,
      thirdside_region: this.storage.data.thirdside_region,
      thirdside_zip_code: this.storage.data.thirdside_zip_code,
      invoice_number: this.storage.data.invoice_number,
      invoice_date: this.storage.data.invoice_date,
      invoice_status: this.storage.data.invoice_status,
      stock_date_come: this.storage.data.stock_date_come,
      stock_delivery_type: this.storage.data.stock_delivery_type,
      stock_date_send: this.storage.data.stock_date_send,
      stock_person_comment: this.storage.data.stock_person_comment,
      user_comment: this.storage.data.user_comment,
      stock_date_delivery: this.storage.data.stock_date_delivery,
      stock_delivery_time: this.storage.data.stock_delivery_time,
      stock_person: this.storage.data.stock_person,
      // itemsParams: this.fb.group({
      //   client_comment_1: this.storage.data.itemsParams ? this.storage.data.itemsParams.client_comment_1 : '',
      //   items_type_1: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_type_1 : '',
      //   delivery_type_1: this.storage.data.itemsParams ? this.storage.data.itemsParams.delivery_type_1 : '',
      //   items_count_1: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_count_1 : '',
      //   items_weight_1: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_weight_1 : '',
      //   items_length_1: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_length_1 : '',
      //   items_width_1: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_width_1 : '',
      //   items_height_1: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_height_1 : '',
      //   items_price_1: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_1 : '',
      //   items_price_currency_1: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_currency_1 : '',
      //
      //   client_comment_2: this.storage.data.itemsParams ? this.storage.data.itemsParams.client_comment_2 : '',
      //   items_type_2: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_type_2 : '',
      //   delivery_type_2: this.storage.data.itemsParams ? this.storage.data.itemsParams.delivery_type_2 : '',
      //   items_count_2: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_count_2 : '',
      //   items_weight_2: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_weight_2 : '',
      //   items_length_2: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_length_2 : '',
      //   items_width_2: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_width_2 : '',
      //   items_height_2: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_height_2 : '',
      //   items_price_2: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_2 : '',
      //   items_price_currency_2: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_currency_2 : '',
      //
      //   client_comment_3: this.storage.data.itemsParams ? this.storage.data.itemsParams.client_comment_3 : '',
      //   items_type_3: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_type_3 : '',
      //   delivery_type_3: this.storage.data.itemsParams ? this.storage.data.itemsParams.delivery_type_3 : '',
      //   items_count_3: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_count_3 : '',
      //   items_weight_3: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_weight_3 : '',
      //   items_length_3: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_length_3 : '',
      //   items_width_3: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_width_3 : '',
      //   items_height_3: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_height_3 : '',
      //   items_price_3: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_3 : '',
      //   items_price_currency_3: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_currency_3 : '',
      //
      //   client_comment_4: this.storage.data.itemsParams ? this.storage.data.itemsParams.client_comment_4 : '',
      //   items_type_4: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_type_4 : '',
      //   delivery_type_4: this.storage.data.itemsParams ? this.storage.data.itemsParams.delivery_type_4 : '',
      //   items_count_4: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_count_4 : '',
      //   items_weight_4: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_weight_4 : '',
      //   items_length_4: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_length_4 : '',
      //   items_width_4: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_width_4 : '',
      //   items_height_4: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_height_4 : '',
      //   items_price_4: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_4 : '',
      //   items_price_currency_4: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_currency_4 : '',
      // }),
    });
  }

  private  checkWhoPaythis () {
    const val = this.form.get('stock_pay').value;
    this.isWhoPay = this.helpersService.whoPay(val);
  }

  setValueAutocomplete () {
   for (let item of Object.keys(this.fromNames)) {
      this.fromNames[item].value = this.storage.data[this.fromNames[item].name];
   }
  }



}
