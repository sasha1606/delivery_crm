import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component ({
    selector: 'app-cabinet',
    templateUrl: './cabinet.component.html',
    styleUrls: ['./cabinet.component.css'],
})

export class CabinetComponent implements  OnInit {

    openMenu = false;

    constructor (
       private router: Router
    ) {}

    ngOnInit () {
        // this.router.navigate(['/login']);
    }

    stateMenu (state) {
        this.openMenu = state;
    }
}
