import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';

import { TransportationsService } from './transportations.service';

import { TransportationsListGet } from './transportations';


@Component ({
    selector: 'app-transportations',
    templateUrl: './transportations.component.html',
    styleUrls: ['./transportations.component.css'],
})

export class TransportationsComponent implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    title = 'Перевозчики';
    transportationList: TransportationsListGet;

    page = 1;
    keyWord = '';
    sort = '';

    constructor (
      private activatedRoute: ActivatedRoute,
      private transportationsService: TransportationsService
    ) {}

    ngOnInit () {
      this.transportationList = this.activatedRoute.snapshot.data['transportations'];
    }

    ngOnDestroy () {
      this.destroy$.next(true);
      this.destroy$.unsubscribe();
    }

    public tablePage (page: number): void {
      this.page = page;
      this.sendSort();
    }

    public tableSort (param: string): void {
      if (this.sort.length > 0 && param === this.sort) {
        param = `-${param}`;
      }
      this.sort = param;
      this.sendSort();
    }

    public tableSearch (word: string): void {
      this.keyWord = word;
      this.sendSort();
    }

    private sendSort () {
      this.transportationsService.search({keyword: this.keyWord,
                                          page: this.page,
                                          sort: this.sort
                                        }).takeUntil(this.destroy$).subscribe(res => {
        this.transportationList = res;
      });
    }

}
