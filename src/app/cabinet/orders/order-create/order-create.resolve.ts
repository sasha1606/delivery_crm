import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { OrdersService } from '../orders.service';

@Injectable()

export class OrderCreateResolve implements Resolve<any> {

  constructor (
    private ordersService: OrdersService
  ) {}

  resolve () {
    return this.ordersService.dataCreateOrders();
  }
}
