import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';

import { TokenStorageService } from './token-storage.service';
import { UserInfoService } from './cabinet/user.info.service';

import { ApiSrc } from './api/api.src';

@Injectable()

export class AuthInterceptor implements HttpInterceptor {
    // https://theinfogrid.com/tech/developers/angular/building-http-interceptor-angular-5/
    private apiKey = 'user-token';

    constructor (
        private tokenStorageService: TokenStorageService,
        private userInfoService: UserInfoService,
        private router: Router,
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const ApiToken = this.tokenStorageService.token;

        if (ApiToken) {
            req = req.clone({setHeaders: {[this.apiKey]: `${ApiToken}`}});
        }

        req = req.clone({setHeaders: {
            'api-key': ApiSrc.apiKey,
        }});
      // 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'

        return next.handle(req).pipe(
            tap(
                (response: HttpResponse<any>) => {
                    if (response.headers && response.headers.get(this.apiKey)) {
                        this.tokenStorageService.token = response.headers.get(this.apiKey);

                        // if (!this.userInfoService.userId) {
                        //     this.userInfoService.saveUserId(response.headers.get(this.apiKey));
                        // }
                    }
                 },
                (error: HttpErrorResponse) => {
                    if (error.status === 403 && !this.tokenStorageService.token) {
                        this.router.navigateByUrl('/login');
                    } else if (error.status === 401) {
                        this.tokenStorageService.deleteToken();
                        this.router.navigateByUrl('/login');
                    }
                }
            )
        );
    }
}

