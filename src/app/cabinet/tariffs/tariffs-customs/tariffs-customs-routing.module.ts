import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TariffsCustomsComponent} from './tariffs-customs.component';

import { TariffsCustomsResolve } from './tariffs-customs.resolve';

const ROUTERS: Routes = [
    {
        path: '',
        component: TariffsCustomsComponent,
        resolve: {
          tariffs: TariffsCustomsResolve,
        }
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class TariffsCustomsRoutingModule {}
