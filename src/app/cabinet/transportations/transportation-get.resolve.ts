import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

import { TransportationsService } from './transportations.service';

@Injectable()

export class TransportationGetResolve implements Resolve<any> {

  constructor (
    private transportationsService: TransportationsService,
    private router: Router
  ) {}

  resolve ( activatedRouteSnapshot: ActivatedRouteSnapshot ) {
    return this.transportationsService.getTransportation(Number(activatedRouteSnapshot.paramMap.get('id'))).catch(err => {
      this.router.navigateByUrl('/cabinet/transportations');
      return Observable.of({error: err});
    });
  }

}
