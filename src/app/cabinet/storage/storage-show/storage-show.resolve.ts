import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

import { StorageService } from '../storage.service';

@Injectable()

export class StorageShowResolve implements Resolve<any> {

  constructor (
    private storageService: StorageService,
    private router: Router
  ) {}

  resolve ( activatedRouteSnapshot: ActivatedRouteSnapshot ) {
    return this.storageService.getStorages(Number(activatedRouteSnapshot.paramMap.get('id'))).catch(err => {
      this.router.navigateByUrl('/cabinet/storage');
      return Observable.of({error: err});
    });
  }

}
