import { Component, Input, OnInit } from '@angular/core';

@Component ({
    selector: 'app-dashboard-profit',
    templateUrl: './dashboard-profit.component.html',
    styleUrls: ['./dashboard-profit.component.css'],
})

export class DashboardProfitComponent implements OnInit {

    @Input() profit;

    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true
    };
    // public barChartLabels: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    // public barChartData: any[] = [
    //   {data: [65, 59, 80, 81, 56, 55, 40, 20], label: 'Series A'}
    // ];

    public barChartType = 'bar';
    public barChartLegend = false;

    public barChartData: any[];
    public barChartLabels: string[];

    ngOnInit () {
      this.barChartData = this.profit.data;
      this.barChartLabels = this.profit.line;
    }
}
