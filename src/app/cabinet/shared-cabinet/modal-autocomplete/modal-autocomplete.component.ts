import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  ViewChild,
  ElementRef,
  ViewContainerRef, Output, EventEmitter
} from '@angular/core';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormControl, FormBuilder, FormGroup} from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import { StorageService } from '../../storage/storage.service';

import { AutocompleteService } from '@shared/services/autocomplete.service';

import { ToastsManager } from 'ng2-toastr';

@Component({
  selector: 'app-modal-autocomplete',
  templateUrl: 'modal-autocomplete.component.html',
  styleUrls: ['modal-autocomplete.component.css'],
})

export class ModalAutocompleteComponent implements OnInit, OnDestroy, OnChanges {

  @Input() ids: number[] = [];
  @Input() showModal?;
  @Input() bntName = 'Запросить ценовое предложение';
  @Input() hideTransportersTypeCarriers = false;
  @Input() methodFromServiceToSendAllInfo;
  @Output() closeModal: EventEmitter<boolean> = new EventEmitter();
  @ViewChild('modalContentRequestQuery') modalElem: ElementRef;

  public formModal: FormGroup;
  public transporters;
  public directions;
  private destroy$: Subject<boolean> = new Subject<boolean>();
  private modalRef: NgbModalRef;

  constructor (
    private modalService: NgbModal,
    private fb: FormBuilder,
    private modalAutocompleteService: AutocompleteService,
    private storageService: StorageService,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit () {

  }

  ngOnDestroy () {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  ngOnChanges (changes: SimpleChanges) {
    // if (changes.showModal.currentValue) {
    //   this.open('content');
    // }
  }

  public open (content): void {
    this.modalRef = this.modalService.open(this.modalElem, {size: 'lg'});
    this.buildModalForm();
    this.changeModalSelect();
  }


  public changeModalSelect () {

    this.formModal.get('transportersType').valueChanges.subscribe(type => {
      if (!type) {
        return;
      }

      this.formModal.get('transporterId').setValue(null);
      this.formModal.get('name').setValue(null);
      this.formModal.get('phone').setValue(null);
      this.formModal.get('email').setValue(null);
      this.formModal.get('responsible_person').setValue(null);
      this.directions = null;
      this.transporters = null;
      this.modalAutocompleteService.changetransportersType(type, true).takeUntil(this.destroy$).subscribe( res => {
        if (res.success === 'ok') {
          this.transporters = res;
        }
      });
    });

    this.formModal.get('transporterId').valueChanges.subscribe(id => {
      if (!id) {
        return;
      }
      this.formModal.get('directionId').setValue(null);
      this.formModal.get('name').setValue(null);
      this.formModal.get('phone').setValue(null);
      this.formModal.get('email').setValue(null);
      this.formModal.get('responsible_person').setValue(null);
      this.directions = null;

      this.modalAutocompleteService.changeTransporterId({
        transportersType: this.formModal.get('transportersType').value,
        transporterId: id,
      })
        .takeUntil(this.destroy$).subscribe( res => {
        if (res.success === 'ok') {
          this.directions = res;
        }
      });
    });

    this.formModal.get('directionId').valueChanges.subscribe(id => {
      if (!id) {
        return;
      }

      this.modalAutocompleteService.changeDirections({
        transportersType: this.formModal.get('transportersType').value,
        transporterId: this.formModal.get('transporterId').value,
        directionId: id,
      })
        .takeUntil(this.destroy$).subscribe( res => {
        this.formModal.get('name').setValue(res.directioninfo.name);
        this.formModal.get('phone').setValue(res.directioninfo.phone);
        this.formModal.get('email').setValue(res.directioninfo.email);
        this.formModal.get('responsible_person').setValue(res.directioninfo.responsible_person);
      });
    });
  }

  public onSubmitModal (): void {
    if (this.formModal.valid) {
      this.storageService[this.methodFromServiceToSendAllInfo](this.formModal.value).takeUntil(this.destroy$).subscribe( res => {
        if (res.success === 'ok') {
          this.toastr.success('Successfully');
          this.closeModal.emit(true);
          this.modalRef.close();
        }  else {
          this.toastr.error('Error');
        }
      });
      //
    } else {
    }
  }

  private buildModalForm () {
    const ids = JSON.stringify(this.ids);
    this.formModal = this.fb.group({
      transportersType: '',
      transporterId: '',
      directionId: '',
      name: '',
      email: '',
      phone: '',
      responsible_person: '',
      ordersIds: ids,
    });
  }
}
