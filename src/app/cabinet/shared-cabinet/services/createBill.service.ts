import { Injectable } from '@angular/core';

import { ApiSrc } from '@api/api.src';

import { ApiService } from '@api/api.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()

export class CreateBillService {

  constructor ( private apiService: ApiService ) {}

  public getListOrders ({date, userId}) {
    return this.apiService.get(`${ApiSrc.customersUserOrder.list}/${userId}?date=${date}&no_pagination=${true}`);

    // let  par = new HttpParams()
    // par = par.append('date', date);
    // par = par.append('no_pagination', 'true');
    // return this.apiService.get(`${ApiSrc.customersUserOrder.list}/${userId}`, {params: par});
  }

  public sendBill (ids) {
    return this.apiService.post(ApiSrc.customerUsers.issueInvoice, ids);
  }

  public getLinkToDocument (data) {
    return this.apiService.post(ApiSrc.customerUsers.pdfOrderInvoice, {ordersIds: data.orderIds});
  }
}
