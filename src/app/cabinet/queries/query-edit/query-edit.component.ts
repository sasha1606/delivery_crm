import { Component, OnInit, OnDestroy, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormControl, FormBuilder, FormGroup} from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { QueriesService } from '../queries.service';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';

import { FromValidation } from '@fromValidation';

import { QueriesGet } from '../queries';

import { maskPhone } from '@shared/const/masks';
import { ToastsManager } from 'ng2-toastr';
import { ValidateOnlyNumber } from '@shared/formValidators/validateOnlyNumber';
import { AutocompleteCity } from '@shared/interface/autocomplete-city';

@Component ({
    selector: 'app-query-edit',
    templateUrl: '../query-crud.component.html',
    styleUrls: ['./query-edit.component.css']
})

export class QueryEditComponent extends FromValidation implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    public maskPhone =  maskPhone;
    public ordersinfoTable;

    title = 'Редактирование запроса';
    form: FormGroup;

    query: QueriesGet;
    idUser: number;

    nds = 0;
    summ = 0;
    tariffs = null;
    public showFrom: boolean;
    public editFrom: boolean;

    public local_from = [];
    public local_to = [];
    // public test1;
    public userInfo;

    public autocompleteCityFromFormComponent: AutocompleteCity;
    public autocompleteCityToFormComponent: AutocompleteCity;

    constructor (
        public router: Router,
        public queriesService: QueriesService,
        private findToAutocompleteService: FindToAutocompleteService,
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef,
    ) {
      super (
        router,
        queriesService
      );
      this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit () {
        this.editFrom = true;
        this.query = this.activatedRoute.snapshot.data['query'];
        this.buildFrom();
        this.activatedRoute.params.subscribe(param => {
            this.idUser = +param['id'];
        });
        this.buildAutocompleteCityToFormComponent();
        this.buildAutocompleteCityFromFormComponent();
    }

    ngOnDestroy () {
      this.destroy$.next(true);
      this.destroy$.unsubscribe();
    }

  // /*========= LOCATION =========*/
  //
  //
  //
  // public isLiction;
  // onGetLocation(city) {
  //   this.isLiction = this.findToAutocompleteService.findLocation(city)
  //     .subscribe(res => {
  //       this.local_from = res.data;
  //       this.local_to = res.data;
  //       if(res !== 0 || res !== '' || res !== 'null') {
  //
  //       }
  //     });
  //
  // }
  //
  // onGetLocationFrom(event) {
  //   this.local_from = [];
  //   const test2 = event.target.value;
  //   this.onGetLocation(test2);
  // }
  //
  // onSelectLocationFrom(event) {
  //   // this.titleCountry_from = event.countryName;
  //   // this.postalCode_from = event.postal_code;
  //   // this.titleRegion_from = event.region;
  //   if (!event) {
  //     return false;
  //   }
  //   this.form.get('country_from').setValue(event.countryName);
  //   this.form.get('zip_code_from').setValue(event.postal_code);
  //   this.form.get('region_from').setValue(event.region);
  //
  // }
  //
  // onGetLocationTo(event) {
  //   this.local_to = [];
  //   const test2 = event.target.value || this.test1;
  //   this.onGetLocation(test2);
  // }
  //
  // onSelectLocationTo(event) {
  //   // this.titleCountry_to = event.countryName;
  //   // this.postalCode_to = event.postal_code;
  //   // this.titleRegion_to = event.region;
  //   if (!event) {
  //     return false;
  //   }
  //   this.form.get('country_to').setValue(event.countryName);
  //   this.form.get('zip_code_to').setValue(event.postal_code);
  //   this.form.get('region_to').setValue(event.region);
  // }
  //
  // /*========= / LOCATION =========*/

  /*------FindUser----------*/

  onGetUserInfo(event) {
    this.findToAutocompleteService.findUser(event.target.value)
      .subscribe(res => {
        this.userInfo = res['data'];
      });
  }

  onSelectUserInfo(event) {
    // this.userCompany = event.company;
    // this.userEmail = event.email;
    // this.userPhone = event.phone;
    if (!event) {
      return false;
    }
    this.form.get('company').setValue(event.company);
    this.form.get('email').setValue(event.email);
    this.form.get('phone').setValue(event.phone);
  }

  /*------ / FindUser----------*/

  public onSubmit (form): void {
    if (this.form.valid) {
      this.queriesService.updateQueries(this.form.value, this.idUser).takeUntil(this.destroy$).subscribe( res => {
        if (res.success === 'ok') {
          this.toastr.success('Has been created successfully');
          setTimeout(() => {
            this.router.navigateByUrl('/cabinet/queries');
          }, 1000);
        }  else {
          this.toastr.error('Error');
          this.validationFormBackEnd(res.validation, this.form);
        }
      });

    } else {
      this.toastr.error('Error');
      this.validationFormsSubmit(this.form);
    }
  }

   private buildFrom (): void {
      this.form = this.fb.group({
        status: [this.query.data.status],
        name: [this.query.data.name],
        company: [this.query.data.company],
        email: this.query.data.email,
        phone: this.query.data.phone,
        // country_from: this.query.data.country_from,
        // city_from: this.query.data.city_from,
        address_from: this.query.data.address_from,
        // region_from: this.query.data.region_from,
        // zip_code_from: this.query.data.zip_code_from,
        type: this.query.data.type,
        // insurance: this.query.data.insurance,
        // insurance_price: this.query.data.insurance_price,
        items_price: this.query.data.items_price,
        // country_to: this.query.data.country_to,
        // city_to: this.query.data.city_to,
        address_to: this.query.data.address_to,
        // region_to: this.query.data.region_to,
        // zip_code_to: this.query.data.zip_code_to,
        items_count: this.query.data.items_count,
        // insurance_currency: this.query.data.insurance_currency,
        items_totalweight: this.query.data.items_totalweight,
        // itemsParams: this.fb.group({
        //   client_comment_1: this.query.data.itemsParams ? this.query.data.itemsParams.client_comment_1 : '',
        //   items_type_1: this.query.data.itemsParams ? this.query.data.itemsParams.items_type_1 : '',
        //   delivery_type_1: this.query.data.itemsParams ? this.query.data.itemsParams.delivery_type_1 : '',
        //   items_count_1: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_count_1 : '', ValidateOnlyNumber],
        //   items_weight_1: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_weight_1 : '', ValidateOnlyNumber],
        //   items_length_1: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_length_1 : '', ValidateOnlyNumber],
        //   items_width_1: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_width_1 : '', ValidateOnlyNumber],
        //   items_height_1: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_height_1 : '', ValidateOnlyNumber],
        //   items_price_1: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_price_1 : '', ValidateOnlyNumber],
        //   items_price_currency_1: this.query.data.itemsParams ? this.query.data.itemsParams.items_price_currency_1 : '',
        //
        //   client_comment_2: this.query.data.itemsParams ? this.query.data.itemsParams.client_comment_2 : '',
        //   items_type_2: this.query.data.itemsParams ? this.query.data.itemsParams.items_type_2 : '',
        //   delivery_type_2: this.query.data.itemsParams ? this.query.data.itemsParams.delivery_type_2 : '',
        //   items_count_2: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_count_2 : '', ValidateOnlyNumber],
        //   items_weight_2: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_weight_2 : '', ValidateOnlyNumber],
        //   items_length_2: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_length_2 : '', ValidateOnlyNumber],
        //   items_width_2: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_width_2 : '', ValidateOnlyNumber],
        //   items_height_2: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_height_2 : '', ValidateOnlyNumber],
        //   items_price_2: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_price_2 : '', ValidateOnlyNumber],
        //   items_price_currency_2: this.query.data.itemsParams ? this.query.data.itemsParams.items_price_currency_2 : '',
        //
        //   client_comment_3: this.query.data.itemsParams ? this.query.data.itemsParams.client_comment_3 : '',
        //   items_type_3: this.query.data.itemsParams ? this.query.data.itemsParams.items_type_3 : '',
        //   delivery_type_3: this.query.data.itemsParams ? this.query.data.itemsParams.delivery_type_3 : '',
        //   items_count_3: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_count_3 : '', ValidateOnlyNumber],
        //   items_weight_3: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_weight_3 : '', ValidateOnlyNumber],
        //   items_length_3: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_length_3 : '', ValidateOnlyNumber],
        //   items_width_3: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_width_3 : '', ValidateOnlyNumber],
        //   items_height_3: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_height_3 : '', ValidateOnlyNumber],
        //   items_price_3: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_price_3 : '', ValidateOnlyNumber],
        //   items_price_currency_3: this.query.data.itemsParams ? this.query.data.itemsParams.items_price_currency_3 : '',
        //
        //   client_comment_4: this.query.data.itemsParams ? this.query.data.itemsParams.client_comment_4 : '',
        //   items_type_4: this.query.data.itemsParams ? this.query.data.itemsParams.items_type_4 : '',
        //   delivery_type_4: this.query.data.itemsParams ? this.query.data.itemsParams.delivery_type_4 : '',
        //   items_count_4: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_count_4 : '', ValidateOnlyNumber],
        //   items_weight_4: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_weight_4 : '', ValidateOnlyNumber],
        //   items_length_4: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_length_4 : '', ValidateOnlyNumber],
        //   items_width_4: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_width_4 : '', ValidateOnlyNumber],
        //   items_height_4: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_height_4 : '', ValidateOnlyNumber],
        //   items_price_4: [ this.query.data.itemsParams ? this.query.data.itemsParams.items_price_4 : '', ValidateOnlyNumber],
        //   items_price_currency_4: this.query.data.itemsParams ? this.query.data.itemsParams.items_price_currency_4 : '',
        // }),
        items_type: this.query.data.items_type,
        client_comment: this.query.data.client_comment,
        delivery_price: this.query.data.delivery_price,
        rate: this.query.data.rate,
        user_comment: this.query.data.user_comment,
      });
    }

    private buildAutocompleteCityToFormComponent () {
      this.autocompleteCityToFormComponent = {
        searchSelectAutocomplete: {
          formControlName: 'city_to',
          label: 'Город получателя',
          value: this.query.data.city_to,
        },
        searchInfoAutocomplete: {
          country: {
            formControlName: 'country_to',
            label: 'Страна получателя',
            value: this.query.data.country_to,
            searchEvent: 'countryName',
          },
          region: {
            formControlName: 'region_to',
            label: 'Район/Штат получателя',
            value: this.query.data.region_to,
            searchEvent: 'region',
          },
          zip_code: {
            formControlName: 'zip_code_to',
            label: 'Почтовый индекс получателя',
            value: this.query.data.zip_code_to,
            searchEvent: 'postal_code',
          }
        }
      }
    }

    private buildAutocompleteCityFromFormComponent () {
      this.autocompleteCityFromFormComponent = {
        searchSelectAutocomplete: {
          formControlName: 'city_from',
          label: 'Город отправителя',
          value: this.query.data.city_from,
        },
        searchInfoAutocomplete: {
          country: {
            formControlName: 'country_from',
            label: 'Страна отправителя',
            value: this.query.data.country_from,
            searchEvent: 'countryName',
          },
          region: {
            formControlName: 'region_from',
            label: 'Район/Штат отправителя',
            value: this.query.data.region_from,
            searchEvent: 'region',
          },
          zip_code: {
            formControlName: 'zip_code_from',
            label: 'Почтовый индекс получателя',
            value: this.query.data.zip_code_from,
            searchEvent: 'postal_code',
          }
        }
      }
    }

}
