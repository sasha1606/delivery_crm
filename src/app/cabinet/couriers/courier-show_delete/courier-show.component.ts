import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component ({
    selector: 'app-courier-show',
    templateUrl: './courier-show.component.html',
    styleUrls: ['./courier-show.component.css']
})

export class CourierShowComponent implements OnInit {
    title = 'Просмотр курьера';
    form: FormGroup;

    userId: number;

    constructor (
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private fb: FormBuilder
    ) {}

    ngOnInit () {
        this.buildForm();
        this.form.disable();

        this.activatedRoute.params.subscribe(param => {
            this.userId = +param['id'];
        });

        if (isNaN(this.userId)) {
            this.router.navigate(['/cabinet/customers-users']);
        } else {

        }
    }

    buildForm () {
        this.form = this.fb.group({
            personalCode: ['', [
                Validators.required,
            ]],
            fio: ['', [
                Validators.required,
            ]],
            email: ['', [
                Validators.required,
            ]],
            tel: ['', [
                Validators.required,
            ]],
            birthday: ['', [
                Validators.required,
            ]],
            country: ['', [
                Validators.required,
            ]],
            city: ['', [
                Validators.required,
            ]],
            zipCode: ['', [
                Validators.required,
            ]],
            address: ['', [
                Validators.required,
            ]],
            house: ['', [
                Validators.required,
            ]],
            housing: ['', [
                Validators.required,
            ]],
            flat: ['', [
                Validators.required,
            ]],
            comment: ['', [
                Validators.required,
            ]],
            file: ['', [
                Validators.required,
            ]],
        });
    }

    onSubmit (form) {
        if (this.form.valid) {

        } else {
            Object.keys(this.form.controls).forEach(field => {
                const control = this.form.get(field);
                control.markAsTouched({ onlySelf: true });
                control.markAsDirty({onlySelf: true});
            });

        }
    }
}
