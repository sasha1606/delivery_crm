import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiSrc } from '@api/api.src';

import { ApiService } from '@api/api.service';


@Injectable()

export class TariffsTransportationsService {

  constructor (
    private apiService: ApiService,
  ) {}

  public getTariffsTransportations (): Observable<any> {
    return this.apiService.get(ApiSrc.tariffsTransportations.tariffs);
  }
}
