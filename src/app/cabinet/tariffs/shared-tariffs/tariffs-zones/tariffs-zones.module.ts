import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TariffsZonesComponent } from './tariffs-zones.component';

import { PipeModule } from '@shared/pipes/pipe.module';

@NgModule({
  imports: [
    CommonModule,
    PipeModule
  ],
  exports: [
    TariffsZonesComponent
  ],
  declarations: [
    TariffsZonesComponent
  ],
})

export class TariffsZonesModule {

}
