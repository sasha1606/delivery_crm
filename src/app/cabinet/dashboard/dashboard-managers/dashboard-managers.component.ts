import { Component, Input, OnInit } from '@angular/core';

@Component ({
    selector: 'app-dashboard-managers',
    templateUrl: './dashboard-managers.component.html',
    styleUrls: ['./dashboard-managers.component.css'],
})

export class DashboardManagersComponent implements OnInit {

    @Input() managersList;
    public managersListData: any;

    // managersListData: any = [
    //     {
    //         name: 'Abraham',
    //         task: 22,
    //         overdue: 5,
    //     },
    //     {
    //         name: 'Abraham',
    //         task: 22,
    //         overdue: 5,
    //     },
    //     {
    //         name: 'Abraham',
    //         task: 22,
    //         overdue: 5,
    //     },
    //     {
    //         name: 'Abraham',
    //         task: 22,
    //         overdue: 0,
    //     },
    //     {
    //         name: 'Abraham',
    //         task: 22,
    //         overdue: 5,
    //     },
    //     {
    //         name: 'Abraham',
    //         task: 22,
    //         overdue: 0,
    //     },
    // ];

    ngOnInit () {
      this.managersListData = this.managersList;
    }
}
