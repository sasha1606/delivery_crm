import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistoryOrderTableComponent } from './history-order-table.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    HistoryOrderTableComponent,
  ],
  declarations: [
    HistoryOrderTableComponent,
  ]
})

export class HistoryOrderTableModule {

}
