import {Component, OnInit, OnDestroy, ViewContainerRef, ViewChild} from '@angular/core';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { CustomersUsersService } from '../customers-users.service';

import { CostumerUserGet, CostumerUser } from '../costumer-user';

import { FromValidation } from '@fromValidation';
import { ToastsManager } from 'ng2-toastr';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';

import { maskPhone } from '@shared/const/masks';
import { AddDirectionTableComponent } from '@shared/add-direction-table/add-direction-table.component';
import {debounceTime, switchMap, tap} from 'rxjs/operators';
import {AutocompleteCity} from '@shared/interface/autocomplete-city';

@Component ({
    selector: 'app-customer-user-create',
    templateUrl: './customer-user-create.component.html',
    styleUrls: ['./customer-user-create.component.css']
})

export class CustomerUserCreateComponent extends FromValidation implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    @ViewChild(AddDirectionTableComponent) addDirectionTableComponent;

    public maskPhone =  maskPhone;

    public title = 'Добавление нового пользователя';
    public form: FormGroup;
    public  user: CostumerUser;
    public  data: CostumerUserGet;
    // public  autocompleteLoading = false;

    public local_from = [];

    public autocompleteCityFormComponent: AutocompleteCity;

    // private autocompleteSubject$ = new Subject();

    constructor (
        public customersUsersService: CustomersUsersService,
        public router: Router,
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef,
        private findToAutocompleteService: FindToAutocompleteService
    ) {
        super(
            router,
            customersUsersService,
        );
      this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit () {
        this.data = this.activatedRoute.snapshot.data['data'];
        this.buildForm();
        // this.autocompleteSubscribe();
        this.buildAutocompleteCityFormComponent();
    }

    ngOnDestroy () {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

    /*========= LOCATION =========*/
    // private onGetLocation(city) {
    //   this.autocompleteSubject$.next(city);
    // }
    //
    // public onGetLocationFrom(event) {
    //   this.local_from = [];
    //   this.onGetLocation(event.target.value);
    // }
    //
    // public onSelectLocationFrom(event) {
    //   if (!event) {
    //     return false;
    //   }
    //   this.form.get('country').setValue(event.countryName);
    //   this.form.get('region').setValue(event.region);
    //   this.form.get('zip_code').setValue(event.postal_code);
    // }
    /*========= / LOCATION =========*/


    public onSubmit (form): void {
        if (this.form.valid) {
            this.form.value.contract_tariff_data = JSON.stringify(this.addDirectionTableComponent.getInputValue());
            this.customersUsersService.createUser(this.form.value).takeUntil(this.destroy$).subscribe( res => {
                if (res.success === 'ok') {
                  this.toastr.success('Has been created successfully');
                  setTimeout(() => {
                    this.router.navigateByUrl('/cabinet/customers-users');
                  }, 1000);
                }  else {
                    this.toastr.error('Error');
                    this.validationFormBackEnd(res.validation, this.form);
                }
            });

        } else {
            this.toastr.error('Error');
            this.validationFormsSubmit(this.form);
        }
    }

    private buildForm (): void {
        this.form = this.fb.group({
            type: [this.data.data.type, [
                Validators.required,
            ]],
            login: ['', [
                Validators.required,
            ]],
            fio: ['', [
                Validators.required,
            ]],
            email: ['', [
                Validators.required,
            ]],
            phone: ['', [
                Validators.required,
            ]],
            company: [''],
            company_code: [''],
            // country: [''],
            // city: [''],
            // address: [''],
            // postal_address: [''],
            // region: [''],
            // zip_code: [''],
            discount: [''],
            contract_tariff: [''],
            filesUpload: [''],
            contract_tariff_data: [''],
        });
    }

    // private autocompleteSubscribe () {
    //   this.autocompleteSubject$.pipe(
    //     tap(() => this.autocompleteLoading = true),
    //     debounceTime(400),
    //     switchMap( (city) => {
    //       return this.findToAutocompleteService.findLocation(city);
    //     })
    //   ).subscribe(res => {
    //     this.local_from = res.data;
    //     this.autocompleteLoading = false;
    //   });
    // }

  private buildAutocompleteCityFormComponent () {
    this.autocompleteCityFormComponent = {
      searchSelectAutocomplete: {
        formControlName: 'city',
        label: 'Город',
        value: '',
      },
      searchInfoAutocomplete: {
        country: {
          formControlName: 'country',
          label: 'Страна',
          value: '',
          searchEvent: 'countryName',
        },
        address: {
          formControlName: 'address',
          label: 'Адрес',
          value: '',
          searchEvent: null,
        },
        postal_address: {
          formControlName: 'postal_address',
          label: 'Почтовый адрес',
          value: '',
          searchEvent: null,
        },
        region: {
          formControlName: 'region',
          label: 'Район/Штат',
          value: '',
          searchEvent: 'region',
        },
        zip_code: {
          formControlName: 'zip_code',
          label: 'Индекс',
          value: '',
          searchEvent: 'postal_code',
        }
      }
    }
  }
}
