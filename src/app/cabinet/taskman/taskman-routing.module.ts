import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TaskmanComponent } from './taskman.component';

import { TaskmanResolve } from './taskman.resolve';

const ROUTERS: Routes = [
    {
        path: '',
        component: TaskmanComponent,
        resolve: {
          tasks: TaskmanResolve,
        }
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class TaskmanRoutingModule {}
