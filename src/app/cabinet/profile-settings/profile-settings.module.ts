import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';

import { ProfileSettingsRoutingModule } from './profile-settings-routing.module';

import { ProfileSettingsComponent } from './profile-settings.component';
import { ProfileSettingsEditComponent } from './profile-settings-edit/profile-settings-edit.component';

import { ProfileSettingsResolve } from './profile-settings.resolve';

import { ProfileSettingsService } from './profile-settings.service';
import { ProfileSettingsEditResolve } from './profile-settings-edit/profile-settings-edit.resolve';

@NgModule ({
    imports: [
        CommonModule,
        ProfileSettingsRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        TextMaskModule,
    ],
    declarations: [
        ProfileSettingsComponent,
        ProfileSettingsEditComponent,
    ],
    providers: [
        ProfileSettingsService,
        ProfileSettingsResolve,
        ProfileSettingsEditResolve,
    ]
})

export class ProfileSettingsModule { }
