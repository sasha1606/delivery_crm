import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StorageDealComponent } from './storage-deal.component';

import { StorageDealResolve } from './storage-deal.resolve';

const ROUTERS: Routes = [
    {
        path: '',
        component: StorageDealComponent,
        resolve: {
          storage: StorageDealResolve,
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class StorageDealRoutingModule {}
