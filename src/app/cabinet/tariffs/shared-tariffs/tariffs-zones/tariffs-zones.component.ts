import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tariffs-zone',
  templateUrl: './tariffs-zones.component.html',
  styleUrls: ['./tariffs-zones.component.css'],
})

export class TariffsZonesComponent implements OnInit {

  @Input() zones = '';

  ngOnInit () {
  }

}
