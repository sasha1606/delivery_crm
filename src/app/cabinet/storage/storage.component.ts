import {Component, OnInit, OnDestroy, ChangeDetectorRef} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { StorageService } from './storage.service';

import { StorageList } from './storage';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HelpersService } from '@shared/services/helpers.service';

@Component ({
    selector: 'app-storage',
    templateUrl: 'storage.component.html',
    styleUrls: ['storage.component.css']
})

export class StorageComponent implements OnInit, OnDestroy {
    public title = 'Склад';

    public managerList = {};
    public managerId: number;

    public storageList: StorageList;

    public statusForSort;

    public storageIds = [];

    // filter
    public sort = '';
    public page = 1;
    public search = '';
    public date = '';
    public status = '';

    public isChecked: any = null;

    private destroy$: Subject<boolean> = new Subject<boolean>();

    constructor (
        private modalService: NgbModal,
        private storageService: StorageService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private helpersService: HelpersService,
        private cdr: ChangeDetectorRef,
    ) {}

    ngOnInit () {
      this.routerEvents();
      this.storageList = this.activatedRoute.snapshot.data['storageList'];
      this.managerList = this.storageList.listmanagers;
    }

    ngOnDestroy () {
      this.destroy$.next(true);
      this.destroy$.unsubscribe();
    }

    public tablePage (page: number): void {
      this.page = page;
      this.sendSort();
    }

    public tableSort (param: string): void {
      if (this.sort.length > 0 && param === this.sort) {
        param = `-${param}`;
      }
      this.sort = param;
      this.sendSort();
    }

    public tableSearch (word: string): void {
      this.search = word;
      this.sendSort();
    }

    public getSearchData (data)  {
      this.date = data.data;
      this.search = data.word;
      this.managerId = data.anotherFilter;
      this.sendSort();
    }

    public open (content) {
        this.modalService.open(content, {size: 'lg'});
    }

    public triggerCheckboxId (storageId) {
      this.storageIds = this.helpersService.getCheckboxIds(storageId, this.storageIds);
    }


    public clearCheckbox () {
      // this.cdr.detectChanges();
      this.isChecked = this.isChecked === false ? null : false;
      this.storageIds = [];
    }

    private sendSort (): void {
      this.storageService.search({
        sort: this.sort,
        key: this.search,
        page: this.page,
        date: this.date,
        manager: this.managerId,
      }).takeUntil(this.destroy$).subscribe( response => {
        this.storageList = response;
      });
    }

    private getInitData () {
      this.storageService.getStoragesList().takeUntil(this.destroy$).subscribe(response => {
        this.storageList = response;
      });
    }

    private routerEvents () {
      this.router.events.takeUntil(this.destroy$).subscribe((e: any) => {
        if (e instanceof NavigationEnd) {
         this.getInitData();
        }
      });
    }
}

