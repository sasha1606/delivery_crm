import { Injectable } from '@angular/core';
import { ApiSrc } from '@api/api.src';

import { ApiService } from '@api/api.service';

@Injectable()

export class AutocompleteService {

  constructor (
    private apiService: ApiService
  ) {}

  public changetransportersType(type: string, formanifest = false) {

    const data = { transportersType: type }
    if (formanifest) {
      data['formanifest'] = true;
    }
    return this.apiService.post(ApiSrc.queries.transportersType, data);
  }

  public changeTransporterId(form: object) {
    return this.apiService.post(ApiSrc.queries.transporterId, form);
  }

  public changeDirections(form: object) {
    return this.apiService.post(ApiSrc.queries.directions, form);
  }

  public modalSendForm (form: object) {
    return this.apiService.post(ApiSrc.queries.modalSend, form);
  }
}

