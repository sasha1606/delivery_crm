import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QueriesPriceComponent } from './queries-price.component';
// import { QueryPriceCreateComponent } from './query-price-create/query-price-create.component';
import { QueryPriceShowComponent } from './query-price-show/query-price-show.component';
import { QueryPriceEditCreateComponent } from './query-price-edit-create/query-price-edit-create.component';

import { QueriesPriceResolve } from './queries-price.resolve';
import { QueryPriceCreateResolve } from './query-price-edit-create/query-price-create.resolve';
import { QueryPriceEditResolve } from './query-price-edit-create/query-price-edit.resolve';
import { QueryPriceShowResolve } from './query-price-show/query-price-show.resolve';
import { typePage } from './queries-price';

const ROUTERS: Routes = [
    {
        path: '',
        component: QueriesPriceComponent,
        resolve: {
            queriesPrice: QueriesPriceResolve,
        }
    },
    {
      path: 'create',
      component: QueryPriceEditCreateComponent,
      resolve: {
        queriesPrice: QueryPriceCreateResolve,
      },
      data: {
        data: typePage.create,
      }
    },
    {
        path: 'show/:id',
        component: QueryPriceShowComponent,
        resolve: {
            queriesPrice: QueryPriceShowResolve,
        }
    },
    {
        path: 'edit/:id',
        component: QueryPriceEditCreateComponent,
        resolve: {
          queriesPrice: QueryPriceEditResolve,
        },
        data: {
          data: typePage.edit,
        }
    },

];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class QueriesPriceRoutingModule {}
