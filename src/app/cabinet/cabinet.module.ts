import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { AuthGuard } from '../auth.guard';

import { CabinetRoutingModule } from './cabinet-routing.module';
import { DirectiveModule } from '@shared/directives/directive.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { QueriesModule } from './queries/queries.module';
import { OrdersModule } from './orders/orders.module';
import { StorageModule } from './storage/storage.module';
import { AgentsModule } from './agents/agents.module';
import { TransportationsModule } from './transportations/transportations.module';
import { QueriesPriceModule } from './queries-price/queries-price.module';
import { CouriersModule } from './couriers/couriers.module';
import { AdminUsersModule } from './admin-users/admin-users.module';
import { CustomersUsersModule } from './customers-users/customers-users.module';
import { AccountingModule } from './accounting/accounting.module';
import { AnalyticsModule } from './analytics/analytics.module';
import { TariffsDklModule } from './tariffs/tariffs-dkl/tariffs-dkl.module';
import { TariffsTransportationsModule } from './tariffs/tariffs-transportations/tariffs-transportations.module';
import { StorageDealModule } from './tariffs/storage-deal/storage-deal.module';
import { TariffsCustomsModule } from './tariffs/tariffs-customs/tariffs-customs.module';
import { TaskmanModule } from './taskman/taskman.module';
import { ProfileSettingsModule } from './profile-settings/profile-settings.module';

// import { TopBardModule } from './layout/top-bar/top-bar.module';
// import { LeftMenuModule } from './layout/left-menu/left-menu.module';

import { CabinetComponent } from './cabinet.component';
import { TopBarComponent } from './layout/top-bar/top-bar.component';
import { LeftMenuComponent } from './layout/left-menu/left-menu.component';
import { FooterComponent } from './footer/footer.component';

@NgModule ({
    imports: [
        // TransportationsModule,
        // // TopBardModule,
        // // LeftMenuModule,
        // AgentsModule,
        // QueriesModule,
        // DashboardModule,
        // OrdersModule,
        // StorageModule,
        // QueriesPriceModule,
        // CouriersModule,
        // AdminUsersModule,
        // CustomersUsersModule,
        // AccountingModule,
        // AnalyticsModule,
        // TariffsDklModule,
        // TariffsTransportationsModule,
        // StorageDealModule,
        // TariffsCustomsModule,
        // TaskmanModule,
        // ProfileSettingsModule,
        CommonModule,
        NgbModule,
        CabinetRoutingModule,
        DirectiveModule,
    ],
    declarations: [
        CabinetComponent,
        LeftMenuComponent,
        TopBarComponent,
        FooterComponent,
    ],
    providers: [
        // AuthGuard
    ]
})

export class CabinetModule {}
