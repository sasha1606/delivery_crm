import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiService } from '@api/api.service';

import { ApiSrc } from '@api/api.src';

import { AdminUserGet, AdminUser, AdminList } from './admin-user';
import { Response } from '@shared/interface/response';

@Injectable()

export class AdminUsersService {

    constructor (
        private apiService: ApiService,
    ) {}

    public getUsersList (): Observable<AdminList[]> {
       return this.apiService.post(ApiSrc.adminUsers.list, {});
    }

    public search (sort: string = '', key: string = '', page: number = 1): Observable<AdminList[]> {
        // for IE
        const  keyword = encodeURIComponent(key);
        return this.apiService.get(`${ApiSrc.adminUsers.list}?sort=${sort}&keyword=${keyword}&page=${page}`);
    }



    public createUser ( user: AdminUser ): Observable<Response> {
        return this.apiService.post(ApiSrc.adminUsers.createUser, user);
    }

    public dataCreateUser (): Observable<AdminUserGet> {
        return this.apiService.get(ApiSrc.adminUsers.createUser);
    }

    public deleteUser (userId: number): Observable<Response> {
        return  this.apiService.post(ApiSrc.adminUsers.delete, { id: userId});
    }

    public getUser ( userId: number ): Observable<AdminUserGet> {
        return this.apiService.get(`${ApiSrc.adminUsers.editUser}/${userId}`);
    }

    public updateUser ( user: AdminUser, userId: number ): Observable<Response> {
        return this.apiService.post(`${ApiSrc.adminUsers.editUser}/${userId}`, user);
    }
}
