import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InsuranceFormElemComponent } from './insurance-form-elem.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule ({
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ],
  declarations: [
    InsuranceFormElemComponent
  ],
  exports: [
    InsuranceFormElemComponent
  ],
})

export class InsuranceFormElemModule {
}
