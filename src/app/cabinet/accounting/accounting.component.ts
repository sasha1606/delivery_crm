import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { AccountingList } from './accounting';

import { AccountingService } from './accounting.service';
import { HelpersService } from '@shared/services/helpers.service';

import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';


@Component ({
    selector: 'app-accounting',
    templateUrl: './accounting.component.html',
    styleUrls: ['./accounting.component.css'],
})

export class AccountingComponent implements OnInit, OnDestroy {

  public title = 'Бухгалтерия';
  public accountingList: AccountingList;

  destroy$: Subject<boolean> = new Subject<boolean>();

  private page = 1;
  private search = '';
  private date = '';
  private sort = '';

  public isChecked: any = false;
  public isCheckedAll = false;
  private inputIds = [];

  constructor (
    private activatedRoute: ActivatedRoute,
    private accountingService: AccountingService,
    private helpersService: HelpersService,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit () {
    this.accountingList = this.activatedRoute.snapshot.data['accounting'];
  }

  ngOnDestroy () {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  public getSearchData (data)  {
    this.date = data.data;
    this.search = data.word;
    this.sortTable();
  }

  public tablePage (page) {
    this.page = page;
    this.sortTable();
  }

  public tableSort (param: string): void {
    if (this.sort.length > 0 && param === this.sort) {
      param = `-${param}`;
    }
    this.sort = param;
    this.sortTable();
  }

  public checkedInput (id: number): void {
    this.inputIds = this.helpersService.getCheckboxIds(id, this.inputIds);
    this.isCheckedAll = false;
  }

  public downloadReport () {
    if (this.inputIds.length > 0) {
      const link = `/api/crm/bookkeeping/getreport?id=${this.inputIds.join(',')}`;
      window.open( link, '_blank');
      // const url = `/api/crm/bookkeeping/getreport/id=${this.inputIds.join(',')}`;
      // const link = document.createElement('a');
      // link.target = '_blank';
      // link.href = url;
      // link.setAttribute('visibility', 'hidden');
      // link.click();
    }
  }

  public checkedAllCheckbox (event) {
    //  TODO better; this.cdr.detectChanges();
    if (this.isChecked === true &&  event.target.checked === true ||
        this.isChecked === false &&  event.target.checked === false
    ) {
      this.isChecked = 1;
    } else {
      this.isChecked = event.target.checked;
    }


    if (this.isChecked) {
      this.inputIds = [];
      this.accountingList.listdata.forEach( (item) => {
        this.inputIds.push(item.id);
      });
    } else  {
      this.inputIds = [];
    }
  }

  private sortTable (): void {
    this.accountingService.search({
      sort: this.sort,
      key: this.search,
      page: this.page,
      date: this.date
    }).takeUntil(this.destroy$).subscribe( response => {
      this.accountingList = response;
      this.inputIds = [];
      this.isChecked = false;
      this.isCheckedAll = false;
    });
  }
}
