import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { ProfileSettingsService } from '../profile-settings.service';

import { FromValidation } from '@fromValidation';

import { ProfileSettingsGet } from '../profile-settings';

import { maskPhone } from '@shared/const/masks';
import { ToastsManager } from 'ng2-toastr';

@Component ({
    selector: 'app-profile-settings-edit',
    templateUrl: './profile-settings-edit.component.html',
    styleUrls: ['./profile-settings-edit.component.css']
})

export class ProfileSettingsEditComponent extends FromValidation implements OnInit {

    destroy$: Subject<boolean> = new Subject<boolean>();

    title = 'Настройка профиля';
    form: FormGroup;
    user: ProfileSettingsGet;

    static onButtonClick = new Subject();

    public maskPhone =  maskPhone;

    constructor (
        public router: Router,
        public profileSettingsService: ProfileSettingsService,
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef,
    ) {
        super (
            router,
            profileSettingsService,
        );
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit () {
        this.user = this.activatedRoute.snapshot.data['user'];
        this.buildForm();
    }

    buildForm (): void {
        this.form = this.fb.group({
            firstname: [this.user.data.firstname,
                Validators.required
            ],
            lastname: [this.user.data.lastname,
                Validators.required
            ],
            phone: [this.user.data.phone,
                Validators.required
            ],
            email: [this.user.data.email,
                Validators.required
            ],
            password: [''],
        });
    }

    public onSubmit (form): void {
        if (this.form.valid) {
            this.profileSettingsService.updateUser(this.form.value).takeUntil(this.destroy$).subscribe( res => {
                if (res.success === 'ok') {
                  this.toastr.success('Has been edit successfully');
                  setTimeout(() => {
                    this.router.navigateByUrl('/cabinet/dashboard');
                  }, 1000);
                }  else {
                    this.toastr.error('Error');
                    this.validationFormBackEnd(res.validation, this.form);
                }
            });

        } else {
            this.toastr.error('Error');
            this.validationFormsSubmit(this.form);
        }
        ProfileSettingsEditComponent.onButtonClick.next();
    }

}
