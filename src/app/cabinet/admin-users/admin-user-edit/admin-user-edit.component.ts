import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { AdminUsersService } from '../admin-users.service';

import { FromValidation } from '@fromValidation';

import { AdminUserGet } from '../admin-user';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';

import { maskPhone } from '@shared/const/masks';
import {AutocompleteCity} from '@shared/interface/autocomplete-city';

@Component ({
    selector: 'app-admin-user-edit',
    templateUrl: './admin-user-edit.component.html',
    styleUrls: ['./admin-user-edit.component.css']
})

export class AdminUserEditComponent extends FromValidation implements OnInit, OnDestroy, AfterViewInit {

    destroy$: Subject<boolean> = new Subject<boolean>();

    title = 'Редактирование пользователя';
    form: FormGroup;
    user: AdminUserGet;
    userId: number;

    public maskPhone =  maskPhone;

    public titleCity_from = '';
    // public titleCountry_from = '';
    // public postalCode_from;
    // public titleRegion_from;
    public local_from = [];
    // public localMain_from = [];

    public autocompleteCityFormComponent: AutocompleteCity;

    constructor (
        public adminUsersService: AdminUsersService,
        public router: Router,
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder,
        public findToAutocompleteService: FindToAutocompleteService,
    ) {
        super(
            router,
            adminUsersService,
        );
    }

    ngOnInit () {
        this.user = this.activatedRoute.snapshot.data['user'];
        this.activatedRoute.params.takeUntil(this.destroy$).subscribe(params => {
            this.userId = +params['id'];
        });

        this.buildForm();

        this.buildAutocompleteCityFormComponent();
    }

    ngAfterViewInit () {

    }

    ngOnDestroy () {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }


    /*========= LOCATION =========*/

    // public isLiction;
    // onGetLocation(city) {
    //   this.isLiction = this.findToAutocompleteService.findLocation(city)
    //     .subscribe(res => {
    //       this.local_from = res.data;
    //     });
    //
    // }
    //
    // onGetLocationFrom(event) {
    //   this.local_from = [];
    //   const test2 = event.target.value;
    //   this.onGetLocation(test2);
    // }
    //
    // onSelectLocationFrom(event) {
    //   // this.titleCountry_from = event.countryName;
    //   // this.postalCode_from = event.postal_code;
    //   // this.titleRegion_from = event.region;
    //   if (!event) {
    //     return false;
    //   }
    //   this.form.get('country').setValue(event.countryName);
    //   this.form.get('zip_code').setValue(event.postal_code);
    //   this.form.get('region').setValue(event.region);
    // }
    /*========= / LOCATION =========*/


    public onSubmit (form): void {
        this.submitForm('updateUser', this.form, this.userId, this.destroy$);
        // if (this.form.valid) {
        //     this.adminUsersService.updateUser(this.form.value, this.userId).takeUntil(this.destroy$).subscribe( res => {
        //         if (res.success === 'ok') {
        //             this.router.navigateByUrl('/cabinet/admin-users');
        //         }  else {
        //             this.validationFormBackEnd(res.validation, this.form);
        //         }
        //     });
        //
        // } else {
        //     this.validationFormsSubmit(this.form);
        // }
    }

    private buildForm (): void {
        this.form = this.fb.group({
            lastname: [ this.user.data.lastname, [
                Validators.required,
            ]],
            username: [ this.user.data.username, [
                Validators.required,
            ]],
            firstname: [ this.user.data.firstname, [
                Validators.required,
            ]],
            email: [this.user.data.email, [
                Validators.required,
            ]],
            phone: [ this.user.data.phone, [
                Validators.required,
            ]],
            // country: [ this.user.data.country],
            // city: [ this.user.data.city],
            // region: [ this.user.data.region],
            // address: [ this.user.data.address],
            // zip_code: [ this.user.data.zip_code],
            // type: [this.user.data.type, [
            //     Validators.required,
            // ]],
        });
    }

  private buildAutocompleteCityFormComponent () {
    this.autocompleteCityFormComponent = {
      searchSelectAutocomplete: {
        formControlName: 'city',
        label: 'Город',
        value: this.user.data.city,
      },
      searchInfoAutocomplete: {
        country: {
          formControlName: 'country',
          label: 'Страна',
          value: this.user.data.country,
          searchEvent: 'countryName',
        },
        region: {
          formControlName: 'region',
          label: 'Район/Штат',
          value: this.user.data.region,
          searchEvent: 'region',
        },
        address: {
          formControlName: 'address',
          label: 'Адрес',
          value: this.user.data.address,
          searchEvent: null,
        },
        zip_code: {
          formControlName: 'zip_code',
          label: 'Индекс',
          value: this.user.data.zip_code,
          searchEvent: 'postal_code',
        }
      }
    }
  }
}
