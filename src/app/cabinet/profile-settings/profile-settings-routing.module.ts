import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileSettingsComponent } from './profile-settings.component';
import { ProfileSettingsEditComponent } from './profile-settings-edit/profile-settings-edit.component';

import { ProfileSettingsResolve } from './profile-settings.resolve';
import { ProfileSettingsEditResolve } from './profile-settings-edit/profile-settings-edit.resolve';


const ROUTER: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: ProfileSettingsComponent,
                resolve: {
                    user: ProfileSettingsResolve,
                }
            },
            {
                path: 'edit',
                component: ProfileSettingsEditComponent,
                resolve: {
                    user: ProfileSettingsEditResolve
                }
            }
        ]
    }
];

@NgModule ({
    imports: [RouterModule.forChild(ROUTER)],
    exports: [RouterModule],
})

export class ProfileSettingsRoutingModule {}
