import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { ReactiveFormsModule } from '@angular/forms';

import { MultiSelectComponent } from './multi-select.component';

@NgModule ({
  imports: [
    CommonModule,
    MultiselectDropdownModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    MultiSelectComponent
  ],
  declarations: [
    MultiSelectComponent
  ],
})

export class MultiSelectModule {

}
