import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { QueriesService } from './queries.service';

@Injectable()

export class QueriesResolve implements Resolve<any> {

    constructor (
        private queriesService: QueriesService,
    ) {}

    resolve () {
        return this.queriesService.getQueriesList();
    }

}
