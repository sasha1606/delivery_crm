import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { CouriersService } from './couriers.service';
import { CourierList } from './couriers';

@Component ({
    selector: 'app-courier',
    templateUrl: './couriers.component.html',
    styleUrls: ['./couriers.component.css']
})

export class CourierComponent implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    title = 'Курьеры';
    couriers: CourierList;

    page = 1;
    search = '';
    sort = '';

    constructor (
      private couriersService: CouriersService,
      private activatedRoute: ActivatedRoute,
    ) {}

    ngOnInit () {
      this.couriers = this.activatedRoute.snapshot.data['couriers'];
    }

    ngOnDestroy () {
      this.destroy$.next(true);
      this.destroy$.unsubscribe();
    }

    public deleteCourier (id: number): void {
      this.couriersService.deleteCourier(id).takeUntil(this.destroy$).subscribe(res => {
        if (res.success === 'ok') {
          this.sortTable();
        }
      });
    }

    public tablePage (page) {
      this.page = page;
      this.sortTable();
    }

    public tableSearch (word: string): void {
      this.search = word;
      this.sortTable();
    }

    public tableSort (param: string): void {
      if (this.sort.length > 0 && param === this.sort) {
        param = `-${param}`;
      }
      this.sort = param;
      this.sortTable();
    }

    private sortTable () {
      this.couriersService.search({page: this.page, key: this.search, sort: this.sort }).takeUntil(this.destroy$).subscribe(res => {
        this.couriers = res;
      });
    }

}
