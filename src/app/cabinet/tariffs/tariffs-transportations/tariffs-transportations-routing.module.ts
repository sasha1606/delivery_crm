import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TariffsTransportationsComponent } from './tariffs-transportations.component';

import { TariffsTransportationsResolve } from './tariffs-transportations.resolve';

const ROUTERS: Routes = [
    {
        path: '',
        component: TariffsTransportationsComponent,
        resolve: {
          tariffs: TariffsTransportationsResolve
        }
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class TariffsTransportationsRoutingModule {}
