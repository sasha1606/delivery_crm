import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnalyticsComponent } from './analytics.component';

import { AnalyticsRoutingModule } from './analytics-routing.module';

import { AnalyticsService } from './analytics.service';
import { AnalyticsResolve } from './analytics.resolve';

import { PaginationModule } from '@shared/pagination/pagination.module';
import { SearchTableModule } from '@shared/search-table/search-table.module';

@NgModule ({
    imports: [
        CommonModule,
        AnalyticsRoutingModule,
        PaginationModule,
        SearchTableModule,
    ],
    declarations: [
        AnalyticsComponent,
    ],
    providers: [
        AnalyticsService,
        AnalyticsResolve,
    ]
})

export class AnalyticsModule {

}
