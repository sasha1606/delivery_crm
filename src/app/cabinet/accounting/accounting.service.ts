import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiService } from '@api/api.service';

import { ApiSrc } from '@api/api.src';

import { AccountingList } from './accounting';

@Injectable()

export class AccountingService {

  constructor (
    private apiService: ApiService
  ) {}

  public getAccountingList (): Observable<AccountingList> {
      return this.apiService.get(ApiSrc.accounting);
  }

  public search ({sort = '', key = '', page = 1, date = ''}): Observable<AccountingList> {
    // for IE
    const  keyword = encodeURIComponent(key);
    return this.apiService.get(`${ApiSrc.accounting}?sort=${sort}&keyword=${keyword}&page=${page}&date=${date}`);
  }

}
