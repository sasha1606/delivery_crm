import { Pagination } from '@shared/interface/pagination';

export interface TaskmanGet {
  listdata: Taskman[];
  success: string;
  listmanagers: object;
  pagination: Pagination;
}

export interface Taskman {
  description: string;
  end_date: number;
  id: number;
  managerFullname: string;
  priorityName: string;
  title: string;
  user_id: number;
  statusName: string;
  status: string;
  statusNames: object;
}
export interface TaskmanModalGet {
  success: string;
  data: TaskmanModal;

}
export interface TaskmanModal {
  created_at: string;
  description: number;
  end_date: number;
  id: string;
  listmanagers: object;
  priority: string;
  priorityNames: object;
  status: number;
  title: string;
  type: string;
  updated_at: object;
  user_id: object;
}
