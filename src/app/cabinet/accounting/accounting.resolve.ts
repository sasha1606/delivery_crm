import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { AccountingService } from './accounting.service';

@Injectable()

export class AccountingResolve implements Resolve<any> {

  constructor (
    private accountingService: AccountingService,
  ) {}

  resolve () {
    return this.accountingService.getAccountingList();
  }

}
