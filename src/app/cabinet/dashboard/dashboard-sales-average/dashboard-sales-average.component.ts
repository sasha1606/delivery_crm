import { Component, OnInit, Input } from '@angular/core';

@Component ({
    selector: 'app-dashboard-sales-average',
    templateUrl: './dashboard-sales-average.component.html',
    styleUrls: ['./dashboard-sales-average.component.css'],
})

export class DashboardSalesAverageComponent implements OnInit {

  @Input() monthRequests;

    // public lineChartData: Array<any> = [
    //     {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    //     {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
    // ];
    // public lineChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
    public lineChartData: Array<any>;
    public lineChartLabels: Array<any>;
    public lineChartOptions: any = {
        responsive: true
    };
    public lineChartColors: Array<any> = [
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        }
    ];
    public lineChartLegend = false;
    public lineChartType = 'line';

    ngOnInit () {
      this.lineChartData = this.monthRequests.data;
      this.lineChartLabels = this.monthRequests.line;
    }
}
