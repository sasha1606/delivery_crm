import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TariffsTransportationsRoutingModule } from './tariffs-transportations-routing.module';

import { TariffsTransportationsComponent } from './tariffs-transportations.component';

import { TariffsTransportationsResolve } from './tariffs-transportations.resolve';
import { TariffsTransportationsService } from './tariffs-transportations.service';

import { TariffsTransportersModule } from '../shared-tariffs/tariffs-transporters/tariffs-transporters.module';
import { TariffsZonesModule } from '../shared-tariffs/tariffs-zones/tariffs-zones.module';

@NgModule ({
    imports: [
        CommonModule,
        TariffsTransportationsRoutingModule,
        TariffsTransportersModule,
        TariffsZonesModule,
    ],
    declarations: [
        TariffsTransportationsComponent,
    ],
    providers: [
      TariffsTransportationsResolve,
        TariffsTransportationsService,
    ],
})

export class TariffsTransportationsModule {

}
