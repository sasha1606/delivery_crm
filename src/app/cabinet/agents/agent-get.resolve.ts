import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

import { AgentsService } from './agents.service';

@Injectable()

export class AgentGetResolve implements Resolve<any> {

  constructor (
    private agentsService: AgentsService,
    private router: Router
  ) {}

  resolve ( activatedRouteSnapshot: ActivatedRouteSnapshot ) {
    return this.agentsService.getAgent(Number(activatedRouteSnapshot.paramMap.get('id'))).catch(err => {
      this.router.navigateByUrl('/cabinet/agents');
      return Observable.of({error: err});
    });
  }

}
