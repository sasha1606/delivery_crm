export class LeftMenuInterface {
    link: string;
    name: string;
    icon: string;
    count?: number;
    [others: string]: any;
}
