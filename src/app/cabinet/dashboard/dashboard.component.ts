import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component ({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
    title = 'Dashboard';

    public dashboardInfo;
    public ordersInfo: string[];

    constructor (
      private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit () {
      this.dashboardInfo = this.activatedRoute.snapshot.data['dashboardInfo'];
      this.ordersInfo = [this.dashboardInfo.courier_go_orders,
                      this.dashboardInfo.recived_orders,
                      this.dashboardInfo.instock_orders];
    }

}
