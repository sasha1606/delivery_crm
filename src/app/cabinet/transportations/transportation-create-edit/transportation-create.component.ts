import { Component, OnInit, OnDestroy, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { TransportationsService } from '../transportations.service';

import { Transportations, TransportationsGet, TransportationsModalGet, typePage } from '../transportations';

import { FromValidation } from '@fromValidation';

import { maskPhone } from '@shared/const/masks';
import { ToastsManager } from 'ng2-toastr';

@Component ({
    selector: 'app-transportation-create',
    templateUrl: './transportation-create.component.html',
    styleUrls: ['./transportation-create.component.css'],
})

export class TransportationCreateComponent extends FromValidation implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    title: string;
    form: FormGroup;
    fromModal: FormGroup;
    modalRef: NgbModalRef;
    typePage: string;
    transportationId: number;

    dynamicFormElem: object[] = [];

    transportation: TransportationsGet;
    transportationModal: TransportationsModalGet;

    public maskPhone =  maskPhone;

    constructor (
        private modalService: NgbModal,
        private activatedRoute: ActivatedRoute,
        public fb: FormBuilder,
        public transportationsService: TransportationsService,
        public router: Router,
        private vcr: ViewContainerRef,
        private toastr: ToastsManager,
    ) {
      super(
        router,
        transportationsService,
      );
      this.toastr.setRootViewContainerRef(vcr);
    }


    ngOnInit () {
        this.transportation = this.activatedRoute.snapshot.data['transportation'];
        this.typePage = this.activatedRoute.snapshot.data['data'];

        this.activatedRoute.params.subscribe(param => {
          this.transportationId = +param['id'];
        });

        this.setTitle(this.typePage);

        this.buildForm();

        if (this.transportation.data.directionsParams) {
            for (const key of Object.keys(this.transportation.data.directionsParams)) {
              this.dynamicFormElem.push(this.transportation.data.directionsParams[key]);
            }
        }
    }

    ngOnDestroy () {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

    public getInfoModal (content) {
        this.transportationsService.dataTransportationModal().takeUntil(this.destroy$).subscribe( res => {
          if (res.success === 'ok') {
            this.transportationModal = res;
            this.buildModalForm();
            this.listeningResponsiblePerson();
            this.openModal(content);
          }
        });
    }

    public onSubmit (form): void {
      if (this.form.valid) {
        const objRequest: Transportations | object = this.reformateRequestObj(this.form.value);

        let answer;
        if (this.typePage === typePage.edit) {
          // sendLink = this.transportationsService.editTransportation.bind(this.transportationsService);
          answer = this.transportationsService.editTransportation({
                                                                    transportation: objRequest,
                                                                    id: this.transportationId
                                                                  })
                                                                  .takeUntil(this.destroy$);
        } else if (this.typePage === typePage.create) {
          answer =  this.transportationsService.createTransportation( objRequest).takeUntil(this.destroy$);
        }

        answer.subscribe( res => {
          if (res.success === 'ok') {
            this.toastr.success('Has been created successfully');
            setTimeout(() => {
              this.router.navigateByUrl('/cabinet/transportations');
            }, 1000);
          }  else {
            this.toastr.error('Error');
            if (res.validation['model']) {
              this.validationFormBackEnd(res.validation['model'], this.form);
            }

            if (res.validation['dynamic_models']) {
              for (const key of Object.keys(res.validation['dynamic_models'])) {
                this.validationFormBackEnd(
                  {[key]: res.validation['dynamic_models'][key]},
                  this.form
                );
              }
            }
          }
        });

      } else {
        this.toastr.error('Error');
        this.validationFormsSubmit(this.form);
      }
    }

    public onSubmitModal (form): void {
      if (this.fromModal.valid) {
        this.transportationsService.createTransportationModal(this.fromModal.value).takeUntil(this.destroy$).subscribe( res => {
          if (res.success === 'ok') {
            this.modalRef.close();
            this.dynamicFormElem.push(res.data);
          }  else {
            this.validationFormBackEnd(res.validation, this.fromModal);
          }
        });
      } else {
        this.validationFormsSubmit(this.fromModal);
      }
    }

    public deleteDynamicElem(item: string): void {
      this.form.removeControl(item);
      let index: any =  item.indexOf('_');
      index = item.slice(index + 1);
      delete this.dynamicFormElem[index - 1];
    }

    private buildForm () {
        this.form = this.fb.group({
            name: [this.transportation.data.name, [
                Validators.required
            ]],
            email: [this.transportation.data.email, [
              Validators.required
            ]],
            phone: [this.transportation.data.phone, [
              Validators.required
            ]],
            // typeParams: ['', [
            //     Validators.required
            // ]],
            responsible_person: this.transportation.data.responsible_person,
        });
    }

    private buildModalForm () {
      this.fromModal = this.fb.group({
          name: ['', [
            Validators.required
          ]],
          email: ['', [
            Validators.required
          ]],
          phone: ['', [
            Validators.required
          ]],
        // typeParams: ['', [
        //     Validators.required
        // ]],
          responsible_person: '',
          comment: ''
      });
    }

    private openModal (content) {
      this.modalRef = this.modalService.open(content, {size: 'lg'});
    }

    private reformateRequestObj (obj) {
      const clone = {};
      for (const key  in obj) {
          if (Object.prototype.toString.call(obj[key]) === '[object Object]') {
            if (!clone['directionsParams']) {
              clone['directionsParams'] = {};
            }
            clone['directionsParams'][key] = obj[key];
          } else {
            clone[key] = obj[key];
          }
      }
      return clone;
    }

    private setTitle (type) {
      this.title = 'Добавить перевозчика';
      if (type === typePage.edit) {
        this.title = ' Редактировать перевозчика';
      }
    }

    private listeningResponsiblePerson () {
        this.fromModal.get('responsible_person').valueChanges.subscribe(val => {
            for ( const key of Object.keys(this.form.value) ) {
                if ( Object.prototype.toString.call(this.form.value[key]) === '[object Object]'  &&
                     this.form.value[key].responsible_person  === val ) {
                      this.fromModal.get('email').setValue(this.form.value[key].email);
                      this.fromModal.get('phone').setValue(this.form.value[key].phone);
                }
            }
        });
    }
}
