import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { AnalyticsList } from './analytics';

import { AnalyticsService } from './analytics.service';

import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Component ({
    selector: 'app-analytics',
    templateUrl: './analytics.component.html',
    styleUrls: ['./analytics.component.css']
})

export class AnalyticsComponent implements OnInit, OnDestroy {

    public title = 'Аналитика';
    public analytics: AnalyticsList;

    destroy$: Subject<boolean> = new Subject<boolean>();

    private page = 1;
    private search = '';
    private sort = '';
    private date = '';

    constructor (
      private activatedRoute: ActivatedRoute,
      private analyticsService: AnalyticsService
    ) {}

    ngOnInit () {
      this.analytics = this.activatedRoute.snapshot.data['analytics'];
    }

    ngOnDestroy () {
      this.destroy$.next(true);
      this.destroy$.unsubscribe();
    }

    public tablePage (page) {
      this.page = page;
      this.sortTable();
    }

    public tableSort (param: string): void {
      if (this.sort.length > 0 && param === this.sort) {
        param = `-${param}`;
      }
      this.sort = param;
      this.sortTable();
    }

    public getSearchData (data)  {
      this.date = data.data;
      this.search = data.word;
      this.sortTable();
    }

    private sortTable () {
        this.analyticsService.search({
        sort: this.sort,
        key: this.search,
        page: this.page,
        date: this.date
      }).takeUntil(this.destroy$).subscribe( orders => {
        this.analytics = orders;
      });
    }

}
