import { Pagination } from '@shared/interface/pagination';

export interface TariffsCustoms {
  success: string;
  pagination: Pagination;
  listdata: Tariffs[];
}

interface Tariffs {
  category: string;
  filesFormated: string;
  id: string;
  name: string;
  price: string;
}
