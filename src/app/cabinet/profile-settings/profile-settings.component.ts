import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { ProfileSettingsGet } from './profile-settings';

import { ProfileSettingsResolve } from './profile-settings.resolve';

@Component ({
    selector: 'app-profile-settings',
    templateUrl: './profile-settings.component.html',
    styleUrls: ['./profile-settings.component.css']
})

export class ProfileSettingsComponent implements OnInit {

    title = 'Настройка профиля';
    form: FormGroup;
    user: ProfileSettingsGet;

    constructor (
        private fb: FormBuilder,
        private profileSettingsResolve: ProfileSettingsResolve,
        private activatedRoute: ActivatedRoute,
    ) {}

    ngOnInit () {
        this.user = this.activatedRoute.snapshot.data['user'];

        this.buildForm();
        this.form.disable();
    }

    buildForm () {
        this.form = this.fb.group({
            firstname: [this.user.data.firstname],
            lastname: [this.user.data.lastname],
            phone: [this.user.data.phone],
            email: [this.user.data.email],
        });
    }
}
