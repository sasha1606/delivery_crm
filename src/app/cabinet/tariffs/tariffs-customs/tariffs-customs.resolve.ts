import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { TariffsCustomsService } from './tariffs-customs.service';

@Injectable()

export class TariffsCustomsResolve implements Resolve<any> {

  constructor (
    private tariffsCustomsService: TariffsCustomsService,
  ) {}

  resolve () {
    return this.tariffsCustomsService.getTariffsCustoms();
  }
}
