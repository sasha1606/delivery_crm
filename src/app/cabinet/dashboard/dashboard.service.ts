import { Injectable } from '@angular/core';

import { ApiService } from '@api/api.service';

import { ApiSrc } from '@api/api.src';

import { Observable } from 'rxjs/Observable';

@Injectable()

export class DashboardService {

  constructor (
    private apiService: ApiService,
  ) {}

  getDashbordInfo (): Observable<any> {
    return this.apiService.get(ApiSrc.dashboard);
  }

}
