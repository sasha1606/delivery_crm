import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnalyticsComponent } from './analytics.component';

import { AnalyticsResolve } from './analytics.resolve';

const ROUTERS: Routes = [
    {
        path: '',
        component: AnalyticsComponent,
        resolve: {
          analytics: AnalyticsResolve,
        }
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class AnalyticsRoutingModule {}
