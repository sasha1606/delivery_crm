import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';

import { CouriersRoutingModule } from './couriers-routing.module';
import { PaginationModule } from '@shared/pagination/pagination.module';
import { FormFileUploadModule } from '@shared/form-file-upload/form-file-upload.module';

import { CourierComponent } from './couriers.component';
import { CourierCrudComponent } from './courier-crud/courier-crud.component';
// import { CourierShowComponent } from './courier-show_delete/courier-show.component';
// import { CourierEditComponent } from './courier-edit_delete/courier-edit.component';

import { CouriersService } from './couriers.service';

import { CouriersResolve } from './couriers.resolve';
import { CourierCreateResolve } from './courier-crud/courier-create.resolve';
import { CourierEditResolve } from './courier-crud/courier-edit.resolve';
import { CourierShowResolve } from './courier-crud/courier-show.resolve';
import { NgSelectModule } from '@ng-select/ng-select';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';
import { AutocompleteCityModule } from '@shared/components/autocomplete-city/autocomplete-city.module';

@NgModule ({
    imports: [
        CommonModule,
        CouriersRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        PaginationModule,
        FormFileUploadModule,
        NgSelectModule,
        TextMaskModule,
        AutocompleteCityModule,
    ],
    declarations: [
        CourierComponent,
        CourierCrudComponent,
        // CourierShowComponent,
        // CourierEditComponent
    ],
    providers: [
        CouriersService,
        CouriersResolve,
        CourierCreateResolve,
        CourierEditResolve,
        CourierShowResolve,
      FindToAutocompleteService
    ],
})

export class CouriersModule {}
