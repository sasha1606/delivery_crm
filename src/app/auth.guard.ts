import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';

import { TokenStorageService } from './token-storage.service';

@Injectable()

export class AuthGuard implements CanActivateChild, CanActivateChild {

    constructor (
        private tokenStorageService: TokenStorageService,
        private router: Router
    ) {}

    canActivate() {
        return true;
    }

    canActivateChild() {
        if (this.tokenStorageService.token) {
            return true;
        } else {
            this.router.navigateByUrl('/login');
            return false;
        }
    }
}
