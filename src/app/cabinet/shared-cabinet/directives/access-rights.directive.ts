import {
  Directive, ElementRef,
  Input,
  OnInit,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import { UserInfoService } from '../../user.info.service';

@Directive({
  selector: '[appAccessRights]'
})

export class AccessRightsDirective implements OnInit {

  @Input() permission?: string[];

  private userRole;

  constructor(
    private elementRef: ElementRef,
    private userInfoService: UserInfoService,
    private viewContainer: ViewContainerRef
  ) {}

  ngOnInit () {
    this.getUserRole();
  }

  private getUserRole () {
    this.userInfoService.getUserInfo().subscribe( response => {
      this.userRole = response.data.type;
      this.checkPermission(this.userRole);
    });
  }

  private checkPermission (userRole) {
    if (this.permission && this.permission.length > 0) {
      const hideElem = this.permission.some( (item) => {
        return item === userRole;
      });

      if (!hideElem) {
        this.elementRef.nativeElement.remove();
      }
    }
  }
}
