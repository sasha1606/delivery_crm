import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AutocompleteCityComponent } from './autocomplete-city.component';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { PipeModule } from '@shared/pipes/pipe.module';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgSelectModule,
    PipeModule,
  ],
  declarations: [
    AutocompleteCityComponent,
  ],
  exports: [
    AutocompleteCityComponent,
  ],
  providers: [
    FindToAutocompleteService,
  ],
})

export class AutocompleteCityModule {

}
