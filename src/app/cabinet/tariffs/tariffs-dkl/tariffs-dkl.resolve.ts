import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { TariffsDklService } from './tariffs-dkl.service';

@Injectable()

export class TariffsDklResolve implements Resolve<any> {

  constructor (
    private tariffsDklService: TariffsDklService
  ) {}

  resolve () {
    return this.tariffsDklService.getTariffsDkl();
  }

}
