import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component ({
    selector: 'app-tariffs-transportations',
    templateUrl: './tariffs-transportations.component.html',
    styleUrls: ['./tariffs-transportations.component.css']
})

export class TariffsTransportationsComponent implements OnInit {

    title = 'Тарифы перевозчиков';
    tariffs: any;

    constructor (
      private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit () {
      this.tariffs = this.activatedRoute.snapshot.data['tariffs'];
    }
}
