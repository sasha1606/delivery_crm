import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { QueriesPriceResolve } from './queries-price.resolve';
import { QueryPriceShowResolve } from './query-price-show/query-price-show.resolve';
import { QueryPriceCreateResolve } from './query-price-edit-create/query-price-create.resolve';
import { QueryPriceEditResolve } from './query-price-edit-create/query-price-edit.resolve';
import { QueriesPriceService } from './queries-price.service';

import { QueriesPriceRoutingModule } from './queries-price-routing.module';

import { QueriesPriceComponent } from './queries-price.component';

import { AutocompleteService } from '@shared/services/autocomplete.service';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';
// import { QueryPriceCreateComponent } from './query-price-create/query-price-create.component';
import { FormFileUploadModule } from '@shared/form-file-upload/form-file-upload.module';

import { QueryPriceShowComponent } from './query-price-show/query-price-show.component';
import { QueryPriceEditCreateComponent } from './query-price-edit-create/query-price-edit-create.component';

import { PipeModule } from '@shared/pipes/pipe.module';
import { PaginationModule } from '@shared/pagination/pagination.module';
import { SearchTableModule } from '@shared/search-table/search-table.module';
import { FormAutocompleteModule } from '@shared/form-autocomplete/form-autocomplete.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { AutocompleteCityModule } from '@shared/components/autocomplete-city/autocomplete-city.module';
import { ParcelFormParamsModule } from '@shared/components/parcel-form-params/parcel-form-params.module';
import { InsuranceFormElemModule } from '@shared/components/insurance-form-elem/insurance-form-elem.module';

@NgModule ({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        QueriesPriceRoutingModule,
        PipeModule,
        PaginationModule,
        SearchTableModule,
        FormAutocompleteModule,
        NgSelectModule,
        FormFileUploadModule,
        AutocompleteCityModule,
        ParcelFormParamsModule,
        InsuranceFormElemModule,
    ],
    declarations: [
        QueriesPriceComponent,
        // QueryPriceCreateComponent,
        QueryPriceShowComponent,
        QueryPriceEditCreateComponent,
    ],
    providers: [
        QueriesPriceResolve,
        QueryPriceCreateResolve,
        QueryPriceEditResolve,
        QueryPriceShowResolve,
        QueriesPriceService,
        AutocompleteService,
        FindToAutocompleteService,
    ]
})

export class QueriesPriceModule {
}
