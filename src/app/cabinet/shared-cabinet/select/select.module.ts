import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import {  SelectComponent } from './select.component';

import { PipeModule } from '../pipes/pipe.module';

@NgModule ({
    imports: [
        CommonModule,
        PipeModule,
        ReactiveFormsModule,
    ],
    declarations: [
        SelectComponent,
    ],
    exports: [
        SelectComponent,
    ],
})

export class SelectModule {

}
