export interface AutocompleteCity {
  searchSelectAutocomplete: Item;
  searchInfoAutocomplete: {
    [key: string]: Item;
  };
}

interface Item {
  formControlName: string;
  label: string;
  value: string;
  searchEvent?: string;
}
