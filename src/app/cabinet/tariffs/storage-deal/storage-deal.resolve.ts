import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { StorageDealService } from './storage-deal.service';

@Injectable()

export class StorageDealResolve implements Resolve<any> {

  constructor (
    private storageDealService: StorageDealService
  ) {}

  resolve () {
    return this.storageDealService.getStorageDeal();
  }

}
