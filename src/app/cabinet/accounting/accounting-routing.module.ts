import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountingComponent } from './accounting.component';

import { AccountingResolve } from './accounting.resolve';

const ROUTERS: Routes = [
    {
        path: '',
        component: AccountingComponent,
        resolve: {
          accounting: AccountingResolve,
        }
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class AdminUsersRoutingModule {}
