import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormBuilder, FormGroup} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { QueriesPriceService } from './queries-price.service';

import { QueriesPriceGet, QueriesPriceListGet } from './queries-price';

@Component ({
    selector: 'app-queries-price',
    templateUrl: './queries-price.component.html',
    styleUrls: ['./queries-price.component.css']
})

export class QueriesPriceComponent implements OnInit, OnDestroy {

  title = 'Запросы на цену';

  public managerList = {};
  public managerId: number;

  destroy$: Subject<boolean> = new Subject<boolean>();

  queriesPrice: QueriesPriceListGet;

  sort = '';
  date = '';
  page = 1;
  search = '';

  constructor (
    public queriesPriceService: QueriesPriceService,
    public router: Router,
    private activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit () {
    this.queriesPrice = this.activatedRoute.snapshot.data['queriesPrice'];
    this.managerList = this.queriesPrice.listmanagers;
  }

  ngOnDestroy () {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  // public getDate (date): void {
  //   this.date = date;
  // }

  public tablePage (page: number): void {
    this.page = page;
    this.sendSort();
  }

  public tableSort (param: string): void {
    if (this.sort.length > 0 && param === this.sort) {
      param = `-${param}`;
    }
    this.sort = param;
    this.sendSort();
  }

  // public tableSearch (word: string): void {
  //   this.search = word;
  //   this.sendSort();
  // }

  public getSearchData (data)  {
    this.date = data.data;
    this.search = data.word;
    this.managerId = data.anotherFilter;
    this.sendSort();
  }


  private sendSort (): void {
    this.queriesPriceService.search({
      sort: this.sort,
      key: this.search,
      page: this.page,
      date: this.date,
      manager: this.managerId
    }).takeUntil(this.destroy$).subscribe( queries => {
      this.queriesPrice = queries;
    });
  }
}
