import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const ROUTERS: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: '/login'
            }
        ]
    },
    {
      path: 'api/crm/bookkeeping/getreport',
      redirectTo: '/api/crm/bookkeeping/getreport',
    },
    {
        path: '**',
        pathMatch: 'full',
        redirectTo: '/login',
    }
];

@NgModule ({
    imports: [RouterModule.forRoot(ROUTERS, {onSameUrlNavigation: 'reload'})],
    exports: [RouterModule],
})

export class AppRoutingModele {}
