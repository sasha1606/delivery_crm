import { Pagination } from '@shared/interface/pagination';


export interface QueriesPriceListGet {
  listdata: ShortQueriesPrice[];
  success: string;
  pagination: Pagination;
  listmanagers: object;
}

export interface ShortQueriesPrice {
  client_comment: string;
  countryCityFromt: string;
  countryCityTot: string;
  created_att: string;
  delivery_pricet: string;
  code: string;
  idt: number;
  insuranceNamet: string;
  items_countt: number;
  items_totalweight: number;
  itemstypeName: number;
  namet: string;
  filesFormated: string;
  sizeFormatedt: string;
  statusNamet: string;
  transporter_direction_namet: string;
  transporter_namet: string;
  typeNamet: string;
}

export interface QueriesPriceGet {
  data: QueriesPrice;
  success: string;
}

export interface QueriesPrice {
  address_from: string;
  address_to: string;
  callback_dkllink: string;
  city_from: string;
  city_to: string;
  transporter_type: string;
  transportersTypeNames: object;
  transporter_id: string;
  transportersList: object;
  transporter_direction_id: string;
  directionsLis: object;
  transporter_direction_name: string;
  transporter_direction_phone: string;
  transporter_direction_email: string;
  transporter_direction_resperson: string;
  client_comment: string;
  client_id: string;
  company: string;
  country_from: string;
  country_to: string;
  delivery_price: number;
  email: string;
  insurance: number;
  insurance_price: number;
  insurance_currency: number;
  itemsParams: {
    client_comment_1: string;
    items_type_1: string;
    delivery_type_1: string;
    items_count_1: string;
    items_weight_1: string;
    items_length_1: string;
    items_width_1: string;
    items_height_1: string;
    items_price_1: string;
    items_price_currency_1: string;
    client_comment_2: string;
    items_type_2: string;
    delivery_type_2: string;
    items_count_2: string;
    items_weight_2: string;
    items_length_2: string;
    items_width_2: string;
    items_height_2: string;
    items_price_2: string;
    items_price_currency_2: string;
    client_comment_3: string;
    items_type_3: string;
    delivery_type_3: string;
    items_count_3: string;
    items_weight_3: string;
    items_length_3: string;
    items_width_3: string;
    items_height_3: string;
    items_price_3: string;
    items_price_currency_3: string;
    client_comment_4: string;
    items_type_4: string;
    delivery_type_4: string;
    items_count_4: string;
    items_weight_4: string;
    items_length_4: string;
    items_width_4: string;
    items_height_4: string;
    items_price_4: string;
    items_price_currency_4: string;
  };
  items_count: number;
  items_price: number;
  items_totalweight: number;
  items_type: string;
  itemstypeNames: object;
  name: string;
  phone: string;
  region_from: string;
  region_to: string;
  status: number;
  type: string;
  typeNames: object;
  statusNames: object;
  user_comment: string;
  zip_code_from: string;
  zip_code_to: string | null;
  filesUpload: string;
  deleteFiles: string;
  estimated_weight: string;
}

export const typePage = {
  edit: 'edit',
  create: 'create'
};

