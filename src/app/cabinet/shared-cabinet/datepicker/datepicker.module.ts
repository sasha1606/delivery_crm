import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { NgDatepickerModule } from 'ng2-datepicker';

import { DatepickerComponent } from './datepicker.component';

import { OwlDateTimeModule, OwlNativeDateTimeModule, OwlDateTimeIntl, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';

export const DefaultIntl = {
    rangeFromLabel: 'С',
    rangeToLabel: 'По',
};

@NgModule ({
    imports: [
        CommonModule,
        FormsModule,
        NgDatepickerModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        ReactiveFormsModule,
    ],
    declarations: [
        DatepickerComponent,
    ],
    exports: [
        DatepickerComponent,
    ],
    providers: [
        {provide: OWL_DATE_TIME_LOCALE, useValue: 'ru'},
        {provide: OwlDateTimeIntl, useValue: DefaultIntl},
    ],
})

export class DatepickerModule { }
