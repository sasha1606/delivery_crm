import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateBillModalComponent } from '@shared/components/create-bill-modal/create-bill-modal.component';
import { DatepickerModule } from '@shared/datepicker/datepicker.module';
import { CreateBillService } from '@shared/services/createBill.service';

@NgModule ({
  imports: [
    CommonModule,
    DatepickerModule
  ],
  declarations: [
    CreateBillModalComponent,
  ],
  exports: [
    CreateBillModalComponent,
  ],
  providers: [
    CreateBillService,
  ]
})

export class CreateBillModalModule {

}
