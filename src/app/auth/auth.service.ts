import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';

import { ApiSrc } from '../api/api.src';

import { LoginUser } from './login-user';

@Injectable()

export class AuthService {

    constructor (
        private apiService: ApiService
    ) {}

    public login (data: LoginUser): Observable<any> {
       return this.apiService.post(ApiSrc.login, data);
    }

    public resetPassword(form): Observable<any> {
      return this.apiService.post(ApiSrc.resetPassword, form);
    }

}
