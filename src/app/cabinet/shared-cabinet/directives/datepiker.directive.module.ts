import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DatepikerDirective } from './datepiker.directive';

@NgModule ({
  imports: [
    CommonModule
  ],
  declarations: [
    DatepikerDirective
  ],
  exports: [
    DatepikerDirective
  ]
})

export class DatepikerDirectiveModule {

}
