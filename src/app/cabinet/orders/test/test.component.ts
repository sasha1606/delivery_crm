import { Component, forwardRef, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css'],
  providers: [{
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TestComponent),
      multi: true
  }],
})

export class TestComponent implements OnInit, ControlValueAccessor {

  value: 1;
  constructor() { }

  ngOnInit() {
  }

  public test () {
  }

  writeValue(val: any): void {
    // this.value = val;

  }

  registerOnChange(fn: any): void {
    this.change = fn;
  }

  registerOnTouched(fn: any): void {
      this.touched = fn;
  }


  change (er) {
    return 55555555;
  }

  touched (re) {
    // console.log(4);
  }

  change43 (event) {
    this.change({input: true, value: event.target.value});
    // this.touched(55555555);
  }




}
