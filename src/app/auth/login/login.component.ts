import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { AuthService } from '../auth.service';
import { TokenStorageService } from '../../token-storage.service';

import { FromValidation } from '../../abstract-class/from-validation';
import { UserInfoService } from '../../cabinet/user.info.service';

@Component ({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent extends FromValidation implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();
    loading = false;
    loginFrom: FormGroup;
    error: any  = null;

    constructor (
        private fb: FormBuilder,
        public authService: AuthService,
        private userInfoService: UserInfoService,
        private tokenStorageService: TokenStorageService,
        public router: Router,
    ) {
        super(
            router,
            authService,
        );
    }

    ngOnInit () {
        if (this.tokenStorageService.token) {
            this.router.navigateByUrl('/cabinet/dashboard');
        }

        this.formBuild();
        this.userInfoService.userInfo = null;
    }

    ngOnDestroy () {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

    public login (): void {
        if (this.loginFrom.valid) {

            this.loading = true;
            this.loginFrom.disable();
            this.error = null;

            if (this.tokenStorageService.token) {
                this.tokenStorageService.deleteToken();
            }

            this.authService.login(this.loginFrom.value).takeUntil(this.destroy$).subscribe(res => {
                if ( res.success === 'ok') {
                    this.router.navigateByUrl('/cabinet/dashboard');
                } else {
                    this.loading = false;
                    this.loginFrom.enable();

                    if (res.hasOwnProperty('validation')) {
                        this.error = res.validation['loginform-password'][0];
                    } else {
                        this.error = 'Неверный логин или пароль.';
                    }
                }
            });

        } else {
           this.validationFormsSubmit(this.loginFrom);
        }
    }

    private formBuild (): void  {
        this.loginFrom = this.fb.group({
            name: ['',
                [
                    Validators.required,
                    Validators.minLength(2),
                ]
            ],
            password: ['',
                [
                    Validators.required,
                    Validators.minLength(2),
                ]
            ],
        });
    }
}
