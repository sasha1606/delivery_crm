import {Component, OnInit, OnDestroy, OnChanges, ViewContainerRef} from '@angular/core';
import {Validators, FormBuilder, FormGroup, FormControl} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { QueriesService } from '../queries.service';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';

import { FromValidation } from '@fromValidation';

import { QueriesGet } from '../queries';
import { ToastsManager } from 'ng2-toastr';

import { maskPhone } from '@shared/const/masks';
import {ValidateOnlyNumber} from '@shared/formValidators/validateOnlyNumber';
import { AutocompleteCity } from '@shared/interface/autocomplete-city';

declare var jQuery: any;
const $ = jQuery;

@Component ({
    selector: 'app-query-create',
    templateUrl: '../query-crud.component.html',
    styleUrls: ['./query-create.component.css']
})

export class QueryCreateComponent extends FromValidation implements OnInit, OnDestroy {


    destroy$: Subject<boolean> = new Subject<boolean>();

    title = 'Добавить запрос';
    form: FormGroup;

    query: QueriesGet;

    public maskPhone =  maskPhone;
    public ordersinfoTable;
    public titleCity_from = '';
    // public titleCountry_from = '';
    // public postalCode_from;
    // public titleRegion_from;
    public local_from = [];
    public localMain_from = [];

    public titleCity_to = '';
    // public titleCountry_to = '';
    // public postalCode_to;
    // public titleRegion_to;
    public local_to = [];
    public localMain_to = [];

    public resultFrom;
    public resultTo;
    public autoComplite = [];
    public cityTitle = [];
    public test1;
    public userInfo;
    // public userCompany;
    // public userEmail;
    // public userPhone;

    public autocompleteCityFromFormComponent: AutocompleteCity;
    public autocompleteCityToFormComponent: AutocompleteCity;


    nds = 0;
    summ = 0;
    tariffs = null;

    public showFrom: boolean;
    public editFrom: boolean;

    constructor (
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private findToAutocompleteService: FindToAutocompleteService,
        public router: Router,
        public queriesService: QueriesService,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef,
    ) {
        super (
            router,
            queriesService,
        );
      this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit () {
        this.editFrom = true;
        this.query = this.activatedRoute.snapshot.data['query'];
        this.buildFrom();
        this.buildAutocompleteCityToFormComponent();
        this.buildAutocompleteCityFromFormComponent();

    }

    ngOnDestroy () {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

    public setNds (nds: number): void {
        this.nds = +nds;
        this.calculation();
    }

    public setSumm (inputVal: string): void {
        this.tariffs = inputVal.trim();
        this.calculation();
    }


  /*========= LOCATION =========*/

  //
  //
  // public isLiction;
  // onGetLocation(city) {
  // this.isLiction = this.findToAutocompleteService.findLocation(city)
  //  .subscribe(res => {
  //    this.local_from = res.data;
  //    this.local_to = res.data;
  //    if(res !== 0 || res !== '' || res !== 'null') {
  //
  //    }
  //  });
  //
  // }
  //
  // onGetLocationFrom(event) {
  //   this.local_from = [];
  //     const test2 = event.target.value;
  //   this.onGetLocation(test2);
  // }
  //
  // onSelectLocationFrom(event) {
  //     // this.titleCountry_from = event.countryName;
  //   // this.postalCode_from = event.postal_code;
  //   // this.titleRegion_from = event.region;
  //     if (!event) {
  //       return false;
  //     }
  //     this.form.get('country_from').setValue(event.countryName);
  //     this.form.get('zip_code_from').setValue(event.postal_code);
  //     this.form.get('region_from').setValue(event.region);
  //
  //   }
  //
  // onGetLocationTo(event) {
  //   this.local_to = [];
  //   const test2 = event.target.value || this.test1;
  //   this.onGetLocation(test2);
  // }
  //
  // onSelectLocationTo(event) {
  //   // this.titleCountry_to = event.countryName;
  //   // this.postalCode_to = event.postal_code;
  //   // this.titleRegion_to = event.region;
  //   if (!event) {
  //     return false;
  //   }
  //   this.form.get('country_to').setValue(event.countryName);
  //   this.form.get('zip_code_to').setValue(event.postal_code);
  //   this.form.get('region_to').setValue(event.region);
  // }

  /*========= / LOCATION =========*/


  /*------FindUser----------*/

  onGetUserInfo(event) {
    this.findToAutocompleteService.findUser(event.target.value)
      .subscribe(res => {
        this.userInfo = res['data'];
      });
  }

  onSelectUserInfo(event) {
    // this.userCompany = event.company;
    // this.userEmail = event.email;
    // this.userPhone = event.phone;
    if (!event) {
      return false;
    }
    this.form.get('company').setValue(event.company);
    this.form.get('email').setValue(event.email);
    this.form.get('phone').setValue(event.phone);
  }

  /*------ / FindUser----------*/

    public onSubmit (form): void {
        if (this.form.valid) {
            this.queriesService.createQueries(this.form.value).takeUntil(this.destroy$).subscribe( res => {
                if (res.success === 'ok') {
                  this.toastr.success('Has been edit successfully');
                  setTimeout(() => {
                    this.router.navigateByUrl('/cabinet/queries');
                  }, 1000);

                }  else {
                  this.toastr.error('Error');
                    this.validationFormBackEnd(res.validation, this.form);
                }
            });

        } else {
            this.toastr.error('Error');
            this.validationFormsSubmit(this.form);
        }
    }

    private buildFrom (): void {
        this.form = this.fb.group({
            status: [this.query.data.status],
            name: [''],
            company: [''],
            email: [''],
            phone: [''],
            country_from: [''],
            city_from: [''],
            address_from: [''],
            region_from: [''],
            zip_code_from: [''],
            type: [this.query.data.type],
            // insurance: [0],
            // insurance_price: '',
            items_price: [''],
            country_to: [''],
            city_to: [''],
            address_to: [''],
            region_to: [''],
            zip_code_to: [''],
            items_count: [''],
            // insurance_currency: [''],
            items_totalweight: [''],
            // itemsParams: this.fb.group({
            //   client_comment_1:  '',
            //   items_type_1: '',
            //   delivery_type_1: '',
            //   items_count_1: ['', ValidateOnlyNumber],
            //   items_weight_1: ['', ValidateOnlyNumber],
            //   items_length_1: ['', ValidateOnlyNumber],
            //   items_width_1: ['', ValidateOnlyNumber],
            //   items_height_1: ['', ValidateOnlyNumber],
            //   items_price_1: ['', ValidateOnlyNumber],
            //   items_price_currency_1: '',
            //
            //   client_comment_2: '',
            //   items_type_2: '',
            //   delivery_type_2: '',
            //   items_count_2: ['', ValidateOnlyNumber],
            //   items_weight_2: ['', ValidateOnlyNumber],
            //   items_length_2: ['', ValidateOnlyNumber],
            //   items_width_2: ['', ValidateOnlyNumber],
            //   items_height_2: ['', ValidateOnlyNumber],
            //   items_price_2: ['', ValidateOnlyNumber],
            //   items_price_currency_2: '',
            //
            //   client_comment_3: '',
            //   items_type_3: '',
            //   delivery_type_3: '',
            //   items_count_3: ['', ValidateOnlyNumber],
            //   items_weight_3: ['', ValidateOnlyNumber],
            //   items_length_3: ['', ValidateOnlyNumber],
            //   items_width_3: ['', ValidateOnlyNumber],
            //   items_height_3: ['', ValidateOnlyNumber],
            //   items_price_3: ['', ValidateOnlyNumber],
            //   items_price_currency_3: '',
            //
            //   client_comment_4: '',
            //   items_type_4: '',
            //   delivery_type_4: '',
            //   items_count_4: ['', ValidateOnlyNumber],
            //   items_weight_4: ['', ValidateOnlyNumber],
            //   items_length_4: ['', ValidateOnlyNumber],
            //   items_width_4: ['', ValidateOnlyNumber],
            //   items_height_4: ['', ValidateOnlyNumber],
            //   items_price_4: ['', ValidateOnlyNumber],
            //   items_price_currency_4: '',
            // }),
            items_type: [this.query.data.items_type],

            client_comment: [''],
            delivery_price: [''],
            rate: [''],
            user_comment: [''],
        });
    }

    private calculation (): boolean | void {
        if (isNaN(this.tariffs)) {
            return false;
        }

       this.summ = this.tariffs - (this.tariffs * this.query.discount / 100);

       if (this.nds) {
           this.summ = this.summ - (this.summ * this.nds / 100);
       } else {
           this.summ = this.summ - (this.summ * this.query.fuel_markup / 100);
       }
    }

    private buildAutocompleteCityToFormComponent () {
      this.autocompleteCityToFormComponent = {
        searchSelectAutocomplete: {
          formControlName: 'city_to',
          label: 'Город получателя',
          value: '',
        },
        searchInfoAutocomplete: {
          country: {
            formControlName: 'country_to',
            label: 'Страна получателя',
            value: '',
            searchEvent: 'countryName',
          },
          region: {
            formControlName: 'region_to',
            label: 'Район/Штат получателя',
            value: '',
            searchEvent: 'region',
          },
          zip_code: {
            formControlName: 'zip_code_to',
            label: 'Почтовый индекс получателя',
            value: '',
            searchEvent: 'postal_code',
          }
        }
      }
    }

    private buildAutocompleteCityFromFormComponent () {
      this.autocompleteCityFromFormComponent = {
        searchSelectAutocomplete: {
          formControlName: 'city_from',
          label: 'Город отправителя',
          value: '',
        },
        searchInfoAutocomplete: {
          country: {
            formControlName: 'country_from',
            label: 'Страна отправителя',
            value: '',
            searchEvent: 'countryName',
          },
          region: {
            formControlName: 'region_from',
            label: 'Район/Штат отправителя',
            value: '',
            searchEvent: 'region',
          },
          zip_code: {
            formControlName: 'zip_code_from',
            label: 'Почтовый индекс получателя',
            value: '',
            searchEvent: 'postal_code',
          }
        }
      }
    }
}
