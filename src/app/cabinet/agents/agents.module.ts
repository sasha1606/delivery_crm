import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { TextMaskModule } from 'angular2-text-mask';

import { AgentsRoutingModule } from './agents-routing.module';

import { AgentsComponent } from './agents.component';
import { AgentCreateComponent } from './agent-create-edit/agent-create.component';
import { AgentFormElementComponent } from './agent-form-element/agent-form-element.component';
import { AgentFormElementShowComponent } from './agent-form-element-show/agent-form-element-show.component';
import { AgentShowComponent } from './agent-show/agent-show.component';

import { AgentsService } from './agents.service';
import { AgentsResolve } from './agents.resolve';
import { AgentCreateResolve } from './agent-create-edit/agent-create.resolve';
import { AgentGetResolve } from './agent-get.resolve';

import {NgSelectModule} from '@ng-select/ng-select';

import { SelectModule } from '@shared/select/select.module';
import { MultiSelectModule } from '@shared/multi-select/multi-select.module';
import { PaginationModule } from '@shared/pagination/pagination.module';
import { PipeModule } from '@shared/pipes/pipe.module';
import { FormAutocompleteModule } from '@shared/form-autocomplete/form-autocomplete.module';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';
import { QueriesPriceListService } from '@shared/services/queries-price-list.service';
import { AutocompleteCityModule } from '@shared/components/autocomplete-city/autocomplete-city.module';
import { ParcelFormParamsModule } from '@shared/components/parcel-form-params/parcel-form-params.module';
import { InsuranceFormElemModule } from '@shared/components/insurance-form-elem/insurance-form-elem.module';


@NgModule ({
    imports: [
        MultiselectDropdownModule,
        CommonModule,
        AgentsRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        PaginationModule,
        MultiSelectModule,
        SelectModule,
        PipeModule,
        TextMaskModule,
        FormAutocompleteModule,
        NgSelectModule,
        AutocompleteCityModule,
        ParcelFormParamsModule,
        InsuranceFormElemModule,
    ],
    declarations: [
        AgentsComponent,
        AgentCreateComponent,
        AgentFormElementComponent,
        AgentFormElementShowComponent,
        AgentShowComponent,
    ],
    providers: [
        AgentsService,
        AgentsResolve,
        AgentCreateResolve,
        AgentGetResolve,
        FindToAutocompleteService,
        QueriesPriceListService,
    ],
})

export class AgentsModule {

}
