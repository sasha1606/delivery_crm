import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TariffsDklComponent } from './tariffs-dkl.component';

import { TariffsDklResolve } from './tariffs-dkl.resolve';

const ROUTERS: Routes = [
    {
        path: '',
        component: TariffsDklComponent,
        resolve: {
          tariffs: TariffsDklResolve
        }
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class TariffsDklRoutingModule {}
