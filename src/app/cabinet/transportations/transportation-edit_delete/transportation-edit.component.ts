import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { maskPhone } from '@shared/const/masks';

@Component ({
    selector: 'app-transportation-edit',
    templateUrl: './transportation-edit.component.html',
    styleUrls: ['./transportation-edit.component.css'],
})

export class TransportationEditComponent implements OnInit {
    title = '';
    form: FormGroup;
    userId: number;

    public maskPhone =  maskPhone;

    constructor (
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private fb: FormBuilder,
    ) {}

    ngOnInit () {
        this.buildForm();

        this.activatedRoute.params.subscribe( params => {
           this.userId = params['id'];

           if (isNaN(this.userId)) {
               this.router.navigate(['/cabinet/transportations']);
           } else {
               console.log(this.userId);
           }
        });
    }

    buildForm () {
        this.form = this.fb.group({
            agentName: ['', [
                Validators.required
            ]],
            typeService: ['', [
                Validators.required
            ]],
            responsiblePerson: ['', [
                Validators.required
            ]],
            email: ['', [
                Validators.required
            ]],
            tel: ['', [
                Validators.required
            ]]
        });
    }

    onSubmit (form) {
        console.log(this.form.valid);
        if (this.form.valid) {
            console.log('form valid', this.form.valid);
        } else {
            Object.keys(this.form.controls).forEach(field => {
                const control = this.form.get(field);
                control.markAsTouched({ onlySelf: true });
                control.markAsDirty({onlySelf: true});
            });

        }
        console.log(form);
    }
}
