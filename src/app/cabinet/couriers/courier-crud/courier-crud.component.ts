import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { FromValidation } from '@fromValidation';

import { CouriersService } from '../couriers.service';

import {CouriersGet, crud} from '../couriers';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';

import { maskPhone } from '@shared/const/masks';
import { ToastsManager } from 'ng2-toastr';
import {AutocompleteCity} from '@shared/interface/autocomplete-city';


@Component ({
    selector: 'app-courier-crud',
    templateUrl: './courier-crud.component.html',
    styleUrls: ['./courier-crud.component.css']
})

export class CourierCrudComponent extends FromValidation implements OnInit {

    destroy$: Subject<boolean> = new Subject<boolean>();

   public maskPhone =  maskPhone;

    title = 'Добавить курьера';
    form: FormGroup;

    crudPage: string;
    idCourier: number;
    courier: CouriersGet;
    // public titleCountry_from = '';
    // public postalCode_from;
    // public titleRegion_from;
    // public local_from = [];

  public autocompleteCityFormComponent: AutocompleteCity;

    constructor (
        public couriersService: CouriersService,
        public router: Router,
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder,
        private findToAutocompleteService: FindToAutocompleteService,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef,
    ) {
      super (
        couriersService,
        router,
      );
      this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit () {
        this.courier = this.activatedRoute.snapshot.data['courier'];
        this.crudPage = this.activatedRoute.snapshot.data['crud'];
        this.activatedRoute.params.takeUntil(this.destroy$).subscribe(params => {
          this.idCourier = +params['id'];
        });
        this.buildForm();

        if (crud.show === this.crudPage) {
          this.form.disable();
        }

        this.buildAutocompleteCityFormComponent();

    }

  /*========= LOCATION =========*/
  // public isLiction;
  // onGetLocation(city) {
  //   this.isLiction = this.findToAutocompleteService.findLocation(city)
  //     .subscribe(res => {
  //       this.local_from = res.data;
  //     });
  //
  // }
  //
  // onGetLocationFrom(event) {
  //   this.local_from = [];
  //   const test2 = event.target.value;
  //   this.onGetLocation(test2);
  // }
  //
  // onSelectLocationFrom(event) {
  //   // this.titleCountry_from = event.countryName;
  //   // this.titleRegion_from = event.region;
  //   // this.postalCode_from = event.postal_code;
  //   if (!event) {
  //     return false;
  //   }
  //   this.form.get('zip_code').setValue(event.postal_code);
  //   this.form.get('country').setValue(event.countryName);
  //   // this.form.get('zip_code').setValue(event.postal_code);
  //
  // }
  /*========= / LOCATION =========*/


    public onSubmit (form): void {
      if (this.form.valid) {
        let answer;
        if (this.crudPage === crud.edit) {
          answer = this.couriersService.updateCourier( {courier: this.form.value, id: this.idCourier} ).takeUntil(this.destroy$);
        } else if (this.crudPage === crud.create) {
          answer =  this.couriersService.createCourier(this.form.value).takeUntil(this.destroy$);
        }

        answer.takeUntil(this.destroy$).subscribe( res => {
          if (res.success === 'ok') {
            this.toastr.success('Successfully');
            setTimeout(() => {
              this.router.navigateByUrl('/cabinet/couriers');
            }, 1000);
          }  else {
            this.toastr.error('Error');
            this.validationFormBackEnd(res.validation, this.form);
          }
        });

      } else {
        this.toastr.error('Error');
        this.validationFormsSubmit(this.form);
      }
    }


    private buildForm () {
        this.form = this.fb.group({
            code: [this.courier.data.code, [
                Validators.required,
            ]],
            fio: [this.courier.data.fio, [
                Validators.required,
            ]],
            email: [this.courier.data.email, [
                Validators.required,
            ]],
            phone: [this.courier.data.phone, [
                Validators.required,
            ]],
            birthday: this.courier.data.birthday,
            // country: this.courier.data.country,
            // city: this.courier.data.city,
            // zip_code: this.courier.data.zip_code,
            address: this.courier.data.address,
            house: this.courier.data.house,
            corps: this.courier.data.corps,
            room: this.courier.data.room,
            comment: this.courier.data.comment,
            // filesList: this.courier.data.filesList,
            filesUpload: this.courier.data.filesUpload,
            deleteFiles: '',
        });
    }

  private buildAutocompleteCityFormComponent () {
    this.autocompleteCityFormComponent = {
      searchSelectAutocomplete: {
        formControlName: 'city',
        label: 'Город',
        value: this.courier.data.city,
      },
      searchInfoAutocomplete: {
        country: {
          formControlName: 'country',
          label: 'Страна',
          value: this.courier.data.country,
          searchEvent: 'countryName',
        },
        zip_code: {
          formControlName: 'zip_code',
          label: 'Индекс',
          value: this.courier.data.zip_code,
          searchEvent: 'postal_code',
        }
      }
    }
  }
}
