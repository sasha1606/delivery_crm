import { Component, EventEmitter, Input, OnInit, Output, OnChanges, SimpleChanges } from '@angular/core';
import { Pagination } from '../interface/pagination';

@Component ({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.css'],
})

export class PaginationComponent implements OnInit, OnChanges {
    @Input() paginationData: Pagination;
    @Input() page: number;

    @Output() pageChange = new EventEmitter();

    from: number;
    to: number;
    count: number[];

    constructor (

    ) {}

    ngOnInit () {
    }

    ngOnChanges (changes: SimpleChanges) {
      if (changes.paginationData && changes.paginationData.currentValue) {
        this.count = new Array(this.paginationData.pagecount);
        this.from = (this.paginationData.pagesize * (this.page - 1)) + 1;
        this.to = ((this.paginationData.pagesize * (this.page - 1))) + this.paginationData.itemscount;
      }
    }

    public setPage (page: number): void {
        this.pageChange.emit(page);
    }

    public previous () {
        if (this.page - 1 > 0) {
            this.pageChange.emit(this.page - 1);
        }
    }

    public nex () {
        if (this.page + 1 <= this.paginationData.pagecount) {
            this.pageChange.emit(this.page + 1);
        }
    }

    public first () {
        this.pageChange.emit(1);
    }

    public last () {
        this.pageChange.emit(this.paginationData.pagecount);
    }
}
