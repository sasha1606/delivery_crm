import { Injectable } from '@angular/core';

import { ApiSrc } from '@api/api.src';

import { ApiService } from '@api/api.service';

@Injectable()

export class FindToAutocompleteService {

  constructor(private apiService: ApiService) {
  }

  public findUser (user) {
    // for IE
    const  keyword = encodeURIComponent(user);
    return this.apiService.get(ApiSrc.findUser + '?keyword=' + keyword);
  }

  public findLocation (city) {
    return this.apiService.get(ApiSrc.findLocation + city);
  }

}
