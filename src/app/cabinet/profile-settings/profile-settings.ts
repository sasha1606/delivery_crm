export interface ProfileSettings {
    email: string;
    firstname: string;
    lastname: string;
    phone: string;
}

export interface ProfileSettingsGet {
    data: ProfileSettings;
    success: string;
}
