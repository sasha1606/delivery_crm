import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Observable';
import { switchMap} from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import { ApiSrc } from '@api/api.src';

import { ApiService } from '@api/api.service';

import { QueriesGet, Queries, QueriesList } from './queries';
import { Response } from '@shared/interface/response';
// import {HttpClient} from '@angular/common/http';

@Injectable()

export class QueriesService {

    constructor (
        private apiService: ApiService,
        // private http: HttpClient
    ) {}

    public getQueriesList (): Observable<QueriesList> {
        return this.apiService.get(ApiSrc.queries.list);
    }

    public search (sort: string = '', key: string = '', page: number = 1, date = '',  orders = '', manager): Observable<QueriesList> {
      // for IE
      const  keyword = encodeURIComponent(key);
      return this.apiService.get(`${ApiSrc.queries.list}?sort=${sort}&keyword=${keyword}&page=${page}&date=${date}&orders=${orders}&manager=${manager}`);
    }



    public dataCreateQueries (): Observable<QueriesGet> {
        return this.apiService.get(ApiSrc.queries.queriesCreate);
    }

/*  public getLocationQueries (city) {
      const url = 'http://dklcrm.profitserver.in.ua/api/crm/location?cityname=' + city;
    return this.http.get(url);
  }*/

  public getLocationQueries (city) {
    return this.apiService.get(ApiSrc.queries.location + city);
  }

  public getUserInfo (user) {
    return this.apiService.get(ApiSrc.findUser + '?keyword=' + user);
  }

    public createQueries (queries: Queries): Observable<Response> {
        return this.apiService.post(ApiSrc.queries.queriesCreate, queries);
    }

    public getQueries ( queriesId: number ): Observable<QueriesGet> {
        return this.apiService.get(`${ApiSrc.queries.edit}/${queriesId}`);
    }

    public updateQueries (queries: Queries, userId: number): Observable<Response> {
        return this.apiService.post(`${ApiSrc.queries.edit}/${userId}`, queries);
    }

    public sentLetter (id: number): Observable<Response> {
        return this.apiService.get(`${ApiSrc.queries.sentLetter}/${id}`);
    }

    public updateSendLetter (queries: Queries, userId: number): Observable<Response> {
      return this.updateQueries(queries, userId).pipe(
          switchMap(res => {
            if (res.success === 'ok') {
              return this.sentLetter(userId);
            }
            return of(res);
          })
        );
    }



    public changetransportersType(type: string) {
        return this.apiService.post(ApiSrc.queries.transportersType, {transportersType: type});
    }

    public changeTransporterId(form: object) {
      return this.apiService.post(ApiSrc.queries.transporterId, form);
    }

    public changeDirections(form: object) {
      return this.apiService.post(ApiSrc.queries.directions, form);
    }

    public modalSendForm (form: object) {
      return this.apiService.post(ApiSrc.queries.modalSend, form);
    }
}
