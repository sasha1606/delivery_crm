import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

import { CustomersUsersService } from '../customers-users.service';

@Injectable()

export class CustomerUserShowResolve implements Resolve<any> {
    constructor (
        private customersUsersService: CustomersUsersService,
        private router: Router,
    ) {}

    resolve (router: ActivatedRouteSnapshot) {
        return this.customersUsersService.showUser(Number(router.paramMap.get('id'))).catch(err => {
            this.router.navigateByUrl('/cabinet/customers-users');
            return Observable.of({error: err});
        });
    }
}
