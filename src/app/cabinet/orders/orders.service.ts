import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Observable';
import {switchMap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';

import { ApiSrc } from '@api/api.src';

import { ApiService } from '@api/api.service';

import { OrdersGet, Orders, OrdersList } from './orders';
import { Response } from '@shared/interface/response';

@Injectable()

export class OrdersService {

  constructor(private apiService: ApiService) {
  }

  public getOrdersList(): Observable<OrdersList> {
    return this.apiService.get(ApiSrc.orders.list);
  }

  public search ({sort = '', key = '', page = 1, date = '', manager = '', status = ''}): Observable<OrdersList> {
    // for IE
    const  keyword = encodeURIComponent(key);
    return this.apiService.get(`${ApiSrc.orders.list}?sort=${sort}&keyword=${keyword}&page=${page}&date=${date}&manager=${manager}&status=${status}`);
  }


  public dataCreateOrders(): Observable<OrdersGet> {
    return this.apiService.get(ApiSrc.orders.ordersCreate);
  }


  public getLocationOrders(city) {
    return this.apiService.get(ApiSrc.orders.location + city);
  }

  public createOrder(orders: Orders): Observable<Response> {
    return this.apiService.post(ApiSrc.orders.ordersCreate, orders);
  }

  public getOrders(ordersId: number): Observable<OrdersGet> {
    return this.apiService.get(`${ApiSrc.orders.edit}/${ordersId}`);
  }

  public updateOrders(orders: Orders, userId: number): Observable<Response> {
    return this.apiService.post(`${ApiSrc.orders.edit}/${userId}`, orders);
  }

  public sendAgain (id) {
    return this.apiService.get(`${ApiSrc.orders.sendAgain}/${id}`);
  }

  public getUserInfo (user) {
    return this.apiService.get(ApiSrc.findUser + '?keyword=' + user);
  }

  public getLocationQueries (city) {
    return this.apiService.get(ApiSrc.queries.location + city);
  }

  // public sentLetter(id: number): Observable<Response> {
  //   return this.apiService.get(`${ApiSrc.orders.sentLetter}/${id}`);
  // }

  // public updateSendLetter(orders: Orders, userId: number): Observable<Response> {
  //   return this.updateOrders(orders, userId).pipe(
  //     switchMap(res => {
  //       if (res.success === 'ok') {
  //         return this.sentLetter(userId);
  //       }
  //       return of(res);
  //     })
  //   );
  // }

  // public changetransportersType(type: string) {
  //   return this.apiService.post(ApiSrc.orders.transportersType, {transportersType: type});
  // }
  //
  // public changeTransporterId(form: object) {
  //   return this.apiService.post(ApiSrc.orders.transporterId, form);
  // }
  //
  // public changeDirections(form: object) {
  //   return this.apiService.post(ApiSrc.orders.directions, form);
  // }
  //
  // public modalSendForm(form: object) {
  //   return this.apiService.post(ApiSrc.orders.modalSend, form);
  // }
}
