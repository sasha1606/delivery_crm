import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../auth.guard';

import { CabinetComponent } from './cabinet.component';

// import { DashboardComponent } from './dashboard/dashboard.component';
// import { QueriesComponent } from './queries/queries.component';
// import { OrdersComponent } from './orders/orders.component';
// import { StorageComponent } from './storage/storage.component';
// import { AgentsComponent } from './agents/agents.component';
// import { TransportationsComponent } from './transportations/transportations.component';
// import { QueriesPriceComponent } from './queries-price/queries-price.component';
// import { CourierComponent } from './couriers/couriers.component';
// import { AdminUsersComponent } from './admin-users/admin-users.component';
// import { CustomersUsersComponent } from './customers-users/customers-users.component';
// import { AccountingComponent } from './accounting/accounting.component';
// import { AnalyticsComponent } from './analytics/analytics.component';
// import { TariffsDklComponent } from './tariffs-dkl/tariffs-dkl.component';
// import { TariffsTransportationsComponent } from './tariffs-transportations/tariffs-transportations.component';
// import { StorageDealComponent } from './storage-deal/storage-deal.component';
// import { TariffsCustomsComponent } from './tariffs-customs/tariffs-customs.component';
// import { TaskmanComponent } from './taskman/taskman.component';

const ROUTERS: Routes = [
    {
        path: 'cabinet',
        component: CabinetComponent,
        // canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        // CanActivateChild: [ AuthGuard ],
        // children: [
        //     { path: 'dashboard', component: DashboardComponent },
        //     { path: 'queries', component: QueriesComponent },
        //     { path: 'orders', component: OrdersComponent },
        //     { path: 'storage', component: StorageComponent },
        //     { path: 'agents', component: AgentsComponent },
        //     { path: 'transportations', component: TransportationsComponent },
        //     { path: 'querie-price', component: QueriesPriceComponent },
        //     { path: 'couriers', component: CourierComponent },
        //     { path: 'admin-users', component: AdminUsersComponent },
        //     { path: 'customers-users', component: CustomersUsersComponent },
        //     { path: 'accounting', component: AccountingComponent },
        //     { path: 'analytics', component: AnalyticsComponent },
        //     { path: 'tariffs-dkl', component: TariffsDklComponent },
        //     { path: 'tariffs-transportations', component: TariffsTransportationsComponent },
        //     { path: 'storage-deal', component: StorageDealComponent },
        //     { path: 'tariffs-customs', component: TariffsCustomsComponent },
        //     { path: 'taskman', component: TaskmanComponent },
        // ]
        children: [
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule',
            },
            {
                path: 'queries',
                loadChildren: './queries/queries.module#QueriesModule'
            },
            {
                path: 'orders',
                loadChildren: './orders/orders.module#OrdersModule'
            },
            {
                path: 'storage',
                loadChildren: './storage/storage.module#StorageModule',
            },
            {
                path: 'agents',
                loadChildren: './agents/agents.module#AgentsModule'
            },
            {
                path: 'transportations',
                loadChildren: './transportations/transportations.module#TransportationsModule'
            },
            {
                path: 'queries-price',
                loadChildren: './queries-price/queries-price.module#QueriesPriceModule'
            },
            {
                path: 'couriers',
                loadChildren: './couriers/couriers.module#CouriersModule'
            },
            {
                path: 'admin-users',
                loadChildren: './admin-users/admin-users.module#AdminUsersModule'
            },
            {
                path: 'customers-users',
                loadChildren: './customers-users/customers-users.module#CustomersUsersModule'
            },
            {
                path: 'accounting',
                loadChildren: './accounting/accounting.module#AccountingModule'
            },
            {
                path: 'analytics',
                loadChildren: './analytics/analytics.module#AnalyticsModule'
            },
            {
                path: 'tariffs-dkl',
                loadChildren: './tariffs/tariffs-dkl/tariffs-dkl.module#TariffsDklModule'
            },
            { path: 'tariffs-transportations',
              loadChildren: './tariffs/tariffs-transportations/tariffs-transportations.module#TariffsTransportationsModule'
            },
            {
                path: 'storage-deal',
                loadChildren: './tariffs/storage-deal/storage-deal.module#StorageDealModule'
            },
            {
                path: 'tariffs-customs',
                loadChildren: './tariffs/tariffs-customs/tariffs-customs.module#TariffsCustomsModule'
            },
            {
                path: 'taskman',
                loadChildren: './taskman/taskman.module#TaskmanModule'
            },
            {
                path: 'profile',
                loadChildren: './profile-settings/profile-settings.module#ProfileSettingsModule'
            },
        ],
        runGuardsAndResolvers: 'always',
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class CabinetRoutingModule {}
