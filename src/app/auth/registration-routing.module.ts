import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import { RegistrationComponent } from './registration_delete/registration.component';
import { LoginComponent } from './login/login.component';
import { PasswordRecoveryComponent } from './password-recovery/password-recovery.component';

const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'password',
        component: PasswordRecoveryComponent,
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class RegistrationRouting {}
