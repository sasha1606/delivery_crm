import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TariffsCustomsRoutingModule } from './tariffs-customs-routing.module';

import { TariffsCustomsComponent } from './tariffs-customs.component';

import { TariffsCustomsResolve } from './tariffs-customs.resolve';
import { TariffsCustomsService } from './tariffs-customs.service';

@NgModule({
  imports: [
    CommonModule,
    TariffsCustomsRoutingModule,
  ],
  declarations: [
    TariffsCustomsComponent,
  ],
  providers: [
    TariffsCustomsResolve,
    TariffsCustomsService,
  ]
})

export class TariffsCustomsModule { }
