import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Observable';

import { ApiSrc } from '@api/api.src';

import { ApiService } from '@api/api.service';

import { StorageGet, Storage, StorageList } from './storage';
import { Response } from '@shared/interface/response';

@Injectable()

export class StorageService {

  constructor(private apiService: ApiService) {
  }

  public getStoragesList(): Observable<StorageList> {
    return this.apiService.get(ApiSrc.storage.list);
  }

  public search ({sort = '', key = '', page = 1, date = '', manager}): Observable<StorageList> {
    // for IE
    const  keyword = encodeURIComponent(key);
    return this.apiService.get(`${ApiSrc.storage.list}?sort=${sort}&keyword=${keyword}&page=${page}&date=${date}&manager=${manager}`);
  }

  public dataCreateStorage(): Observable<StorageGet> {
    return this.apiService.get(ApiSrc.storage.create);
  }

  public getLocationStorage(city) {
    return this.apiService.get(ApiSrc.storage.location + city);
  }

  public createStorage(storage: Storage): Observable<Response> {
    return this.apiService.post(ApiSrc.storage.create, storage);
  }

  public getStorages(storageId: number): Observable<StorageGet> {
    return this.apiService.get(`${ApiSrc.storage.edit}/${storageId}`);
  }

  public updateStorages(storage: Storage, userId: number): Observable<Response> {
    return this.apiService.post(`${ApiSrc.storage.edit}/${userId}`, storage);
  }

  public getUserInfo (user) {
    return this.apiService.get(ApiSrc.findUser + '?keyword=' + user);
  }

  public getLocationQueries (city) {
    return this.apiService.get(ApiSrc.queries.location + city);
  }

  public sendCreateManifest (form) {
    return this.apiService.post(ApiSrc.storage.createManifest, form);
  }

  public sendRequestQuote (form) {
    return this.apiService.post(ApiSrc.storage.quote, form);
  }
}
