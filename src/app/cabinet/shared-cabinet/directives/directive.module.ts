import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccessRightsDirective } from '@shared/directives/access-rights.directive';

@NgModule ({
  imports: [
    CommonModule
  ],
  declarations: [
    AccessRightsDirective
  ],
  exports: [
    AccessRightsDirective
  ]
})

export class DirectiveModule {

}
