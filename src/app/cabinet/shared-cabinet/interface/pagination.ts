export interface Pagination {
    itemscount: number;
    page: number;
    pagecount: number;
    pagesize: number;
    totalcount: number;
}
