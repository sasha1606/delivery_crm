import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';

@Component ({
  selector: 'app-search-table',
  templateUrl: './search-table.component.html',
  styleUrls: ['./search-table.component.css']
})

export class SearchTableComponent implements OnInit {

  @Input() anotherFilterData = '';
  @Input() anotherFilterDataSecond = '';
  @Input() anotherFilterPlaceholder = '';
  @Input() anotherFilterPlaceholderSecond = '';
  @Input() searchInput = 'Поиск по слову';

  @Output() searchData: EventEmitter<any> = new EventEmitter();

  public date = '';
  public searchWord = '';
  public anotherFilter = '';
  public anotherFilterSecond = '';

  constructor () {

  }

  ngOnInit () {
  }

  public getDate (date): void {
    this.date = date;
  }

  public tableSearch (): void {
    this.sendSortData();
  }

  private sendSortData () {
    this.searchData.emit({data: this.date, word: this.searchWord, anotherFilter:  this.anotherFilter, anotherFilterSecond: this.anotherFilterSecond});
  }

}
