import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

import { AdminUsersService } from '../admin-users.service';

@Injectable()

export class AdminUsersEditResolve implements Resolve<any> {

    constructor (
        private adminUsersService: AdminUsersService,
        private router: Router,
    ) {}

    resolve (router: ActivatedRouteSnapshot) {
        return this.adminUsersService.getUser(Number(router.paramMap.get('id'))).catch(err => {
            this.router.navigateByUrl('/cabinet/admin-users');
            return Observable.of({error: err});
        });
    }
}
