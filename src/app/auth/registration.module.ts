import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { LoginComponent } from './login/login.component';
import { PasswordRecoveryComponent } from './password-recovery/password-recovery.component';

import { RegistrationRouting } from './registration-routing.module';

import { AuthService } from './auth.service';

@NgModule ({
    imports: [
        CommonModule,
        RegistrationRouting,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
    declarations: [
       LoginComponent,
       PasswordRecoveryComponent,
    ],
    providers: [
        AuthService,
    ]
})

export class RegistrationModule { }
