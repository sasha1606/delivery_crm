import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { TransportationsService } from './transportations.service';

@Injectable()

export class TransportationsResolve implements Resolve<any> {

  constructor (
    private transportationsService: TransportationsService
  ) {}

  resolve () {
    return this.transportationsService.getTransportationsList();
  }

}
