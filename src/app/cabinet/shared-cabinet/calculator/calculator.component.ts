import {Component, Input, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css'],
})

export class CalculatorComponent implements OnInit, OnChanges {

  @Input() form;
  @Input() discount;
  @Input() fuelMarkupPercentData;

  public nds = 0;
  public summ = 0;
  public tariffs = 0;
  public ndsPercent = 0;
  public fuelMarkup = 0;
  public fuelMarkupPercent = 0;

  ngOnInit () {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.discount && changes.discount.currentValue >= 0) {
      this.discount = changes.discount.currentValue;
    }
  }

  public calculation (): boolean | void {
    if (isNaN(this.tariffs)) {
      return false;
    }
    this.fuelMarkup = this.rounded(this.tariffs * (this.fuelMarkupPercent / 100));
    const sizeDiscount = this.tariffs * (this.discount / 100);
    const intermediate = this.fuelMarkup + (this.tariffs - sizeDiscount);
    this.nds = this.rounded(intermediate * (this.ndsPercent / 100));

    this.summ =  this.rounded(this.nds + intermediate);
  }

  public setCalcToFrom () {
    this.buildForm();
  }

  private buildForm () {
    this.form.addControl('tarif', new FormControl(this.tariffs));
    this.form.addControl('sale_percent', new FormControl(this.discount));
    this.form.addControl('nds', new FormControl(this.nds));
    this.form.addControl('nds_percent', new FormControl(this.ndsPercent));
    this.form.addControl('fuel_comission', new FormControl(this.fuelMarkup));
    this.form.addControl('fuel_comission_percent', new FormControl(this.fuelMarkupPercent));
    this.form.get('delivery_price').setValue(this.summ);
  }

  private rounded  (number: string | number): number {
    return Math.round(parseFloat(String(number)) * 100) / 100;
  }

}
