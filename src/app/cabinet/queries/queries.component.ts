import {Component, OnInit, OnDestroy, ViewContainerRef} from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Validators, FormControl, FormBuilder, FormGroup} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { QueriesService } from './queries.service';

import { FromValidation } from '@fromValidation';

import { QueriesList, QueriesGet } from './queries';
import {ToastsManager} from 'ng2-toastr/ng2-toastr';

import { maskPhone } from '@shared/const/masks';
import { ValidateOnlyNumber } from '@shared/formValidators/validateOnlyNumber';

@Component ({
    selector: 'app-queries',
    templateUrl: './queries.component.html',
    styleUrls: ['./queries.component.css']
})

export class QueriesComponent extends FromValidation implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    public maskPhone =  maskPhone;

    public managerList = {};
    public managerId: number;

    public title = 'Запросы';
    public form: FormGroup;

    public queriesList: QueriesList;
    public query: QueriesGet;

    public modalRef: NgbModalRef;

    public queryId: number;
    public sort = '';
    public date = '';
    public page = 1;
    public orders = '';
    public search = '';


    constructor (
        private toastr: ToastsManager,
        private vcr: ViewContainerRef,
        public queriesService: QueriesService,
        public router: Router,
        private modalService: NgbModal,
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder,

    ) {
      super(
        router,
        queriesService,
      );
      this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit () {
        this.queriesList = this.activatedRoute.snapshot.data['queryList'];
        this.managerList = this.queriesList.listmanagers;
        this.buildFrom();
    }

    ngOnDestroy () {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

    public getDate (date): void {
        this.date = date;
    }

    public tablePage (page: number): void {
        this.page = page;
        this.sendSort();
    }

    public tableSort (param: string): void {
        if (this.sort.length > 0 && param === this.sort) {
            param = `-${param}`;
        }
        this.sort = param;
        this.sendSort();
    }

    public tableSearch (word: string): void {
        this.search = word;
        this.sendSort();
    }

    public tableOrders (val): void {
        this.orders = val;
        this.sendSort();
    }

    public open (content): void {
       this.modalRef =  this.modalService.open(content, {size: 'lg'});
    }

    public editQueries (orderId: number, modal: object): void {
        this.queryId = orderId;
        this.queriesService.getQueries(orderId).takeUntil(this.destroy$).subscribe( queries => {
            this.query = queries;
            this.open(modal);
            this.setFromValue(queries);
        });
    }

    public sendLetter(): void {

     /* this.toastr.success('Traffic Source has been created successfully');*/

      if (this.form.valid) {
        this.queriesService.updateSendLetter(this.form.value, this.queryId).takeUntil(this.destroy$).subscribe( res => {
          if (res.success === 'ok') {
            this.toastr.success('Has been sent successfully');
            this.modalRef.close();
          } else {
            this.toastr.error('Error');
            this.validationFormBackEnd(res.validation, this.form);
          }
        });

      } else {
        this.toastr.error('Error');
        this.validationFormsSubmit(this.form);
      }

    }

    public getSearchData (data)  {
      this.date = data.data;
      this.search = data.word;
      this.managerId = data.anotherFilter;
      this.sendSort();
    }

    private buildFrom (): void {
        this.form = this.fb.group({
            name: [''],
            company: [''],
            email: [''],
            phone: [''],
            country_from: [''],
            city_from: [''],
            region_from: [''],
            zip_code_from: [''],
            type: [''],
            // insurance: [''],
            // insurance_price: [''],
            items_price: [''],
            country_to: [''],
            city_to: [''],
            region_to: [''],
            zip_code_to: [''],
            items_count: [''],
            items_totalweight: [''],
            // insurance_currency: '',
            // itemsParams: this.fb.group({
            //   client_comment_1:  '',
            //   items_type_1:  '',
            //   delivery_type_1: '',
            //   items_count_1: ['', ValidateOnlyNumber],
            //   items_weight_1: ['', ValidateOnlyNumber],
            //   items_length_1: ['', ValidateOnlyNumber],
            //   items_width_1: ['', ValidateOnlyNumber],
            //   items_height_1: ['', ValidateOnlyNumber],
            //   items_price_1: ['', ValidateOnlyNumber],
            //   items_price_currency_1: '',
            //
            //   client_comment_2: '',
            //   items_type_2: '',
            //   delivery_type_2: '',
            //   items_count_2: ['', ValidateOnlyNumber],
            //   items_weight_2: ['', ValidateOnlyNumber],
            //   items_length_2: ['', ValidateOnlyNumber],
            //   items_width_2: ['', ValidateOnlyNumber],
            //   items_height_2: ['', ValidateOnlyNumber],
            //   items_price_2: ['', ValidateOnlyNumber],
            //   items_price_currency_2: '',
            //
            //   client_comment_3: '',
            //   items_type_3: '',
            //   delivery_type_3: '',
            //   items_count_3: ['', ValidateOnlyNumber],
            //   items_weight_3: ['', ValidateOnlyNumber],
            //   items_length_3: ['', ValidateOnlyNumber],
            //   items_width_3: ['', ValidateOnlyNumber],
            //   items_height_3: ['', ValidateOnlyNumber],
            //   items_price_3: ['', ValidateOnlyNumber],
            //   items_price_currency_3: '',
            //
            //   client_comment_4: '',
            //   items_type_4: '',
            //   delivery_type_4: '',
            //   items_count_4: ['', ValidateOnlyNumber],
            //   items_weight_4: ['', ValidateOnlyNumber],
            //   items_length_4: ['', ValidateOnlyNumber],
            //   items_width_4: ['', ValidateOnlyNumber],
            //   items_height_4: ['', ValidateOnlyNumber],
            //   items_price_4: ['', ValidateOnlyNumber],
            //   items_price_currency_4: '',
            // }),
            items_type: [''],
            client_comment: [''],
            delivery_price: [''],
            user_comment: [''],
        });
    }

    private setFromValue (queries: QueriesGet): void {
        Object.keys(this.form.controls).forEach(field => {
            const control = this.form.get(field);
            control.patchValue(queries.data[field]);
        });
    }

    private sendSort (): void {
        this.queriesService.search(
            this.sort, this.search, this.page, this.date, this.orders, this.managerId
        ).takeUntil(this.destroy$).subscribe( queries => {
            this.queriesList = queries;
        });
    }
}
