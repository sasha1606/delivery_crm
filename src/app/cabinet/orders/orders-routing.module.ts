import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdersComponent } from './orders.component';
import { OrderCreateComponent } from './order-create/order-create.component';
import { OrderShowComponent } from './order-show/order-show.component';
import { OrderEditComponent } from './order-edit/order-edit.component';

import { OrderShowResolve } from './order-show/order-show.resolve';
import { OrderEditResolve } from './order-edit/order-edit.resolve';
import { OrderCreateResolve } from './order-create/order-create.resolve';
import { OrdersResolve } from './orders.resolve';

const ROUTERS: Routes = [
    {
        path: '',
        component: OrdersComponent,
        resolve: {
          orderList: OrdersResolve,
        }
    },
    {
        path: 'create',
        component: OrderCreateComponent,
        resolve: {
          order: OrderCreateResolve,
        }
    },
    {
        path: 'show/:id',
        component: OrderShowComponent,
        resolve: {
          order: OrderShowResolve,
        }
    },
    {
        path: 'edit/:id',
        component: OrderEditComponent,
        resolve: {
          order: OrderEditResolve,
        }
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class OrdersRoutingModule {}
