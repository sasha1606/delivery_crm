import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';

import { AdminUsersRoutingModule } from './admin-users-routing.module';

import { AdminUsersService } from './admin-users.service';

import { AdminUsersResolve } from './admin-users.resolve';
import { AdminUsersEditResolve } from './admin-user-edit/admin-user-edit.resolve';
import { AdminUserCreateResolve } from './admin-user-create/admin-user-create.resolve';

import { AdminUsersComponent } from './admin-users.component';
import { AdminUserCreateComponent } from './admin-user-create/admin-user-create.component';
import { AdminUserEditComponent } from './admin-user-edit/admin-user-edit.component';

import { PipeModule } from '@shared/pipes/pipe.module';
import { PaginationModule } from '@shared/pagination/pagination.module';
import { SelectModule } from '@shared/select/select.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';
import {AutocompleteCityModule} from '@shared/components/autocomplete-city/autocomplete-city.module';

@NgModule ({
    imports: [
        CommonModule,
        AdminUsersRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        PipeModule,
        PaginationModule,
        SelectModule,
        NgSelectModule,
        TextMaskModule,
        AutocompleteCityModule,
    ],
    declarations: [
        AdminUsersComponent,
        AdminUserCreateComponent,
        AdminUserEditComponent,
    ],
    providers: [
        AdminUsersService,
        AdminUsersResolve,
        AdminUsersEditResolve,
        AdminUserCreateResolve,
        FindToAutocompleteService,
    ]
})

export class AdminUsersModule {}
