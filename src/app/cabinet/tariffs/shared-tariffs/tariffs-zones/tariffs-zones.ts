export interface TariffsZones {
  code: string;
  level: string;
  title: string;
}
