import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';

import { DatepickerModule } from '../shared-cabinet/datepicker/datepicker.module';
import { CustomersUsersRoutingModule } from './customers-users-routing.module';

import { CustomersUsersService } from './customers-users.service';

import { CustomerUserResolve } from './customer-user.resolve';
import { CustomerUserCreateResolve } from './customer-user-create/customer-user-create.resolve';
import { CustomerUserShowResolve } from './customer-user-show/customer-user-show.resolve';
import { CustomerUserEditResolve } from './customer-user-edit/customer-user-edit.resolve';

import { CustomersUsersComponent } from './customers-users.component';
import { CustomerUserCreateComponent } from './customer-user-create/customer-user-create.component';
import { CustomerUserShowComponent } from './customer-user-show/customer-user-show.component';
import { CustomerUserEditComponent } from './customer-user-edit/customer-user-edit.component';
import { CustomerUserOrdersListComponent } from './customer-user-show/customer-user-orders-list/customer-user-orders-list.component';

import { PipeModule } from '@shared/pipes/pipe.module';

import { PaginationModule } from '@shared/pagination/pagination.module';
import { TooltipModule } from '@shared/tooltip/tooltip.module';
import { SearchTableModule } from '@shared/search-table/search-table.module';
import { FormFileUploadModule } from '@shared/form-file-upload/form-file-upload.module';

import { HelpersService } from '@shared/services/helpers.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';
import { AddDirectionTableModule } from '@shared/add-direction-table/add-direction-table.module';
import { CreateBillModalModule } from '@shared/components/create-bill-modal/create-bill-modal.module';
import { AutocompleteCityModule } from '@shared/components/autocomplete-city/autocomplete-city.module';

@NgModule ({
    imports: [
        CommonModule,
        CustomersUsersRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        DatepickerModule,
        PipeModule,
        PaginationModule,
        TooltipModule,
        NgSelectModule,
        SearchTableModule,
        TextMaskModule,
        FormFileUploadModule,
        AddDirectionTableModule,
        CreateBillModalModule,
        AutocompleteCityModule,
    ],
    declarations: [
        CustomerUserEditComponent,
        CustomerUserCreateComponent,
        CustomerUserShowComponent,
        CustomersUsersComponent,
        CustomerUserOrdersListComponent,
        // KeysPipe,
    ],
    providers: [
        CustomersUsersService,
        CustomerUserResolve,
        CustomerUserCreateResolve,
        CustomerUserShowResolve,
        CustomerUserEditResolve,
        FindToAutocompleteService,
        HelpersService,
    ]
})

export class CustomersUsersModule {}
