import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component ({
  selector: 'app-search-table-order',
  templateUrl: './search-table-order.component.html',
  styleUrls: ['./search-table-order.component.css']
})

export class SearchTableOrderComponent {

  @Output() searchData: EventEmitter<any> = new EventEmitter();

  date = '';
  searchWord = '';

  constructor () {

  }

  // public getDate (date): void {
  //   this.date = date;
  // }

  public tableSearch (): void {
    this.sendSortData();
  }

  private sendSortData () {
    this.searchData.emit({data: this.date, word: this.searchWord});
  }

}
