import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { TariffsTransportationsService } from './tariffs-transportations.service';

@Injectable()

export class TariffsTransportationsResolve implements Resolve<any> {

  constructor (
    private tariffsTransportationsService: TariffsTransportationsService
  ) {}

  resolve () {
    return this.tariffsTransportationsService.getTariffsTransportations();
  }

}
