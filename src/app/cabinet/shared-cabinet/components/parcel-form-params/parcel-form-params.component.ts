import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ValidateOnlyNumber } from '@shared/formValidators/validateOnlyNumber';

@Component ({
  selector: 'app-parcel-form-params',
  templateUrl: './parcel-form-params.component.html',
  styleUrls: ['./parcel-form-params.component.css'],
})

export class ParcelFormParamsComponent implements OnInit {

  @Input() form: FormGroup;
  @Input() formData;
  @Input() formDisabled = false;
  @Input() calcFormControlName?;
  @Input() typeFormControlName?;

  public countItemsParams = new Array(4);

  constructor (
    private fb: FormBuilder,
  ) {}

  ngOnInit () {
    this.buildForm();

    if (this.formDisabled) {
      this.form.disable();
    }

    if (this.calcFormControlName) {
      this.subscribeItemsParams();
      this.subscribeType();
    }

  }

  buildForm () {
    this.form.addControl('itemsParams', this.fb.group({
      client_comment_1:  this.formData.itemsParams ? this.formData.itemsParams.client_comment_1 : '',
      items_type_1:  this.formData.itemsParams ? this.formData.itemsParams.items_type_1 : '',
      delivery_type_1: this.formData.itemsParams ? this.formData.itemsParams.delivery_type_1 : '',
      items_count_1: [this.formData.itemsParams ? this.formData.itemsParams.items_count_1 : '', ValidateOnlyNumber],
      items_weight_1: [this.formData.itemsParams ? this.formData.itemsParams.items_weight_1 : '', ValidateOnlyNumber],
      items_length_1: [this.formData.itemsParams ? this.formData.itemsParams.items_length_1 : '', ValidateOnlyNumber],
      items_width_1: [this.formData.itemsParams ? this.formData.itemsParams.items_width_1 : '', ValidateOnlyNumber],
      items_height_1: [this.formData.itemsParams ? this.formData.itemsParams.items_height_1 : '', ValidateOnlyNumber],
      items_price_1: [this.formData.itemsParams ? this.formData.itemsParams.items_price_1 : '', ValidateOnlyNumber],
      items_price_currency_1: this.formData.itemsParams ? this.formData.itemsParams.items_price_currency_1 : '',

      client_comment_2: this.formData.itemsParams ? this.formData.itemsParams.client_comment_2 : '',
      items_type_2: this.formData.itemsParams ? this.formData.itemsParams.items_type_2 : '',
      delivery_type_2: this.formData.itemsParams ? this.formData.itemsParams.delivery_type_2 : '',
      items_count_2: [this.formData.itemsParams ? this.formData.itemsParams.items_count_2 : '', ValidateOnlyNumber],
      items_weight_2: [this.formData.itemsParams ? this.formData.itemsParams.items_weight_2 : '', ValidateOnlyNumber],
      items_length_2: [this.formData.itemsParams ? this.formData.itemsParams.items_length_2 : '', ValidateOnlyNumber],
      items_width_2: [this.formData.itemsParams ? this.formData.itemsParams.items_width_2 : '', ValidateOnlyNumber],
      items_height_2: [this.formData.itemsParams ? this.formData.itemsParams.items_height_2 : '', ValidateOnlyNumber],
      items_price_2: [this.formData.itemsParams ? this.formData.itemsParams.items_price_2 : '', ValidateOnlyNumber],
      items_price_currency_2: this.formData.itemsParams ? this.formData.itemsParams.items_price_currency_2 : '',

      client_comment_3: this.formData.itemsParams ? this.formData.itemsParams.client_comment_3 : '',
      items_type_3: this.formData.itemsParams ? this.formData.itemsParams.items_type_3 : '',
      delivery_type_3: this.formData.itemsParams ? this.formData.itemsParams.delivery_type_3 : '',
      items_count_3: [this.formData.itemsParams ? this.formData.itemsParams.items_count_3 : '', ValidateOnlyNumber],
      items_weight_3: [this.formData.itemsParams ? this.formData.itemsParams.items_weight_3 : '', ValidateOnlyNumber],
      items_length_3: [this.formData.itemsParams ? this.formData.itemsParams.items_length_3 : '', ValidateOnlyNumber],
      items_width_3: [this.formData.itemsParams ? this.formData.itemsParams.items_width_3 : '', ValidateOnlyNumber],
      items_height_3: [this.formData.itemsParams ? this.formData.itemsParams.items_height_3 : '', ValidateOnlyNumber],
      items_price_3: [this.formData.itemsParams ? this.formData.itemsParams.items_price_3 : '', ValidateOnlyNumber],
      items_price_currency_3: this.formData.itemsParams ? this.formData.itemsParams.items_price_currency_3 : '',

      client_comment_4: this.formData.itemsParams ? this.formData.itemsParams.client_comment_4 : '',
      items_type_4: this.formData.itemsParams ? this.formData.itemsParams.items_type_4 : '',
      delivery_type_4: this.formData.itemsParams ? this.formData.itemsParams.delivery_type_4 : '',
      items_count_4: [this.formData.itemsParams ? this.formData.itemsParams.items_count_4 : '', ValidateOnlyNumber],
      items_weight_4: [this.formData.itemsParams ? this.formData.itemsParams.items_weight_4 : '', ValidateOnlyNumber],
      items_length_4: [this.formData.itemsParams ? this.formData.itemsParams.items_length_4 : '', ValidateOnlyNumber],
      items_width_4: [this.formData.itemsParams ? this.formData.itemsParams.items_width_4 : '', ValidateOnlyNumber],
      items_height_4: [this.formData.itemsParams ? this.formData.itemsParams.items_height_4 : '', ValidateOnlyNumber],
      items_price_4: [this.formData.itemsParams ? this.formData.itemsParams.items_price_4 : '', ValidateOnlyNumber],
      items_price_currency_4: this.formData.itemsParams ? this.formData.itemsParams.items_price_currency_4 : '',
    }));
  }

  private subscribeItemsParams () {
    this.form.get('itemsParams').valueChanges.subscribe( () => {
      this.calcEstimatedWeight();
    });
  }

  private subscribeType () {
    this.form.get(this.typeFormControlName).valueChanges.subscribe( () => {
      this.calcEstimatedWeight();
    });
  }

  private calcEstimatedWeight () {
    const itemsParams = this.form.get('itemsParams').value;
    let divider = 5000;

    if (this.form.get(this.typeFormControlName).value.includes('air')) {
      divider = 6000;
    }

    let tootleEstimatedWeigh: any = 0;
    for (let i = 1; i <= this.countItemsParams.length; i++) {
      if (isNaN(tootleEstimatedWeigh)) {
        tootleEstimatedWeigh = 0;
        break;
      }
      if (this.form.get(this.typeFormControlName).value === '6_road_freight') {
        tootleEstimatedWeigh += (+itemsParams[`items_length_${i}`] / 100) * (+itemsParams[`items_width_${i}`] / 100) * (+itemsParams[`items_height_${i}`] / 100) * 333 * +itemsParams[`items_count_${i}`];
        tootleEstimatedWeigh = Number(tootleEstimatedWeigh.toFixed(2).replace(/\.?0+$/, ''));
      } else {
        if (+itemsParams[`items_count_${i}`] && +itemsParams[`items_count_${i}`] > 0) {
          tootleEstimatedWeigh += (+itemsParams[`items_length_${i}`] * +itemsParams[`items_width_${i}`] * +itemsParams[`items_height_${i}`] / divider) * +itemsParams[`items_count_${i}`];
          // tootleEstimatedWeigh = Number(Number(tootleEstimatedWeigh).toFixed(2).replace(/\.?0+$/, ''));
          tootleEstimatedWeigh = Number(tootleEstimatedWeigh.toFixed(2).replace(/\.?0+$/, ''));
        }
      }
    }
    if (!isNaN(tootleEstimatedWeigh)) {
      this.form.get(this.calcFormControlName).setValue(tootleEstimatedWeigh.toFixed(2));
    }
  }
}
