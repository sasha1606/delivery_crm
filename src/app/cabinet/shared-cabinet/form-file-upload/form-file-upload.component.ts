import { Component, OnInit, Input } from '@angular/core';
import { typePage } from '@shared/const/crud';

@Component ({
  selector: 'app-form-file-upload',
  templateUrl: './form-file-upload.component.html',
  styleUrls: ['./form-file-upload.component.css'],
})

export class FormFileUploadComponent implements OnInit {

  @Input() form;
  @Input() inputName;
  @Input() files?;
  @Input() modeCrud?;
  @Input() deleteFilesName?;

  public fileArray = [];
  public fileDelteArray = [];
  public typePage = typePage;

  constructor () {}

  ngOnInit () {
    this.addFiles();
  }

  public fileChange (event) {
    const checkedFile = this.fileArray.some( (elem) => {
      return elem['name'] === event.target.files[0].name;
    });

    if (checkedFile) {
      return false;
    }

    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.fileArray.push({
          name: file.name,
          type: file.type,
          size: file.size,
          value: reader.result,
        });

        this.form.get(this.inputName).setValue(this.fileArray);
      };
    }
  }

  public deleteFile (file) {
    this.fileArray = this.fileArray.filter((item) => {
      return item.name !== file['name'];
    });
    this.checkFileWasAdded(file);
  }

  private addFiles() {
    if (this.files) {
      this.fileArray = this.files;
    }
  }

  private checkFileWasAdded (file) {
    if (file['size']) {
      this.form.get(this.inputName).setValue(this.fileArray);
    } else {
      this.fileDelteArray.push(file);
      this.form.get(this.deleteFilesName).setValue(this.fileDelteArray);
    }
  }

}
