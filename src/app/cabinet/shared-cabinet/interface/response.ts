export interface Response {
  error?: string;
  validation?: Object;
  success?: string;
  message?: string;
}
