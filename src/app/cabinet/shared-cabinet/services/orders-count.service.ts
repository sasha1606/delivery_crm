import { Injectable } from '@angular/core';

import { ApiSrc } from '@api/api.src';

import { ApiService } from '@api/api.service';
import { map } from 'rxjs/operators';

@Injectable()

export class OrdersCountService {

  constructor (
    private apiService: ApiService
  ) {

  }

  getCountsNewOrders () {
    return this.apiService.get(ApiSrc.countsNewOrders.countsNewOrders).pipe(
        map( (item) => {
            return item.data.filter( (filterItem) => {
              return filterItem.type === 'ORDERS';
            });
        })
    );
  }


  toZeroCountsNewOrders () {
    return this.apiService.get(ApiSrc.countsNewOrders.zeroCountsNewOrders, {type: 'ORDERS'});
  }

}
