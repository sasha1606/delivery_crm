import { Pagination } from '@shared/interface/pagination';

export interface AdminUserGet {
    data: AdminUser;
    success: string;
    validation?: string;
}

export interface AdminUser {
    address: string;
    city: string;
    username: string;
    country: string;
    email: string;
    firstname: string;
    lastname: string;
    phone: string;
    region: string;
    type: string;
    typeNames: object;
    zip_code: string;
}

export interface AdminList {
    listdata: ShortUser[];
    pagination: Pagination;
    status: string;
}

interface ShortUser {
    id: number;
    username: string;
    email: string;
    firstname: string;
    lastname: string;
    phone: string;
}

