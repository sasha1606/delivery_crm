import { Component, Input, OnInit } from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';

@Component ({
  selector: 'app-insurance-form-elem',
  templateUrl: './insurance-form-elem.component.html',
  styleUrls: ['./insurance-form-elem.component.css'],
})

export class InsuranceFormElemComponent implements OnInit {

  @Input() form: FormGroup;
  @Input() formData;
  @Input() formDisabled = false;

  public readOnlyInputs = false;

  constructor () {}

  ngOnInit () {
    this.buildForm();

    if (this.formDisabled) {
      this.form.disable();
    } else {
      this.insuranceValueChange();
      this.initInsuranceValue(this.formData['insurance']);
    }

  }

  private buildForm () {
    this.form.addControl('insurance', new FormControl(this.formData['insurance']));
    this.form.addControl('insurance_price', new FormControl(this.formData['insurance_price']));
    this.form.addControl('insurance_currency', new FormControl(this.formData['insurance_currency']));
  }

  private insuranceValueChange () {
      this.form.get('insurance').valueChanges.subscribe( (val) => {
        this.initInsuranceValue(val);
      });
  }

  private initInsuranceValue (val) {
    if (+val) {
      this.readOnlyInputs = false;
    } else {
      this.form.get('insurance_price').setValue('');
      this.form.get('insurance_currency').setValue('');
      this.readOnlyInputs = true;
    }
  }

}
