import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TariffsTransportersComponent } from './tariffs-transporters.component';

// import { PipeModule } from '../../../shared-cabinet/pipes/pipe.module';

@NgModule({
  imports: [
    CommonModule,
    // PipeModule
  ],
  exports: [
    TariffsTransportersComponent
  ],
  declarations: [
    TariffsTransportersComponent
  ],
})

export class TariffsTransportersModule {

}
