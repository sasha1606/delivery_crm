import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountingComponent } from './accounting.component';
import { AdminUsersRoutingModule } from './accounting-routing.module';

import { PaginationModule } from '@shared/pagination/pagination.module';
import { SearchTableModule } from '@shared/search-table/search-table.module';

import { HelpersService } from '@shared/services/helpers.service';

import { AccountingService } from './accounting.service';
import { AccountingResolve } from './accounting.resolve';
import { FormsModule } from '@angular/forms';

@NgModule ({
    imports: [
        CommonModule,
        AdminUsersRoutingModule,
        PaginationModule,
        SearchTableModule,
        FormsModule,
    ],
    declarations: [
        AccountingComponent
    ],
    providers: [
        AccountingService,
        AccountingResolve,
        HelpersService,
    ]
})

export class AccountingModule {

}
