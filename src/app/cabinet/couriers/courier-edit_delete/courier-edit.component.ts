import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { maskPhone } from '@shared/const/masks';

@Component ({
    selector: 'app-courier-edit',
    templateUrl: './courier-edit.component.html',
    styleUrls: ['./courier-edit.component.css']
})

export class CourierEditComponent implements OnInit {
    title = 'Редактирование курьера';
    form: FormGroup;
    userId: number;

    public maskPhone =  maskPhone;

    constructor (
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private fb: FormBuilder,
    ) {}

    ngOnInit () {
        this.buildForm();

        this.activatedRoute.params.subscribe(param => {
            this.userId = +param['id'];
        });

        if (isNaN(this.userId)) {
            this.router.navigate(['/cabinet/customers-users']);
        } else {

        }
    }

    buildForm () {
        this.form = this.fb.group({
            personalCode: ['', [
                Validators.required,
            ]],
            fio: ['', [
                Validators.required,
            ]],
            email: ['', [
                Validators.required,
            ]],
            tel: ['', [
                Validators.required,
            ]],
            birthday: ['', [
                Validators.required,
            ]],
            country: ['', [
                Validators.required,
            ]],
            city: ['', [
                Validators.required,
            ]],
            zipCode: ['', [
                Validators.required,
            ]],
            address: ['', [
                Validators.required,
            ]],
            house: ['', [
                Validators.required,
            ]],
            housing: ['', [
                Validators.required,
            ]],
            flat: ['', [
                Validators.required,
            ]],
            comment: ['', [
                Validators.required,
            ]],
            file: ['', [
                Validators.required,
            ]],
        });
    }

    onSubmit (form) {
        console.log(this.form.valid);
        if (this.form.valid) {
            console.log('form valid', this.form.valid);
        } else {
            Object.keys(this.form.controls).forEach(field => {
                const control = this.form.get(field);
                control.markAsTouched({ onlySelf: true });
                control.markAsDirty({onlySelf: true});
            });

        }
        console.log(form);
    }
}
