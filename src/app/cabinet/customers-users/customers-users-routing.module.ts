import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomersUsersComponent } from './customers-users.component';
import { CustomerUserCreateComponent } from './customer-user-create/customer-user-create.component';
import { CustomerUserShowComponent } from './customer-user-show/customer-user-show.component';
import { CustomerUserEditComponent } from './customer-user-edit/customer-user-edit.component';

import { CustomerUserCreateResolve } from './customer-user-create/customer-user-create.resolve';
import { CustomerUserShowResolve } from './customer-user-show/customer-user-show.resolve';
import { CustomerUserResolve } from './customer-user.resolve';
import { CustomerUserEditResolve } from './customer-user-edit/customer-user-edit.resolve';


const ROUTERS: Routes = [
    {
        path: '',
        component: CustomersUsersComponent,
        resolve: {
            users: CustomerUserResolve,
        }
    },
    {
        path: 'create',
        component: CustomerUserCreateComponent,
        resolve: {
            data: CustomerUserCreateResolve,
        },
    },
    {
        path: 'show/:id',
        component: CustomerUserShowComponent,
        resolve: {
            user: CustomerUserShowResolve,
        },
    },
    {
        path: 'edit/:id',
        component: CustomerUserEditComponent,
        resolve: {
            user: CustomerUserEditResolve,
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class CustomersUsersRoutingModule {}
