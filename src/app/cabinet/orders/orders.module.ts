import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';

import { OrdersRoutingModule } from './orders-routing.module';

import { OrdersComponent } from './orders.component';
import { OrderCreateComponent } from './order-create/order-create.component';
import { OrderShowComponent } from './order-show/order-show.component';
import { OrderEditComponent } from './order-edit/order-edit.component';

import { OrdersService } from './orders.service';
import { HelpersService } from '@shared/services/helpers.service';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';

import { OrderShowResolve } from './order-show/order-show.resolve';
import { OrderCreateResolve } from './order-create/order-create.resolve';
import { OrderEditResolve } from './order-edit/order-edit.resolve';
import { OrdersResolve } from './orders.resolve';

import { PipeModule } from '@shared/pipes/pipe.module';
import { PaginationModule } from '@shared/pagination/pagination.module';
import { SearchTableModule } from '@shared/search-table/search-table.module';
import { FormFileUploadModule } from '@shared/form-file-upload/form-file-upload.module';
import { FormAutocompleteModule } from '@shared/form-autocomplete/form-autocomplete.module';
import { DatepikerDirectiveModule } from '@shared/directives/datepiker.directive.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { DatepickerModule } from '@shared/datepicker/datepicker.module';
import { CalculatorModule } from '@shared/calculator/calculator.module';
import { AddDirectionTableModule } from '@shared/add-direction-table/add-direction-table.module';
import { HistoryOrderTableModule } from '@shared/components/history-order-table/history-order-table.module';
import { ParcelFormParamsModule } from '@shared/components/parcel-form-params/parcel-form-params.module';
import { AutocompleteCityModule } from '@shared/components/autocomplete-city/autocomplete-city.module';
import { InsuranceFormElemModule } from '@shared/components/insurance-form-elem/insurance-form-elem.module';

@NgModule ({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    OrdersRoutingModule,
    PipeModule,
    PaginationModule,
    NgSelectModule,
    Ng2AutoCompleteModule,
    SearchTableModule,
    FormAutocompleteModule,
    DatepikerDirectiveModule,
    DatepickerModule,
    FormFileUploadModule,
    TextMaskModule,
    CalculatorModule,
    AddDirectionTableModule,
    HistoryOrderTableModule,
    ParcelFormParamsModule,
    AutocompleteCityModule,
    InsuranceFormElemModule,
  ],
  declarations: [
    OrdersComponent,
    OrderShowComponent,
    OrderCreateComponent,
    OrderEditComponent
  ],
  providers: [
    OrdersService,
    OrderShowResolve,
    OrderCreateResolve,
    OrderEditResolve,
    OrdersResolve,
    HelpersService,
    FindToAutocompleteService,
  ]
})

export class OrdersModule { }
