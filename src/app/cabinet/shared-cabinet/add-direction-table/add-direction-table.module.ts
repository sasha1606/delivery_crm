import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddDirectionTableComponent } from './add-direction-table.component';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    AddDirectionTableComponent,
  ],
  exports: [
    AddDirectionTableComponent,
  ]
})

export class AddDirectionTableModule {

}
