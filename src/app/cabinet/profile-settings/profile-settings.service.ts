import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiSrc } from '@api/api.src';
import { ApiService } from '@api/api.service';
import { Response } from '@shared/interface/response';


import { ProfileSettings, ProfileSettingsGet } from './profile-settings';


@Injectable()

export class ProfileSettingsService {
    id: number;

    constructor (
        private apiService: ApiService
    ) {}

    getUser (): Observable<ProfileSettingsGet>  {
       return this.apiService.get(ApiSrc.profile.user);
    }

    updateUser ( user: ProfileSettings): Observable<Response> {
        return this.apiService.post(ApiSrc.profile.user, user);
    }
}
