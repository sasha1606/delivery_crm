import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiSrc } from '@api/api.src';
import { ApiService } from '@api/api.service';

import { QueriesPriceGet, QueriesPriceListGet, QueriesPrice } from './queries-price';
import { Response } from '@shared/interface/response';

@Injectable()

export class QueriesPriceService {

  constructor (
      private apiService: ApiService
  ) {}

  public getQueriesPriceList (queryParams = {}): Observable<QueriesPriceListGet>  {
    return this.apiService.get( ApiSrc.queriesPrice.list, queryParams);
  }

  public search ({sort = '', key = '', page = 1, date = '', manager}): Observable<QueriesPriceListGet> {
    // for IE
    const  keyword = encodeURIComponent(key);
    return this.apiService.get(`${ApiSrc.queriesPrice.list}?sort=${sort}&keyword=${keyword}&page=${page}&date=${date}&manager=${manager}`);
  }


  public getQueriesPrice (id: number): Observable<QueriesPriceGet> {
    return this.apiService.get(`${ApiSrc.queriesPrice.show}/${id}`);
  }

  public dataQueriesPrice (): Observable<QueriesPriceGet> {
    return this.apiService.get(ApiSrc.queriesPrice.create);
  }

  public createQueriesPrice (queriesPrice: QueriesPrice): Observable<Response> {
    return this.apiService.post(ApiSrc.queriesPrice.create, queriesPrice);
  }

  public updateQueriesPrice ({queriesPrice, queriesPriceId}): Observable<Response> {
    return this.apiService.post(`${ApiSrc.queriesPrice.edit}/${queriesPriceId}`, queriesPrice);
  }

  public sendAgainRequest (queryId: number) {
    return this.apiService.get(`${ApiSrc.queriesPrice.sendpricerequest}/${queryId}`);
  }

}
