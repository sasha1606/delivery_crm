import { Component, Input, OnChanges, SimpleChanges, DoCheck, EventEmitter, Output, OnInit } from '@angular/core';
import { DatepickerOptions } from 'ng2-datepicker';
import { FormControl } from '@angular/forms';

@Component ({
    selector: 'app-datepicker',
    templateUrl: './datepicker.component.html',
    styleUrls: ['./datepicker.component.css'],
    // inputs: ['date']
})

export class DatepickerComponent implements OnInit {

    // [new Date(), new Date()];
    // TODO: remove variable date
    @Input() date;
    @Input() form?;
    @Input() formDisabled?;
    @Input() formElemValue?;
    @Input() formElemName?;
    @Input() placeholder = 'Выберете дату';
    @Input() datepikerMode = 'range'; //single
    @Input('minDate') set minDate(date) {
      if (date) {
        this.minDateForTimePiker = date;
      }
    }

    @Output()
    public sendDate = new EventEmitter();

    public minDateForTimePiker = '';

    // public curentDate;
    // public options: DatepickerOptions = {
    //       minYear: 1970,
    //       maxYear: 2030,
    //       displayFormat: 'MM[/]DD[/]YYYY',
    //       barTitleFormat: 'MM[/]YYYY',
    //       dayNamesFormat: 'dd',
    //       firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    //       minDate: new Date(Date.now()), // Minimal selectable date
    //       // maxDate: new Date(Date.now()),  // Maximal selectable date
    //       // barTitleIfEmpty: 'Click to select a datesaasd'
    //   };


    // private _date;

    ngOnInit () {
      this.buildForm();
      this.setDate();
    }

    public onChangeDate (date) {
        let doubleDate;
        if (date[0]) {
            doubleDate = `${+date[0] / 1000} ~ ${+date[1] / 1000}`;
        } else {
            doubleDate = '';
        }
        this.sendDate.emit(doubleDate);
    }

    private buildForm() {
      if (this.form) {
        this.form.addControl(this.formElemName, new FormControl(this.date));
        if (this.formDisabled) {
          // this.form.disable();
          this.form.get(this.formElemName).disable();
        }
      }
    }

    private setDate() {
      if (this.formElemValue) {
        this.date = new Date(this.formElemValue * 1000);
      }
    }

    // set date (value) {
    //     this._date = value;
    // }
    //
    // get date () {
    //     return this._date;
    // }
}
