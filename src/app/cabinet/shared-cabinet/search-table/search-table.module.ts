import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SearchTableComponent } from './search-table.component';

import { DatepickerModule } from '../datepicker/datepicker.module';
import { PipeModule } from '@shared/pipes/pipe.module';

@NgModule ({
  imports: [
    CommonModule,
    DatepickerModule,
    FormsModule,
    PipeModule,
  ],
  exports: [
    SearchTableComponent
  ],
  declarations: [
    SearchTableComponent
  ]
})

export class SearchTableModule {

}
