import {Component, Input, OnInit, ViewChild} from '@angular/core';

@Component ({
    selector: 'app-dashboard-orders',
    templateUrl: './dashboard-orders.component.html',
    styleUrls: ['./dashboard-orders.component.css'],
})

export class DashboardOrdersComponent implements OnInit {

    @Input() monthOrders;
    @Input() yearOrders;

    public isYearActive = true;

    public lineChartLabels: Array<any> = [];
    public lineChartData: Array<any> = [];

    public lineChartOptions: any = {
        responsive: true,
    };

    public lineChartColors: Array<any> = [
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }
    ];

    public lineChartLegend = false;

    public lineChartType = 'line';

    ngOnInit () {
      this.orderYear();
    }

    public orderYear () {
      this.isYearActive = true;
      // this.lineChartData = [ {data: [65, 59, 80, 81, 56, 55, 40], label: 'Year'} ];
      // const data = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
      // this.addNewLabelsToChart(data)
      this.lineChartData = this.yearOrders.data;
      this.addNewLabelsToChart(this.yearOrders.line);
    }

    public orderMonth () {
      this.isYearActive = false;
      this.lineChartData = this.monthOrders.data;
      this.addNewLabelsToChart(this.monthOrders.line);
    }

    private addNewLabelsToChart (labels) {
      this.lineChartLabels.length = 0;
      for (let i = 0; labels.length > i; i++) {
        this.lineChartLabels.push(labels[i]);
      }
    }
}
