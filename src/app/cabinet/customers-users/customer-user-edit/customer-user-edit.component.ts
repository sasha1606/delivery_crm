import {Component, OnInit, OnDestroy, ViewContainerRef, ViewChild, QueryList} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { CustomersUsersService } from '../customers-users.service';

import { FromValidation } from '@fromValidation';

import { CostumerUserGet } from '../costumer-user';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';
import { AddDirectionTableComponent } from '@shared/add-direction-table/add-direction-table.component';

import { maskPhone } from '@shared/const/masks';
import { ToastsManager } from 'ng2-toastr';
import {debounceTime, switchMap, tap} from 'rxjs/operators';
import { AutocompleteCity } from '@shared/interface/autocomplete-city';

@Component ({
    selector: 'app-customer-user-edit',
    templateUrl: './customer-user-edit.component.html',
    styleUrls: ['./customer-user-edit.component.css']
})

export class CustomerUserEditComponent extends FromValidation implements OnInit, OnDestroy {

    @ViewChild(AddDirectionTableComponent) addDirectionTableComponent;

    public title = 'Pедактирование пользователя';
    public form: FormGroup;

    public user: CostumerUserGet;
    public userId: any;
    public local_from = [];

    public maskPhone =  maskPhone;

    // public  autocompleteLoading = false;
    // private autocompleteSubject$ = new Subject();
    public autocompleteCityFormComponent: AutocompleteCity;

    private destroy$: Subject<boolean> = new Subject<boolean>();

    constructor (
        public customersUsersService: CustomersUsersService,
        public router: Router,
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder,
        private findToAutocompleteService: FindToAutocompleteService,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef,
    ) {
        super(
            router,
            customersUsersService,
        );
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit () {
        this.user = this.activatedRoute.snapshot.data['user'];

        this.buildForm();
        this.activatedRoute.params.subscribe(param => {
            this.userId = +param['id'];
        });

        this.buildAutocompleteCityFormComponent();

        // this.autocompleteSubscribe();
    }

    ngOnDestroy () {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

    /*========= LOCATION =========*/
    // private onGetLocation(city) {
    //   this.autocompleteSubject$.next(city);
    // }
    //
    // public onGetLocationFrom(event) {
    //   this.local_from = [];
    //   this.onGetLocation(event.target.value);
    // }
    //
    // public onSelectLocationFrom(event) {
    //   if (!event) {
    //     return false;
    //   }
    //   this.form.get('country').setValue(event.countryName);
    //   this.form.get('region').setValue(event.region);
    //   this.form.get('zip_code').setValue(event.postal_code);
    // }
    /*========= / LOCATION =========*/

    public onSubmit (form): void {
        if (this.form.valid) {
          this.form.value.contract_tariff_data = JSON.stringify(this.addDirectionTableComponent.getInputValue());
          this.customersUsersService.updateUser(this.form.value, this.userId).takeUntil(this.destroy$).subscribe( res => {
                if (res.success === 'ok') {
                    this.toastr.success('Has been edit successfully');
                    setTimeout(() => {
                      this.router.navigateByUrl('/cabinet/customers-users');
                    }, 1000);
                }  else {
                    this.toastr.error('Error');
                    this.validationFormBackEnd(res.validation, this.form);
                }
            });

        } else {
            this.toastr.error('Error');
            this.validationFormsSubmit(this.form);
        }
    }

    private buildForm (): void {
      this.form = this.fb.group({
        type: [this.user.data.type, [
          Validators.required,
        ]],
        status: [this.user.data.status, [
          Validators.required,
        ]],
        login: [this.user.data.login, [
          Validators.required,
        ]],
        fio: [this.user.data.fio, [
          Validators.required,
        ]],
        email: [this.user.data.email, [
          Validators.required,
        ]],
        phone: [this.user.data.phone, [
          Validators.required,
        ]],
        company: [this.user.data.company],
        company_code: [this.user.data.company_code, ],
        // country: [this.user.data.country],
        // city: [this.user.data.city],
        // address: [this.user.data.address],
        // postal_address: [this.user.data.postal_address],
        // region: [this.user.data.region],
        zip_code: [this.user.data.zip_code],
        discount: [this.user.data.discount],
        contract_tariff: [this.user.data.contract_tariff],
        email2: [this.user.data.email2],
        mail_for_send: [this.user.data.mail_for_send],
        phone2: [this.user.data.phone2],
        user_id: [this.user.data.user_id],
        contract_tariff_data: '',
        filesUpload: '',
        deleteFiles: '',
      });
    }

    // private autocompleteSubscribe () {
    //   this.autocompleteSubject$.pipe(
    //     tap(() => this.autocompleteLoading = true),
    //     debounceTime(400),
    //     switchMap( (city) => {
    //       return this.findToAutocompleteService.findLocation(city);
    //     })
    //   ).subscribe(res => {
    //     this.local_from = res.data;
    //     this.autocompleteLoading = false;
    //   });
    // }

    private buildAutocompleteCityFormComponent () {
      this.autocompleteCityFormComponent = {
        searchSelectAutocomplete: {
          formControlName: 'city',
          label: 'Город',
          value: this.user.data.city,
        },
        searchInfoAutocomplete: {
          country: {
            formControlName: 'country',
            label: 'Страна',
            value: this.user.data.country,
            searchEvent: 'countryName',
          },
          address: {
            formControlName: 'address',
            label: 'Адрес',
            value: this.user.data.address,
            searchEvent: null,
          },
          postal_address: {
            formControlName: 'postal_address',
            label: 'Почтовый адрес',
            value: this.user.data.postal_address,
            searchEvent: null,
          },
          region: {
            formControlName: 'region',
            label: 'Район/Штат',
            value: this.user.data.region,
            searchEvent: 'region',
          },
          zip_code: {
            formControlName: 'zip_code',
            label: 'Индекс',
            value: this.user.data.zip_code,
            searchEvent: 'postal_code',
          }
        }
      }
    }
}
