import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StorageDeal } from './storage-deal';

@Component({
  selector: 'app-storage-deal',
  templateUrl: './storage-deal.component.html',
  styleUrls: ['./storage-deal.component.css']
})

export class StorageDealComponent implements OnInit {

  title = 'Складские услуги';
  storageDeal: StorageDeal;

  constructor (
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit () {
    this.storageDeal = this.activatedRoute.snapshot.data['storage'];
  }

}
