import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { QueriesPriceService } from '../queries-price.service';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { QueriesPriceGet } from '../queries-price';

import { FromValidation } from '@fromValidation';

import { typePage } from '@shared/const/crud';
import { AutocompleteService } from '@shared/services/autocomplete.service';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';
import { ToastsManager } from 'ng2-toastr';
import { ValidateOnlyNumber } from '@shared/formValidators/validateOnlyNumber';
import { AutocompleteCity } from '@shared/interface/autocomplete-city';

@Component ({
    selector: 'app-query-price-edit-create',
    templateUrl: './query-price-edit-create.component.html',
    styleUrls: ['./query-price-edit-create.component.css'],
})

export class QueryPriceEditCreateComponent extends FromValidation implements OnInit {
    public title = 'Ценновой запрос';

    private destroy$: Subject<boolean> = new Subject<boolean>();

    public form: FormGroup;
    public queriesPriceId: number;

    public transporters;
    public directions;

    public typePage: string;

    public countItemsParams = new Array(4);

    // public titleCity_from = '';
    // public titleCountry_from = '';
    // public postalCode_from;
    // public titleRegion_from;
    public local_from = [];
    // public localMain_from = [];

    // public titleCity_to = '';
    // public titleCountry_to = '';
    // public postalCode_to;
    // public titleRegion_to;
    public local_to = [];
    // public localMain_to = [];
    public test1;

  public autocompleteCityFromFormComponent: AutocompleteCity;
  public autocompleteCityToFormComponent: AutocompleteCity;


  public fromNames = {
    type: {
      name: 'transporter_type',
      value: '',
    },
    id: {
      name: 'transporter_id',
      value: '',
    },
    direction_id: {
      name: 'transporter_direction_id',
      value: '',
    },
    direction_name: {
      name: 'transporter_direction_name',
      value: '',
    },
    direction_phone: {
      name: 'transporter_direction_phone',
      value: '',
    },
    direction_email: {
      name: 'transporter_direction_email',
      value: '',
    },
    direction_resperson: {
      name: 'transporter_direction_resperson',
      value: '',
    },
  };

  constructor (
    public queriesPriceService: QueriesPriceService,
    public router: Router,
    private autocompleteService: AutocompleteService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private findToAutocompleteService: FindToAutocompleteService,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
  ) {
    super(
      router,
      queriesPriceService,
    );
    this.toastr.setRootViewContainerRef(vcr);
  }

    queriesPrice: QueriesPriceGet;

    ngOnInit () {
        this.queriesPrice = this.activatedRoute.snapshot.data['queriesPrice'];

        this.typePage = this.activatedRoute.snapshot.data['data'];

        this.activatedRoute.params.subscribe( params => {
            this.queriesPriceId = +params['id'];
        });
        this.buildForm();
        // this.subscribeItemsParams();
        // this.subscribeType();
        this.setValueAutocomplete();
        // this.changeModalSelect();

        this.buildAutocompleteCityFromFormComponent();
        this.buildAutocompleteCityToFormComponent();
    }

    public onSubmit (form): void {
      if (this.form.valid) {
        let answer;
        if (this.typePage === typePage.edit) {
          answer = this.queriesPriceService.updateQueriesPrice({
            queriesPrice: this.form.value,
            queriesPriceId: this.queriesPriceId
          })
            .takeUntil(this.destroy$);
        } else if (this.typePage === typePage.create) {
          answer =  this.queriesPriceService.createQueriesPrice(this.form.value).takeUntil(this.destroy$);
        }

        answer.subscribe( res => {
          if (res.success === 'ok') {

            this.toastr.success('Successfully');
            setTimeout(() => {
              this.router.navigateByUrl('/cabinet/queries-price');
            }, 1000);
          }  else {
            this.toastr.error('Error');
            this.validationFormBackEnd(res.validation, this.form);
          }
        });
      } else {
        this.toastr.error('Error');
        this.validationFormsSubmit(this.form);
      }
    }

  /*========= LOCATION =========*/
  //
  //
  //
  // public isLiction;
  // onGetLocation(city) {
  //   this.isLiction = this.findToAutocompleteService.findLocation(city)
  //     .subscribe(res => {
  //       this.local_from = res.data;
  //       this.local_to = res.data;
  //       this.isLiction.unsubscribe();
  //     });
  // }
  //
  // onGetLocationFrom(event) {
  //   this.local_from = [];
  //   const test2 = event.target.value;
  //   this.onGetLocation(test2);
  // }
  //
  // onSelectLocationFrom(event) {
  //   // this.titleCountry_from = event.countryName;
  //   // this.postalCode_from = event.postal_code;
  //   // this.titleRegion_from = event.region;
  //   if (!event) {
  //     return false;
  //   }
  //   this.form.get('country_from').setValue(event.countryName);
  //   this.form.get('zip_code_from').setValue(event.postal_code);
  //   this.form.get('region_from').setValue(event.region);
  // }
  //
  //
  // onGetLocationTo(event) {
  //   this.local_to = [];
  //   const test2 = event.target.value || this.test1;
  //   this.onGetLocation(test2);
  // }
  // onSelectLocationTo(event) {
  //   // this.titleCountry_to = event.countryName;
  //   // this.postalCode_to = event.postal_code;
  //   // this.titleRegion_to = event.region;
  //   if (!event) {
  //     return false;
  //   }
  //   this.form.get('country_to').setValue(event.countryName);
  //   this.form.get('zip_code_to').setValue(event.postal_code);
  //   this.form.get('region_to').setValue(event.region);
  // }

  /*========= / LOCATION =========*/


    private buildForm () {
      this.form = this.fb.group({
        // transporter_type: this.queriesPrice.data.transporter_type,
        // transporter_id: this.queriesPrice.data.transporter_id,
        // transporter_direction_id: this.queriesPrice.data.transporter_direction_id,
        // transporter_direction_name: this.queriesPrice.data.transporter_direction_name,
        // transporter_direction_email: this.queriesPrice.data.transporter_direction_email,
        // transporter_direction_resperson: this.queriesPrice.data.transporter_direction_resperson,
        // transporter_direction_phone: this.queriesPrice.data.transporter_direction_phone,
        // city_from: this.queriesPrice.data.city_from,
        // country_from: this.queriesPrice.data.city_from,
        // region_from: this.queriesPrice.data.region_from,
        // zip_code_from: this.queriesPrice.data.zip_code_from,
        type: this.queriesPrice.data.type,
        // insurance: this.queriesPrice.data.insurance,
        items_price: this.queriesPrice.data.items_price,
        // city_to: this.queriesPrice.data.city_to,
        // country_to: this.queriesPrice.data.country_to,
        // region_to: this.queriesPrice.data.region_to,
        // zip_code_to: this.queriesPrice.data.zip_code_to,
        // insurance_currency: this.queriesPrice.data.insurance_currency,
        // insurance_price: this.queriesPrice.data.insurance_price,
        items_count: this.queriesPrice.data.items_count,
        items_totalweight: this.queriesPrice.data.items_totalweight,
        estimated_weight: '',
        // itemsParams: this.fb.group({
        //   client_comment_1: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.client_comment_1 : '',
        //   items_type_1: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_type_1 : '',
        //   delivery_type_1: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.delivery_type_1 : '',
        //   items_count_1: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_count_1 : '', ValidateOnlyNumber],
        //   items_weight_1: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_weight_1 : '', ValidateOnlyNumber],
        //   items_length_1: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_length_1 : '', ValidateOnlyNumber],
        //   items_width_1: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_width_1 : '', ValidateOnlyNumber],
        //   items_height_1: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_height_1 : '', ValidateOnlyNumber],
        //   items_price_1: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_1 : '', ValidateOnlyNumber],
        //   items_price_currency_1: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_currency_1 : '',
        //
        //   client_comment_2: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.client_comment_2 : '',
        //   items_type_2: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_type_2 : '',
        //   delivery_type_2: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.delivery_type_2 : '',
        //   items_count_2: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_count_2 : '', ValidateOnlyNumber],
        //   items_weight_2: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_weight_2 : '', ValidateOnlyNumber],
        //   items_length_2: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_length_2 : '', ValidateOnlyNumber],
        //   items_width_2: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_width_2 : '', ValidateOnlyNumber],
        //   items_height_2: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_height_2 : '', ValidateOnlyNumber],
        //   items_price_2: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_2 : '', ValidateOnlyNumber],
        //   items_price_currency_2: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_currency_2 : '',
        //
        //   client_comment_3: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.client_comment_3 : '',
        //   items_type_3: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_type_3 : '',
        //   delivery_type_3: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.delivery_type_3 : '',
        //   items_count_3: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_count_3 : '', ValidateOnlyNumber],
        //   items_weight_3: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_weight_3 : '', ValidateOnlyNumber],
        //   items_length_3: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_length_3 : '', ValidateOnlyNumber],
        //   items_width_3: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_width_3 : '', ValidateOnlyNumber],
        //   items_height_3: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_height_3 : '', ValidateOnlyNumber],
        //   items_price_3: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_3 : '', ValidateOnlyNumber],
        //   items_price_currency_3: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_currency_3 : '',
        //
        //   client_comment_4: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.client_comment_4 : '',
        //   items_type_4: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_type_4 : '',
        //   delivery_type_4: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.delivery_type_4 : '',
        //   items_count_4: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_count_4 : '', ValidateOnlyNumber],
        //   items_weight_4: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_weight_4 : '', ValidateOnlyNumber],
        //   items_length_4: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_length_4 : '', ValidateOnlyNumber],
        //   items_width_4: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_width_4 : '', ValidateOnlyNumber],
        //   items_height_4: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_height_4 : '', ValidateOnlyNumber],
        //   items_price_4: [ this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_4 : '', ValidateOnlyNumber],
        //   items_price_currency_4: this.queriesPrice.data.itemsParams ? this.queriesPrice.data.itemsParams.items_price_currency_4 : '',
        // }),
        items_type: this.queriesPrice.data.items_type,
        delivery_price: this.queriesPrice.data.delivery_price,
        client_comment: this.queriesPrice.data.client_comment,
        filesUpload: '',
        deleteFiles: '',
      });
    }

  // private changeModalSelect () {
  //
  //   this.form.get('transporter_type').valueChanges.subscribe(type => {
  //     if (!type) {
  //       return;
  //     }
  //
  //     this.form.get('transporter_id').setValue(null);
  //     this.form.get('transporter_direction_name').setValue(null);
  //     this.form.get('transporter_direction_phone').setValue(null);
  //     this.form.get('transporter_direction_email').setValue(null);
  //     this.form.get('transporter_direction_resperson').setValue(null);
  //     this.directions = null;
  //     this.transporters = null;
  //
  //     this.autocompleteService.changetransportersType(type).takeUntil(this.destroy$).subscribe( res => {
  //       if (res.success === 'ok') {
  //         console.log(res);
  //         this.transporters = res;
  //       }
  //     });
  //   });
  //
  //   this.form.get('transporter_id').valueChanges.subscribe(id => {
  //     if (!id) {
  //       return;
  //     }
  //     this.form.get('transporter_direction_id').setValue(null);
  //     this.form.get('transporter_direction_name').setValue(null);
  //     this.form.get('transporter_direction_phone').setValue(null);
  //     this.form.get('transporter_direction_email').setValue(null);
  //     this.form.get('transporter_direction_resperson').setValue(null);
  //     this.directions = null;
  //
  //     this.autocompleteService.changeTransporterId({
  //       transportersType: this.form.get('transporter_type').value,
  //       transporterId: id,
  //     })
  //       .takeUntil(this.destroy$).subscribe( res => {
  //       if (res.success === 'ok') {
  //         this.directions = res;
  //       }
  //     });
  //   });
  //
  //   this.form.get('transporter_direction_id').valueChanges.subscribe(id => {
  //     if (!id) {
  //       return;
  //     }
  //
  //     this.autocompleteService.changeDirections({
  //       transportersType: this.form.get('transporter_type').value,
  //       transporterId: this.form.get('transporter_id').value,
  //       directionId: id,
  //     })
  //       .takeUntil(this.destroy$).subscribe( res => {
  //       this.form.get('transporter_direction_name').setValue(res.directioninfo.name);
  //       this.form.get('transporter_direction_phone').setValue(res.directioninfo.phone);
  //       this.form.get('transporter_direction_email').setValue(res.directioninfo.email);
  //       this.form.get('transporter_direction_resperson').setValue(res.directioninfo.responsible_person);
  //     });
  //   });
  // }

  setValueAutocomplete () {
    for (const item of Object.keys(this.fromNames)) {
      this.fromNames[item].value = this.queriesPrice.data[this.fromNames[item].name];
    }
  }

  // private subscribeItemsParams () {
  //   this.form.get('itemsParams').valueChanges.subscribe( (response) => {
  //    this.calcEstimatedWeight();
  //   });
  // }
  //
  // private subscribeType () {
  //   this.form.get('type').valueChanges.subscribe( () => {
  //     this.calcEstimatedWeight();
  //   });
  // }

  // private calcEstimatedWeight () {
  //   const data = this.form.get('itemsParams').value;
  //   let divider = 5000;
  //   if (this.form.get('type').value.includes('air')) {
  //     divider = 6000;
  //   }
  //   let tootleEstimatedWeigh: any = 0;
  //   for (let i = 1; i <= this.countItemsParams.length; i++) {
  //     if (isNaN(tootleEstimatedWeigh)) {
  //       tootleEstimatedWeigh = 0;
  //       break;
  //     }
  //     if (this.form.get('type').value === '6_road_freight') {
  //       // for (let i = 1; i <= this.countItemsParams.length; i++) {
  //       //   if (isNaN(tootleEstimatedWeigh)) {
  //       //     tootleEstimatedWeigh = 0;
  //       //     break;
  //       //   }
  //         tootleEstimatedWeigh += (+data[`items_length_${i}`] / 100) * (+data[`items_width_${i}`] / 100) * (+data[`items_height_${i}`] / 100) * 333 * +data[`items_price_${i}`];
  //         tootleEstimatedWeigh = Number(Number(tootleEstimatedWeigh).toFixed(2).replace(/\.?0+$/,''));
  //       // }
  //     } else {
  //       // for (let i = 1; i <= this.countItemsParams.length; i++) {
  //         // if (isNaN(tootleEstimatedWeigh)) {
  //         //   tootleEstimatedWeigh = 0;
  //         //   break;
  //         // }
  //         if (+data[`items_price_${i}`] && +data[`items_price_${i}`] > 0) {
  //           tootleEstimatedWeigh += (+data[`items_length_${i}`] * +data[`items_width_${i}`] * +data[`items_height_${i}`] / divider) * +data[`items_price_${i}`];
  //           tootleEstimatedWeigh = Number(Number(tootleEstimatedWeigh).toFixed(2).replace(/\.?0+$/,''));
  //         }
  //       // }
  //     }
  //   }
  //   this.form.get('estimated_weight').setValue(tootleEstimatedWeigh.toFixed(2));
  // }

  private buildAutocompleteCityToFormComponent () {
    this.autocompleteCityToFormComponent = {
      searchSelectAutocomplete: {
        formControlName: 'city_to',
        label: 'Город получателя',
        value: this.queriesPrice.data.city_to,
      },
      searchInfoAutocomplete: {
        country: {
          formControlName: 'country_to',
          label: 'Страна получателя',
          value: this.queriesPrice.data.country_to,
          searchEvent: 'countryName',
        },
        region: {
          formControlName: 'region_to',
          label: 'Район/Штат получателя',
          value: this.queriesPrice.data.region_to,
          searchEvent: 'region',
        },
        zip_code: {
          formControlName: 'zip_code_to',
          label: 'Индекс получателя',
          value: this.queriesPrice.data.zip_code_to,
          searchEvent: 'postal_code',
        }
      }
    }
  }

  private buildAutocompleteCityFromFormComponent () {
    this.autocompleteCityFromFormComponent = {
      searchSelectAutocomplete: {
        formControlName: 'city_from',
        label: 'Город отправителя',
        value: this.queriesPrice.data.city_from,
      },
      searchInfoAutocomplete: {
        country: {
          formControlName: 'country_from',
          label: 'Страна отправителя',
          value: this.queriesPrice.data.country_from,
          searchEvent: 'countryName',
        },
        region: {
          formControlName: 'region_from',
          label: 'Район/Штат отправителя',
          value: this.queriesPrice.data.region_from,
          searchEvent: 'region',
        },
        zip_code: {
          formControlName: 'zip_code_from',
          label: 'Индекс отправителя',
          value: this.queriesPrice.data.zip_code_from,
          searchEvent: 'postal_code',
        }
      }
    }
  }

}
