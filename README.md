# Installation

1) git clone https://sasha1606@bitbucket.org/sasha1606/delivery_crm.git

2) npm install

3) ng serve --port 8080 --host 0.0.0.0


# Local server

1) ng server


# Code Style and fix

1) ng lint

2) ng lint --fix


# Build to prod

1) ng build --prod

2) ng build --prod --env=prod  (environments)

3) with prefix if need fix style place --extract-css=false (css)


# change directory build to prod

1) ng build -prod --output-path=


# TODO

1) ValueAccessor

2) abstract class for table search submit form

3) customer-user-show.component - tooltip
