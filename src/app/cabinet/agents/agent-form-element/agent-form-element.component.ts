import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { maskPhone } from '@shared/const/masks';

@Component ({
  selector: 'app-agent-form-element-component',
  templateUrl: './agent-form-element.component.html',
  styleUrls: ['./agent-form-element.component.css']
})

export class AgentFormElementComponent implements OnInit {

  @Input() dataForm;
  @Input() form;
  @Input() counter;

  myOptions: IMultiSelectOption[] = [];
  mySettings: IMultiSelectSettings;
  myTexts: IMultiSelectTexts;

  @Output() deleteElem: EventEmitter<any> = new EventEmitter();

  public maskPhone =  maskPhone;

  constructor (
    private fb: FormBuilder,
  ) {}

  ngOnInit () {
    this.setItemToSelect();

    // Settings configuration
    this.mySettings = {
      dynamicTitleMaxItems: 2,
    };

    // Text configuration
    this.myTexts = {
      checked: 'Выбрано',
      checkedPlural: 'Выбрано',
      defaultTitle: 'Выберете',
    };

  }

  public delete (formName) {
    this.deleteElem.emit(formName);
  }

  private fromBuilder () {
    this.form.addControl(`n_${this.counter}`, this.fb.group({
      'name': this.dataForm.name,
      'email': [this.dataForm.email, Validators.required],
      'comment': this.dataForm.comment,
      'phone': [this.dataForm.phone],
      'responsible_person': this.dataForm.responsible_person,
      'typeParams': [this.dataForm.typeParams],
      'id': this.dataForm.id,
    }));
  }

  private setItemToSelect () {
    for (const key of Object.keys(this.dataForm.typeNames)) {
      this.myOptions.push({
        id: key,
        name: this.dataForm.typeNames[key],
      });
    }
    this.fromBuilder();
  }

}
