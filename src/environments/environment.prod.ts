export const environment = {
  production: true,
  apiHost: '/api/crm',
  origin: window.location.origin,
};
