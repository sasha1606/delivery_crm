import { Pagination } from '@shared/interface/pagination';

export interface CouriersGet {
  data: Courier;
  success: string;
  validation?: object;
}

export interface Courier {
  address: string;
  birthday: string;
  city: string;
  code: string;
  comment: string;
  corps: string;
  phone: string;
  country: string;
  email: string;
  filesList: object;
  fio: string;
  house: object;
  room: string;
  zip_code: string;
  filesUpload: object[];
}

export interface CourierList {
  listdata: ShortCourier[];
  pagination: Pagination;
  success: string;
}

interface ShortCourier {
  code: number;
  email: string;
  fio: string;
  id: string;
  phone: string;
}

export const crud = {
    show: 'show',
    edit: 'edit',
    create: 'create',
};
