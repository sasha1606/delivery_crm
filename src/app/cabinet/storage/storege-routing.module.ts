import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StorageComponent } from './storage.component';
import { StorageEditComponent } from './storage-edit/storage-edit.component';
import { StorageShowComponent } from './storage-show/storage-show.component';

import { StorageShowResolve } from './storage-show/storage-show.resolve';
import { StorageEditResolve } from './storage-edit/storage-edit.resolve';
import { StorageResolve } from './storage.resolve';

const ROUTERS: Routes = [
    {
        path: '',
        component: StorageComponent,
        resolve: {
          storageList: StorageResolve,
        }
    },
    {
      path: 'show/:id',
      component: StorageShowComponent,
      resolve: {
        storage: StorageShowResolve,
      }
    },
    {
      path: 'edit/:id',
      component: StorageEditComponent,
      resolve: {
        storage: StorageEditResolve,
      }
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTERS)],
    exports: [RouterModule]
})

export class StorageRoutingModule {}
