import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { CustomersUsersService } from '../customers-users.service';

@Injectable()

export class CustomerUserCreateResolve implements Resolve<any> {

    constructor (
        private customersUsersService: CustomersUsersService,
    ) {}

    resolve () {
        return this.customersUsersService.dataCreateUser();
    }
}
