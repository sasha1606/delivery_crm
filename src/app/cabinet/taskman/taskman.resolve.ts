import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { TaskmanService } from './taskman.service';

@Injectable()

export class TaskmanResolve implements Resolve<any> {

  constructor (
    private taskmanService: TaskmanService
  ) {}

  resolve () {
    return this.taskmanService.getTasks();
  }

}
