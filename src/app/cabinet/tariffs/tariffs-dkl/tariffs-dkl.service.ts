import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
// import { forkJoin } from 'rxjs/observable/forkJoin';

import { ApiSrc } from '@api/api.src';

import { ApiService } from '@api/api.service';

import { TariffsDkl } from './tariffs-dkl';

@Injectable()

export class TariffsDklService {

  constructor (
    private apiService: ApiService,
  ) {}

  public getTariffsDkl (): Observable<TariffsDkl> {
    return this.apiService.get(ApiSrc.tariffsDkl.tariffs);
  }

}
