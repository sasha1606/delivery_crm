import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SearchTableOrderComponent } from './search-table-order.component';


@NgModule ({
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    SearchTableOrderComponent
  ],
  declarations: [
    SearchTableOrderComponent
  ]
})

export class SearchTableOrderModule {

}
