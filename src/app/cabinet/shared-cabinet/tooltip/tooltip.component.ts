import { Component, Input, DoCheck, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-tooltip',
    templateUrl: './tooltip.component.html',
    styleUrls: ['./tooltip.component.css'],
})

export class TooltipComponent implements OnChanges {

    @Input() showTooltip = false;
    @Input() delay = 2000;
    @Input() urlRedirect = null;


    constructor (
        private router: Router,
    ) {}

    ngOnChanges (changes: SimpleChanges) {
        if (this.showTooltip) {
            this.hideTooltip();
        }
    }

    // ngDoCheck () {
    // }

    private hideTooltip () {
        setTimeout(() => {
            this.showTooltip = false;
            if (this.urlRedirect) {
                this.router.navigateByUrl(this.urlRedirect);
            }
        }, this.delay);
    }
}
