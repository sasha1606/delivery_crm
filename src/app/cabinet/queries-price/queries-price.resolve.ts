import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';

import { QueriesPriceService } from './queries-price.service';

@Injectable()

export class QueriesPriceResolve implements Resolve<any> {

  constructor (
    private queriesPriceService: QueriesPriceService
  ) {}

  resolve (activatedRouteSnapshot: ActivatedRouteSnapshot) {
    const queryParams = activatedRouteSnapshot.queryParams;
    return this.queriesPriceService.getQueriesPriceList(queryParams);
  }
}


