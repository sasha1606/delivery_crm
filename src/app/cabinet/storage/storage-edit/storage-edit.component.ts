import {Component, OnInit, OnDestroy, ViewContainerRef, AfterViewInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormControl, FormBuilder, FormGroup} from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { StorageService } from '../storage.service';
import { HelpersService } from '@shared/services/helpers.service';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';

import { FromValidation } from '@fromValidation';

import { StorageGet } from '../storage';
import { ToastsManager } from 'ng2-toastr';
import { maskPhone, maskTime} from '@shared/const/masks';
import { AutocompleteCity } from '@shared/interface/autocomplete-city';

@Component ({
  selector: 'app-storage-edit',
  templateUrl: './storage-edit.component.html',
  styleUrls: ['./storage-edit.component.css']
})

export class StorageEditComponent extends FromValidation implements OnInit, OnDestroy, AfterViewInit {

  public maskTime = maskTime;
  public maskPhone =  maskPhone;

  public isWhoPay = false;
  public title = 'Редактирование';
  public form: FormGroup;
  public storage: StorageGet;
  public storageId;

  public userInfo_from;
  public userInfo_to;
  public local_to = [];
  public local_from = [];
  public minDateForDateSend;

  public fromNames = {
    type: {
      name: 'transporter_type',
      value: '',
    },
    id: {
      name: 'transporter_id',
      value: '',
    },
    direction_id: {
      name: 'transporter_direction_id',
      value: '',
    },
    direction_name: {
      name: 'transporter_direction_name',
      value: '',
    },
    direction_phone: {
      name: 'transporter_direction_phone',
      value: '',
    },
    direction_email: {
      name: 'transporter_direction_email',
      value: '',
    },
    direction_resperson: {
      name: 'transporter_direction_resperson',
      value: '',
    },
  };

  public autocompleteCityToFormComponent: AutocompleteCity;
  public autocompleteCityFromFormComponent: AutocompleteCity;

  private  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor (
    private fb: FormBuilder,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private activatedRoute: ActivatedRoute,
    private helpersService: HelpersService,
    private findToAutocompleteService: FindToAutocompleteService,
    public router: Router,
    public storageService: StorageService,
  ) {
    super (
      router,
      storageService,
    );
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit () {
    this.activatedRoute.params.subscribe(param => {
      this.storageId = +param['id'];
    });
    this.storage = this.activatedRoute.snapshot.data['storage'];
    this.buildFrom();
    this.changeWhoPay();
    this.checkWhoPaythis();
    this.setValueAutocomplete();
    this.buildAutocompleteCityToFormComponent();
    this.buildAutocompleteCityFromFormComponent();

  }

  ngAfterViewInit () {
    this.changeFormValStockDateCome();
  }

  ngOnDestroy () {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  /*========= LOCATION =========*/

  // public isLiction;
  // onGetLocation(city) {
  //   this.isLiction = this.findToAutocompleteService.findLocation(city)
  //     .subscribe(res => {
  //       this.local_from = res.data;
  //       this.local_to = res.data;
  //       this.isLiction.unsubscribe();
  //     });
  //
  // }
  //
  // onGetLocationFrom(event) {
  //   this.local_from = [];
  //   const test2 = event.target.value;
  //   this.onGetLocation(test2);
  // }
  //
  // onSelectLocationFrom(event) {
  //   if (!event) {
  //     return false;
  //   }
  //   this.form.get('country_from').setValue(c);
  //   this.form.get('zip_code_from').setValue(event.postal_code);
  //   this.form.get('region_from').setValue(event.region);
  // }
  //
  //
  // onGetLocationTo(event) {
  //   this.local_to = [];
  //   const test2 = event.target.value || this.test1;
  //   this.onGetLocation(test2);
  // }
  // onSelectLocationTo(event) {
  //   if (!event) {
  //     return false;
  //   }
  //   this.form.get('country_to').setValue(event.countryName);
  //   this.form.get('zip_code_to').setValue(event.postal_code);
  //   this.form.get('region_to').setValue(event.region);
  // }

  /*========= / LOCATION =========*/


  /*------FindUser----------*/

  onGetUserInfoFrom(event) {
    this.findToAutocompleteService.findUser(event.target.value)
      .subscribe(res => {
        this.userInfo_from = res['data'];
      });
  }

  onSelectUserInfoFrom(event) {
    if (!event) {
      return false;
    }
    this.form.get('name').patchValue(event.fio);
    this.form.get('company').patchValue(event.company);
    this.form.get('email').patchValue(event.email);
    this.form.get('phone').patchValue(event.phone);
  }

  onGetUserInfoTo(event) {
    this.findToAutocompleteService.findUser(event.target.value)
      .subscribe(res => {
        this.userInfo_to = res['data'];
      });
  }

  onSelectUserInfoTo(event) {
    if (!event) {
      return false;
    }
    this.form.get('name_receiver').setValue(event.fio);
    this.form.get('company_receiver').setValue(event.company);
    this.form.get('email_receiver').setValue(event.email);
    this.form.get('phone_receiver').setValue(event.phone);
  }
  /*------FindUser----------*/


  public onSubmit (form): void {
    if (this.form.valid) {
      // TODO: not very good talk with pasha
      // this.form.get('pickup_date').setValue(+this.form.get('pickup_date').value / 1000);
      const dataWithNewFormatDate = this.helpersService.changeFormatDateInForm(this.form.value,
                ['pickup_date', 'invoice_date', 'stock_date_delivery', 'stock_date_send', 'stock_date_come']);

      this.storageService.updateStorages(dataWithNewFormatDate, this.storageId).takeUntil(this.destroy$).subscribe( res => {
        if (res.success === 'ok') {
          this.toastr.success('Has been edit successfully');
          setTimeout(() => {
            this.router.navigateByUrl('/cabinet/storage');
          }, 1000);
        }  else {
          this.toastr.error('Error');
          this.validationFormBackEnd(res.validation, this.form);
        }
      });
    } else {
      this.toastr.error('Error');
      this.validationFormsSubmit(this.form);
    }
  }

  private buildFrom (): void {
    this.form = this.fb.group({
      code_dkl: this.storage.data.code_dkl,
      code_transporter: this.storage.data.code_transporter,
      name: this.storage.data.name,
      courier_id: this.storage.data.courier_id,
      company: this.storage.data.company,
      email: this.storage.data.email,
      phone: this.storage.data.phone,
      // city_from: this.storage.data.city_from,
      // country_from: this.storage.data.country_from,
      // region_from: this.storage.data.region_from,
      // zip_code_from: this.storage.data.zip_code_from,
      name_receiver: this.storage.data.name_receiver,
      company_receiver: this.storage.data.company_receiver,
      email_receiver: this.storage.data.email_receiver,
      phone_receiver: this.storage.data.phone_receiver,
      // city_to: this.storage.data.city_to,
      // country_to: this.storage.data.country_to,
      // region_to: this.storage.data.region_to,
      // zip_code_to: this.storage.data.zip_code_to,
      type: this.storage.data.type,
      // insurance: this.storage.data.insurance,
      // insurance_currency: this.storage.data.insurance_currency,
      // insurance_price: this.storage.data.insurance_price,
      export_cause: this.storage.data.export_cause,
      export_terms: this.storage.data.export_terms,
      stock_pay_type: this.storage.data.stock_pay_type,
      stock_pay: this.storage.data.stock_pay,
      stock_price: this.storage.data.stock_price,
      stock_price_currency: this.storage.data.stock_price_currency,
      stock_comment: this.storage.data.stock_comment,
      stock_delivery_fact_pay: this.storage.data.stock_delivery_fact_pay,
      pickup_timefrom: this.storage.data.pickup_timefrom,
      pickup_timeto: this.storage.data.pickup_timeto,
      client_comment: this.storage.data.client_comment,
      items_count: this.storage.data.items_count,
      items_totalweight: this.storage.data.items_totalweight,
      items_type: this.storage.data.items_type,
      items_price: this.storage.data.items_price,
      items_price_currency: this.storage.data.items_price_currency,
      delivery_type: this.storage.data.delivery_type,
      status: this.storage.data.status,
      thirdside_name: this.storage.data.thirdside_name,
      thirdside_company: this.storage.data.thirdside_company,
      thirdside_email: this.storage.data.thirdside_email,
      thirdside_phone: this.storage.data.thirdside_phone,
      thirdside_city: this.storage.data.thirdside_city,
      thirdside_country: this.storage.data.thirdside_country,
      thirdside_region: this.storage.data.thirdside_region,
      thirdside_zip_code: this.storage.data.thirdside_zip_code,
      invoice_number: this.storage.data.invoice_number,
      invoice_date: this.storage.data.invoice_date,
      invoice_status: this.storage.data.invoice_status,
      stock_date_come: this.storage.data.stock_date_come,
      stock_delivery_type: this.storage.data.stock_delivery_type,
      stock_date_send: this.storage.data.stock_date_send,
      stock_person_comment: this.storage.data.stock_person_comment,
      user_comment: this.storage.data.user_comment,
      stock_date_delivery: this.storage.data.stock_date_delivery,
      stock_delivery_time: this.storage.data.stock_delivery_time,
      stock_person: this.storage.data.stock_person,
      filesUpload: '',
      deleteFiles: '',
      // itemsParams: this.fb.group({
      //   client_comment_1: this.storage.data.itemsParams ? this.storage.data.itemsParams.client_comment_1 : '',
      //   items_type_1: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_type_1 : '',
      //   delivery_type_1: this.storage.data.itemsParams ? this.storage.data.itemsParams.delivery_type_1 : '',
      //   items_count_1: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_count_1 : '', ValidateOnlyNumber],
      //   items_weight_1: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_weight_1 : '', ValidateOnlyNumber],
      //   items_length_1: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_length_1 : '', ValidateOnlyNumber],
      //   items_width_1: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_width_1 : '', ValidateOnlyNumber],
      //   items_height_1: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_height_1 : '', ValidateOnlyNumber],
      //   items_price_1: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_1 : '', ValidateOnlyNumber],
      //   items_price_currency_1: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_currency_1 : '',
      //
      //   client_comment_2: this.storage.data.itemsParams ? this.storage.data.itemsParams.client_comment_2 : '',
      //   items_type_2: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_type_2 : '',
      //   delivery_type_2: this.storage.data.itemsParams ? this.storage.data.itemsParams.delivery_type_2 : '',
      //   items_count_2: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_count_2 : '', ValidateOnlyNumber],
      //   items_weight_2: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_weight_2 : '', ValidateOnlyNumber],
      //   items_length_2: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_length_2 : '', ValidateOnlyNumber],
      //   items_width_2: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_width_2 : '', ValidateOnlyNumber],
      //   items_height_2: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_height_2 : '', ValidateOnlyNumber],
      //   items_price_2: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_2 : '', ValidateOnlyNumber],
      //   items_price_currency_2: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_currency_2 : '',
      //
      //   client_comment_3: this.storage.data.itemsParams ? this.storage.data.itemsParams.client_comment_3 : '',
      //   items_type_3: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_type_3 : '',
      //   delivery_type_3: this.storage.data.itemsParams ? this.storage.data.itemsParams.delivery_type_3 : '',
      //   items_count_3: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_count_3 : '', ValidateOnlyNumber],
      //   items_weight_3: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_weight_3 : '', ValidateOnlyNumber],
      //   items_length_3: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_length_3 : '', ValidateOnlyNumber],
      //   items_width_3: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_width_3 : '', ValidateOnlyNumber],
      //   items_height_3: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_height_3 : '', ValidateOnlyNumber],
      //   items_price_3: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_3 : '', ValidateOnlyNumber],
      //   items_price_currency_3: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_currency_3 : '',
      //
      //   client_comment_4: this.storage.data.itemsParams ? this.storage.data.itemsParams.client_comment_4 : '',
      //   items_type_4: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_type_4 : '',
      //   delivery_type_4: this.storage.data.itemsParams ? this.storage.data.itemsParams.delivery_type_4 : '',
      //   items_count_4: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_count_4 : '', ValidateOnlyNumber],
      //   items_weight_4: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_weight_4 : '', ValidateOnlyNumber],
      //   items_length_4: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_length_4 : '', ValidateOnlyNumber],
      //   items_width_4: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_width_4 : '', ValidateOnlyNumber],
      //   items_height_4: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_height_4 : '', ValidateOnlyNumber],
      //   items_price_4: [ this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_4 : '', ValidateOnlyNumber],
      //   items_price_currency_4: this.storage.data.itemsParams ? this.storage.data.itemsParams.items_price_currency_4 : '',
      // }),
    });
  }

  private setValueAutocomplete () {
    for (let item of Object.keys(this.fromNames)) {
      this.fromNames[item].value = this.storage.data[this.fromNames[item].name];
    }
  }

  private  checkWhoPaythis () {
    const val = this.form.get('stock_pay').value;
    this.isWhoPay = this.helpersService.whoPay(val);
  }

  private changeWhoPay () {
    this.form.get('stock_pay').valueChanges.subscribe(type => {
      this.isWhoPay = this.helpersService.whoPay(type);
    });
  }


  private buildAutocompleteCityToFormComponent () {
    this.autocompleteCityToFormComponent = {
        searchSelectAutocomplete: {
          formControlName: 'city_to',
          label: 'Город получателя',
          value: this.storage.data.city_to,
        },
        searchInfoAutocomplete: {
          country: {
            formControlName: 'country_to',
            label: 'Страна получателя',
            value: this.storage.data.country_to,
            searchEvent: 'countryName',
          },
          region: {
            formControlName: 'region_to',
            label: 'Район/Штат получателя',
            value: this.storage.data.region_to,
            searchEvent: 'region',
          },
          zip_code: {
            formControlName: 'zip_code_to',
            label: 'Индекс получателя',
            value: this.storage.data.zip_code_to,
            searchEvent: 'postal_code',
          }
        }
      };
  }

  private buildAutocompleteCityFromFormComponent () {
    this.autocompleteCityFromFormComponent = {
      searchSelectAutocomplete: {
        formControlName: 'city_from',
        label: 'Город отправителя',
        value: this.storage.data.city_from,
      },
      searchInfoAutocomplete: {
        country: {
          formControlName: 'country_from',
          label: 'Страна отправителя',
          value: this.storage.data.country_from,
          searchEvent: 'countryName',
        },
        region: {
          formControlName: 'region_from',
          label: 'Район/Штат отправителя',
          value: this.storage.data.region_from,
          searchEvent: 'region',
        },
        zip_code: {
          formControlName: 'zip_code_from',
          label: 'Индекс отправителя',
          value: this.storage.data.zip_code_from,
          searchEvent: 'postal_code',
        }
      }
    };
  }

  private changeFormValStockDateCome () {
    this.form.get('stock_date_come').valueChanges.subscribe( (date) => {
      this.minDateForDateSend = date;
    });
  }
}
