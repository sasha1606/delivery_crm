import { Component, OnInit, OnDestroy, ViewContainerRef } from '@angular/core';
import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { ToastsManager } from 'ng2-toastr';
import 'rxjs/add/operator/takeUntil';

import { AdminUsersService } from '../admin-users.service';

import { AdminUserGet, AdminUser } from '../admin-user';

import { FromValidation } from '@fromValidation';
import { FindToAutocompleteService } from '@shared/services/findToAutocomplete.service';
import { maskPhone } from '@shared/const/masks';
import {AutocompleteCity} from '@shared/interface/autocomplete-city';

@Component ({
    selector: 'app-admin-user-create',
    templateUrl: './admin-user-create.component.html',
    styleUrls: ['./admin-user-create.component.css']
})

export class AdminUserCreateComponent extends FromValidation implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    title = 'Добавление нового пользователя';
    form: FormGroup;
    user: AdminUser;
    data: AdminUserGet;

    public maskPhone =  maskPhone;

    public local_from = [];

    public autocompleteCityFormComponent: AutocompleteCity;

    constructor (
        public adminUsersService: AdminUsersService,
        public router: Router,
        private fb: FormBuilder,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef,
        private activatedRoute: ActivatedRoute,
        public findToAutocompleteService: FindToAutocompleteService,
    ) {
        super(
            router,
            adminUsersService
        );
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit () {
        this.data = this.activatedRoute.snapshot.data['data'];
        this.buildForm();
        this.buildAutocompleteCityFormComponent();
    }

    ngOnDestroy () {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

    /*========= LOCATION =========*/

    // public isLiction;
    // onGetLocation(city) {
    //   this.isLiction = this.findToAutocompleteService.findLocation(city)
    //     .subscribe(res => {
    //       this.local_from = res.data;
    //     });
    //
    // }
    //
    // onGetLocationFrom(event) {
    //   this.local_from = [];
    //   const test2 = event.target.value;
    //   this.onGetLocation(test2);
    // }
    //
    // onSelectLocationFrom(event) {
    //   // this.titleCountry_from = event.countryName;
    //   // this.postalCode_from = event.postal_code;
    //   // this.titleRegion_from = event.region;
    //   if (!event) {
    //     return false;
    //   }
    //   this.form.get('country').setValue(event.countryName);
    //   this.form.get('zip_code').setValue(event.postal_code);
    //   this.form.get('region').setValue(event.region);
    // }
    /*========= / LOCATION =========*/

    public onSubmit (form): void {
        if (this.form.valid) {
            this.adminUsersService.createUser(this.form.value).takeUntil(this.destroy$).subscribe( res => {
                if (res.success === 'ok') {
                    this.toastr.success('Has been created successfully');
                    setTimeout(() => {
                      this.router.navigateByUrl('/cabinet/admin-users');
                    }, 1000);
                }  else {
                    this.toastr.error('Error');
                    this.validationFormBackEnd(res.validation, this.form);
                }
            });

        } else {
            this.toastr.error('Error');
            this.validationFormsSubmit(this.form);
        }
    }

    private buildForm (): void {
        this.form = this.fb.group({
            lastname: [ '', [
                Validators.required,
            ]],
            username: [ '', [
                Validators.required,
            ]],
            firstname: [ '', [
                Validators.required,
            ]],
            email: ['', [
                Validators.required,
            ]],
            phone: [ '', [
                Validators.required,
            ]],
            // country: [''],
            // city: [''],
            // region: [''],
            // address: [''],
            // zip_code: [''],
            type: [ this.data.data.type, [
                Validators.required,
            ]],
        });
    }

  private buildAutocompleteCityFormComponent () {
    this.autocompleteCityFormComponent = {
      searchSelectAutocomplete: {
        formControlName: 'city',
        label: 'Город',
        value: '',
      },
      searchInfoAutocomplete: {
        country: {
          formControlName: 'country',
          label: 'Страна',
          value: '',
          searchEvent: 'countryName',
        },
        region: {
          formControlName: 'region',
          label: 'Район/Штат',
          value: '',
          searchEvent: 'region',
        },
        address: {
          formControlName: 'address',
          label: 'Адрес',
          value: '',
          searchEvent: null,
        },
        zip_code: {
          formControlName: 'zip_code',
          label: 'Индекс',
          value: '',
          searchEvent: 'postal_code',
        }
      }
    }
  }
}
