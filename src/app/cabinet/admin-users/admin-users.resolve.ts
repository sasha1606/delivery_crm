import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { AdminUsersService } from './admin-users.service';

@Injectable()

export class AdminUsersResolve implements Resolve<any> {
    constructor (
        private adminUsersService: AdminUsersService
    ) {}

    resolve (router: ActivatedRouteSnapshot) {
        return this.adminUsersService.getUsersList();
    }
}
