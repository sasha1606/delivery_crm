import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {ActivatedRoute, Router, Event, NavigationEnd, NavigationStart} from '@angular/router';

import { TokenStorageService } from '../../../token-storage.service';
import { UserInfoService } from '../../user.info.service';

import { UserInfo } from '../../shared-cabinet/interface/user-info';
import {ProfileSettingsEditComponent} from '../../profile-settings/profile-settings-edit/profile-settings-edit.component';

@Component ({
    selector: 'app-top-bar',
    templateUrl: './top-bar.component.html',
    styleUrls: ['./top-bar.component.css'],
})

export class TopBarComponent implements OnInit {

    user: UserInfo;

    @Output()
    public eventMainMenu: EventEmitter<boolean> = new EventEmitter<boolean>();

    private openMainMenu = false;
    public openProfileMenu = false;
    public isEnd = false;

    constructor (
        // private activatedRoute: ActivatedRoute,
        private router: Router,
        private tokenStorageService: TokenStorageService,
        private userInfoService: UserInfoService,
    ) {
    }


    ngOnInit () {

        this.userInfoService.getUserInfo().subscribe( res => {
            this.user = res.data;
        });

        this.router.events.subscribe( (event: Event) => {
          ProfileSettingsEditComponent.onButtonClick.subscribe(() => {this.isEnd = true; });
          if (event instanceof NavigationEnd && this.isEnd === true) {
            this.openProfileMenu = false;
              this.userInfoService.getUserInfo().subscribe( ress => {
                this.user = ress.data;
            });
            this.isEnd = false;
          }
        });
    }

    public toggleMainMenu () {
        this.openMainMenu = !this.openMainMenu;
        this.eventMainMenu.emit(this.openMainMenu);
    }

    public toggleProfileMenu (event) {
        this.openProfileMenu = !this.openProfileMenu;
        event.preventDefault();
    }

    public logout () {
        this.tokenStorageService.deleteToken();
    }
}
