import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiService } from '../api/api.service';
import { ApiSrc } from '../api/api.src';

import { UserInfoGet } from './shared-cabinet/interface/user-info';

import { tap, shareReplay } from 'rxjs/operators';

@Injectable()

export class UserInfoService {
    public id: number;
    public userInfo;

    constructor (
        private apiService: ApiService,
    ) {}

    public get userId () {
        return this.id;
    }

    public set userId (id: number) {
        this.id = id;
    }

    public getUserInfo (): Observable<UserInfoGet> {
      if (!this.userInfo) {
        this.userInfo = this.apiService.get(ApiSrc.userInfo).pipe(
          // tap( res => this.userRole = res.data.type),
          shareReplay(1)
        );
      }
      return this.userInfo;
    }

    public saveUserId (idBase64: string) {
        let id = idBase64.split('.')[1];
        // decode Base64
        id = atob(id);

        // decode JSON
        id = JSON.parse(id)['user_id'];
        this.userId = Number(id);
    }

    // public getUserRole () {
    //   if (!this.userRole) {
    //     this.getUserInfo().subscribe();
    //     console.log(1);
    //   }
    //   return this.userRole;
    // }

}
