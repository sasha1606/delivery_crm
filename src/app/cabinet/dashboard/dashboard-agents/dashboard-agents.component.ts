import { Component, OnInit, Input } from '@angular/core';

@Component ({
    selector: 'app-dashboard-agents',
    templateUrl: './dashboard-agents.component.html',
    styleUrls: ['./dashboard-agents.component.css'],
})

export class DashboardAgentsComponent implements OnInit {

    @Input() agents;

    public agentsInfo;
    // data: any = [
    //     {
    //         name: 'Компания 1',
    //         count: 2
    //     },
    //     {
    //         name: 'Компания 2',
    //         count: 2
    //     },
    //     {
    //         name: 'Компания 3',
    //         count: 2
    //     },
    //     {
    //         name: 'Компания 4',
    //         count: 2
    //     },
    //     {
    //         name: 'Компания 5',
    //         count: 2
    //     }
    // ];

    ngOnInit () {
      this.agentsInfo = this.agents;
    }
}
