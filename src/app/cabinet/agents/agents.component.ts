import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { AgentsService } from './agents.service';
import { AgentsListGet } from './agents';

@Component ({
    selector: 'app-agents',
    templateUrl: './agents.component.html',
    styleUrls: ['./agents.component.css'],
})

export class AgentsComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();

  title = 'Агенты';
  agentsList: AgentsListGet;

  page = 1;
  keyWord = '';
  sort = '';

  constructor (
    private activatedRoute: ActivatedRoute,
    private agentsService: AgentsService
  ) {}

  ngOnInit () {
    this.agentsList = this.activatedRoute.snapshot.data['agents'];
  }

  ngOnDestroy () {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  public tablePage (page: number): void {
    this.page = page;
    this.sendSort();
  }

  public tableSort (param: string): void {
    if (this.sort.length > 0 && param === this.sort) {
      param = `-${param}`;
    }
    this.sort = param;
    this.sendSort();
  }

  public tableSearch (word: string): void {
    this.keyWord = word;
    this.sendSort();
  }

  private sendSort () {
    this.agentsService.search({
      keyword: this.keyWord,
      page: this.page,
      sort: this.sort
    })
      .takeUntil(this.destroy$).subscribe(res => {
      this.agentsList = res;
    });
  }
}
