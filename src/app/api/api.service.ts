import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';

@Injectable()

export class ApiService {

    constructor (
        private http: HttpClient
    ) {}

    private url (link: string): string {
        if (link.length > 0) {
            return `${environment.apiHost}/${link}`;
        } else {
            return environment.apiHost;
        }
    }

    public get (url: string, params = {}): Observable<any> {
        let httpParams =  new HttpParams();
        if (Object.keys(params).length) {
          Object.keys(params).forEach( (item) => {
            httpParams = httpParams.append(item, params[item]);
          });
        }

        return this.http.get(this.url(url), {params: httpParams});
    }

    public post (url: string, data: object = {}, option: object = {}): Observable<any> {
        return this.http.post(this.url(url), data, option);
    }
}
