import { Pagination } from '@shared/interface/pagination';

export interface Agent {
  name: string | null;
  email: string | null;
  phone: string | null;
  responsible_person: string | null;
  typeNames: object;
  typeParams?: string[] | null;
  typesFormated: string;
  id: number;
  directionsParams: AgentModal[] | null;
}

export interface AgentsListGet {
  listdata: ShortAgents[];
  success: string;
  pagination: Pagination;
}

export interface AgentGet {
  data: Agent;
  success?: string;
  error?: string;
  validation?: object;
}

export interface AgentModal {
  carrier_id: number | null;
  comment: string | null;
  email: string | null;
  id: number | null;
  name: string | null;
  phone: string | null;
  responsible_person: string | null;
  typeParams: string[];
  typeNames: object | null;
}

export interface AgentModalGet {
  data: AgentModal;
  success?: string;
  error?: string;
  validation?: object;
  id: number;
}

interface ShortAgents {
  email: number;
  id: string;
  name: string;
  phone: string;
  responsible_person: string;
  typesFormated: string;
}

export const typePage = {
  edit: 'edit',
  create: 'create'
};


