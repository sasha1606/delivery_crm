import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { FormAutocompleteComponent } from './form-autocomplete.component';

import { PipeModule } from '@shared/pipes/pipe.module';
import { AutocompleteService } from '@shared/services/autocomplete.service';

@NgModule({
  imports:  [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PipeModule
  ],
  declarations: [
    FormAutocompleteComponent,
  ],
  exports: [
    FormAutocompleteComponent,
  ],
  providers: [
    AutocompleteService
  ]
})

export  class FormAutocompleteModule {

}
