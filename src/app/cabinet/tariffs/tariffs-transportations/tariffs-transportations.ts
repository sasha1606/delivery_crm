import { TariffsZones } from '../shared-tariffs/tariffs-zones/tariffs-zones';
import { TariffsTransporters } from '../shared-tariffs/tariffs-transporters/tariffs-transporters';

export interface TariffsTransportations {
  success: string;
  zones: TariffsZones[];
  transporters: TariffsTransporters[];
}
