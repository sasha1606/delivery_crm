import { Component, OnInit, OnDestroy } from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { OrdersService } from './orders.service';

import { OrdersList, OrdersGet } from './orders';

import { OrdersCountService } from '@shared/services/orders-count.service';

@Component ({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})

export class OrdersComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();

  public title = 'Заказы';

  public ordersList: OrdersList;

  public sort = '';
  public page = 1;
  public search = '';
  public date = '';
  public userIdSearch = '';
  public statusSearch = '';
  public users;
  public status;


  constructor (
    public ordersService: OrdersService,
    public router: Router,
    private activatedRoute: ActivatedRoute,
    private ordersCountService: OrdersCountService,
  ) {}

  ngOnInit () {
    this.routerEvents();
    this.ordersList = this.activatedRoute.snapshot.data['orderList'];
    this.setInitFilterData();
    this.toZeroOrdersCount();

  }

  ngOnDestroy () {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  public tablePage (page: number): void {
    this.page = page;
    this.sendSort();
  }

  public tableSort (param: string): void {
    if (this.sort.length > 0 && param === this.sort) {
      param = `-${param}`;
    }
    this.sort = param;
    this.sendSort();
  }

  public tableSearch (word: string): void {
    this.search = word;
    this.sendSort();
  }

  public getSearchData (data)  {
    this.date = data.data;
    this.search = data.word;
    this.userIdSearch = data.anotherFilter;
    this.statusSearch = data.anotherFilterSecond;
    this.sendSort();
  }

  private sendSort (): void {
    this.ordersService.search({
      sort: this.sort,
      key: this.search,
      page: this.page,
      date: this.date,
      manager: this.userIdSearch,
      status: this.statusSearch,
    }).takeUntil(this.destroy$).subscribe( orders => {
      this.ordersList = orders;
    });
  }

  private toZeroOrdersCount() {
    this.ordersCountService.toZeroCountsNewOrders().subscribe();
  }

  private getInitData () {
    this.ordersService.getOrdersList().takeUntil(this.destroy$).subscribe(response => {
      this.ordersList = response;
      this.setInitFilterData();
      this.toZeroOrdersCount();
    });
  }

  private routerEvents () {
    this.router.events.takeUntil(this.destroy$).subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.getInitData();
      }
    });
  }

  private setInitFilterData() {
    this.users = this.ordersList.listmanagers;
    this.status = this.ordersList.liststatuses;
  }
}
