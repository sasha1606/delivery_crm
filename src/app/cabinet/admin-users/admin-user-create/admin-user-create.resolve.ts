import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { AdminUsersService } from '../admin-users.service';

@Injectable()

export class AdminUserCreateResolve implements Resolve<any> {

    constructor (
        private adminUsersService: AdminUsersService,
    ) {}

    resolve () {
        return this.adminUsersService.dataCreateUser();
    }
}
