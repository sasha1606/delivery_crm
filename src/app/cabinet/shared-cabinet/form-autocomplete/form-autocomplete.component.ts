import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AutocompleteService } from '@shared/services/autocomplete.service';
import { Subject } from 'rxjs/Subject';

@Component ({
  selector: 'app-form-autocomplete',
  templateUrl: './form-autocomplete.component.html',
  styleUrls: ['./form-autocomplete.component.css']

})

export class FormAutocompleteComponent implements OnInit, OnDestroy, OnChanges {

  @Input() form;
  @Input() formName: any;
  @Input() disabledForm?;

  public transporters;
  public directions;
  private destroy$: Subject<boolean> = new Subject<boolean>();

  constructor (
    private fb: FormBuilder,
    private modalAutocompleteService: AutocompleteService,
  ) {}

  ngOnInit () {
    this.buildForm();

    if (this.disabledForm) {
      this.form.disable();
    }

    this.changeModalSelect();

    this.getDirections(this.formName.type.value, this.formName.id.value);
    this.getTransporters(this.formName.type.value);
  }

  ngOnDestroy () {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  ngOnChanges (changes: SimpleChanges) {
  }

  public changeModalSelect () {

    this.form.get(this.formName.type.name).valueChanges.subscribe(type => {
      if (!type) {
        return;
      }

      this.form.get(this.formName.id.name).setValue('');
      this.form.get(this.formName.direction_name.name).setValue('');
      this.form.get(this.formName.direction_phone.name).setValue('');
      this.form.get(this.formName.direction_email.name).setValue('');
      this.form.get(this.formName.direction_resperson.name).setValue('');
      this.directions = null;
      this.transporters = null;

      this.getTransporters(type);
    });

    this.form.get(this.formName.id.name).valueChanges.subscribe(id => {
      if (!id) {
        return;
      }

      this.form.get(this.formName.direction_id.name).setValue('');
      this.form.get(this.formName.direction_name.name).setValue('');
      this.form.get(this.formName.direction_phone.name).setValue('');
      this.form.get(this.formName.direction_email.name).setValue('');
      this.form.get(this.formName.direction_resperson.name).setValue('');
      this.directions = null;

      this.getDirections(this.form.get(this.formName.type.name).value, id);
    });

    this.form.get(this.formName.direction_id.name).valueChanges.subscribe(id => {
      if (!id) {
        return;
      }

      this.modalAutocompleteService.changeDirections({
        transportersType: this.form.get(this.formName.type.name).value,
        transporterId: this.form.get(this.formName.id.name).value,
        directionId: id,
      })
        .takeUntil(this.destroy$).subscribe( res => {
        this.form.get(this.formName.direction_name.name).setValue(res.directioninfo.name);
        this.form.get(this.formName.direction_phone.name).setValue(res.directioninfo.phone);
        this.form.get(this.formName.direction_email.name).setValue(res.directioninfo.email);
        this.form.get(this.formName.direction_resperson.name).setValue(res.directioninfo.responsible_person);
      });
    });
  }

  private buildForm () {
    this.form.addControl(this.formName.type.name, new FormControl(this.formName.type.value));
    this.form.addControl(this.formName.id.name, new FormControl(this.formName.id.value));
    this.form.addControl(this.formName.direction_id.name, new FormControl(this.formName.direction_id.value));
    this.form.addControl(this.formName.direction_name.name, new FormControl(this.formName.direction_name.value));
    this.form.addControl(this.formName.direction_phone.name, new FormControl(this.formName.direction_phone.value));
    this.form.addControl(this.formName.direction_email.name, new FormControl(this.formName.direction_email.value));
    this.form.addControl(this.formName.direction_resperson.name, new FormControl(this.formName.direction_resperson.value));
  }

  private getTransporters (value) {
    if (value) {
      this.modalAutocompleteService.changetransportersType(value).takeUntil(this.destroy$).subscribe(res => {
        if (res.success === 'ok') {
          this.transporters = res;
        }
      });
    }
  }

  private getDirections (value, id) {
    if (value && id ) {
      this.modalAutocompleteService.changeTransporterId({
        transportersType: value,
        transporterId: id,
      })
        .takeUntil(this.destroy$).subscribe( res => {
        if (res.success === 'ok') {
          this.directions = res;
        }
      });
    }
  }

}
