import {Component, OnInit, OnDestroy, ViewContainerRef} from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { TokenStorageService } from '../../token-storage.service';
import { AuthService } from '../auth.service';
import { ToastsManager } from 'ng2-toastr';

@Component ({
    selector: 'app-password-recovery',
    templateUrl: './password-recovery.component.html',
    styleUrls: ['./password-recovery.component.css']
})

export class PasswordRecoveryComponent implements OnInit, OnDestroy {

    private  destroy$: Subject<boolean> = new Subject<boolean>();

    public form: FormGroup;

    constructor (
      private fb: FormBuilder,
      private tokenStorageService: TokenStorageService,
      private authService: AuthService,
      private toastr: ToastsManager,
      private vcr: ViewContainerRef,
    ) {
      this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit () {
      this.buildForm();
    }

  ngOnDestroy () {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

    public resetPassword () {
      if (this.form.valid) {

        if (this.tokenStorageService.token) {
          this.tokenStorageService.deleteToken();
        }

        this.authService.resetPassword(this.form.value).takeUntil(this.destroy$).subscribe(res => {
          if (res.success === 'ok') {
            this.toastr.success('Successfully');
            // setTimeout(() => {
            //   this.router.navigateByUrl('/cabinet/couriers');
            // }, 1000);
          }  else {
            this.toastr.error('Error');
            // this.validationFormBackEnd(res.validation, this.form);
          }
        }, (errors) => {
          this.toastr.error('Error');
        });

      }
    }

    private buildForm () {
      this.form = this.fb.group({
          email: ['', Validators.required],
      });
    }
}
