import {Component, OnInit, OnDestroy, ViewContainerRef} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { CustomersUsersService } from './customers-users.service';

import { CostumerUserList } from './costumer-user';
import {ToastsManager} from 'ng2-toastr';

@Component ({
    selector: 'app-admin-user',
    templateUrl: './customers-users.component.html',
    styleUrls: ['./customers-users.component.css']
})

export class CustomersUsersComponent implements OnInit, OnDestroy {

    private destroy$: Subject<boolean> = new Subject<boolean>();

    public title = 'Клиенты';

    public usersList: CostumerUserList;
    public userIds: number[] = [];

    public date = '';
    public sort = '';
    public page = 1;
    public search = '';
    public orders = '';
    public searchUserId = '';

    public isChecked = false;

    constructor (
        private activatedRoute: ActivatedRoute,
        private customersUsersService: CustomersUsersService,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef,
    ) {
      this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit () {
        this.usersList = this.activatedRoute.snapshot.data['users'];
    }

    ngOnDestroy () {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }

    public getDate (date): void {
        this.date = date;
    }

    public check (id: number): void {
        this.userIds = this.customersUsersService.checkboxIds(id, this.userIds);
    }

    public sendLetter (): void {
        if (this.userIds.length > 0) {
            this.customersUsersService.sentLetter({clientsIds: this.userIds}).takeUntil(this.destroy$).subscribe(res => {
              if (res.error) {
                this.toastr.error(res.message);
              } else if (res.success) {
                this.toastr.success('Has been sent successfully');
              }

              this.isChecked = null;
              this.userIds = [];
            });
        }
      this.isChecked = false;
    }

    public tableSort (param: string): void {
        if (this.sort.length > 0 && param === this.sort) {
            param = `-${param}`;
        }
        this.sort = param;
        this.sendSort();
    }

    public tableSearch (word: string): void {
        this.search = word;
        this.sendSort();
    }

    public tablePage (page: number): void {
        this.page = page;
        this.sendSort();
    }

    public tableOrders (val): void {
        this.orders = val;
        this.sendSort();
    }

    private sendSort (): void {
        this.customersUsersService.search(
            this.sort, this.search, this.page, this.date, this.orders, this.searchUserId
        ).takeUntil(this.destroy$).subscribe( users => {
            this.usersList = users;
        });
    }
}
